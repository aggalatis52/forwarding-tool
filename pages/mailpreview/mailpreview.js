let MailPreviewClass = function (Helpers) {
  this.Helpers = Helpers
  this.jobId = null
  this.groupId = null
  this.forwarders = []
}

MailPreviewClass.prototype.sendOffer = function () {
  let self = this
  console.log(`lalalal`)
}

MailPreviewClass.prototype.bindEventsOnBtn = function () {
  let self = this

  $('#mail-preview-request-offer').on('click', async function () {
    Swal.fire({
      title: 'Request Offer?',
      text: `Are you sure you want to request offer via email?`,
      icon: 'warning',
      showCancelButton: true,
      showLoaderOnConfirm: true,
      cancelButtonText: 'Cancel',
      confirmButtonColor: '#dc3545',
      confirmButtonText: 'Confirm',
      preConfirm: async () => {
        const result = await self.sendEmails()
        if (!result) throw 'Error'
      }
    }).then(result => {
      console.log(result)
      if (result.isConfirmed) {
        Swal.fire('Success!', `Offer Requested!`, 'success').then(result => {
          $('#mail-preview-modal').modal('hide')
          $('#add-job-modal').modal('show')
          if (self.jobId != null) window.IndividualsClass.refreshTable('prices')
          if (self.groupId != null) window.IndividualsClass.refreshTable('group-prices')
        })
      }
    })
  })
}

MailPreviewClass.prototype.renderJobPreviewModal = async function (jobId, forwarders = []) {
  let self = this
  self.jobId = jobId
  self.forwarders = forwarders
  self.groupId = null
  const response = await self.getJobPreviewFile(jobId)
  $('#add-job-modal').modal('hide')
  $('#email-preview').prop('src', `${self.Helpers.SERVER_DOCUMENTS_URL}/${response}`)
  let forwardersHtml = `<h5>Forwarders List</h5><br /> <br /><div class="col-12 col-md-12 col-sm-12">`
  for (let valFor of forwarders) {
    forwardersHtml += `
    <div class="checkbox">
       <label class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input forwarder-offer-checkbox" data-id='${valFor.forwarder_id}'>
         <span class="custom-control-indicator"></span>
         <span class="" >${valFor.forwarder_name}</span>
       </label>
    </div>`
  }
  forwardersHtml += '</div>'
  $('#mail-preview-forwarders').html(forwardersHtml)
  $('#mail-preview-modal').modal('show')
}

MailPreviewClass.prototype.renderGroupPreviewModal = async function (groupId, forwarders = []) {
  let self = this
  self.jobId = null
  self.forwarders = forwarders
  self.groupId = groupId
  const response = await self.getGroupPreviewFile(groupId)
  $('#add-job-modal').modal('hide')
  $('#email-preview').prop('src', `${self.Helpers.SERVER_DOCUMENTS_URL}/${response}`)
  let forwardersHtml = `<h5>Forwarders List</h5><br /> <br /><div class="col-12 col-md-12 col-sm-12">`
  for (let valFor of forwarders) {
    forwardersHtml += `
    <div class="checkbox">
       <label class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input forwarder-offer-checkbox" data-id='${valFor.forwarder_id}'>
         <span class="custom-control-indicator"></span>
         <span class="" >${valFor.forwarder_name}</span>
       </label>
    </div>`
  }
  forwardersHtml += '</div>'
  $('#mail-preview-forwarders').html(forwardersHtml)
  $('#mail-preview-modal').modal('show')
}

MailPreviewClass.prototype.getJobPreviewFile = async function (jobId) {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + `/jobs/${jobId}/requestPreview`, { headers: self.Helpers.getHeaders() })
    return response.data
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

MailPreviewClass.prototype.getGroupPreviewFile = async function (groupId) {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + `/jobGroups/${groupId}/requestPreview`, { headers: self.Helpers.getHeaders() })
    return response.data
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

MailPreviewClass.prototype.checkExistance = function () {
  let self = this
  console.log(`Mail Preview Exists..`)
}

MailPreviewClass.prototype.sendEmails = async function () {
  let self = this
  let selectedForwarderIds = []
  $('.forwarder-offer-checkbox').each(function () {
    if ($(this).is(':checked')) selectedForwarderIds.push($(this).data('id'))
  })
  for (let forwarderId of selectedForwarderIds) {
    let dummyPrice = {
      price_job_group_id: self.groupId,
      price_job_id: self.jobId,
      price_forwarder_id: forwarderId,
      price_value: null,
      price_currency_name: null,
      price_currency_rate: null,
      price_selected: false,
      price_offer_requested: false
    }
    const newPrice = await self.addPrice(dummyPrice)
    if (newPrice.price_job_id != null) await self.requestOfferForPrice(newPrice.price_id, 'job')
    if (newPrice.price_job_group_id != null) await self.requestOfferForPrice(newPrice.price_id, 'group')
  }
  return true
}

MailPreviewClass.prototype.addPrice = async function (price) {
  let self = this
  try {
    const response = await axios.post(self.Helpers.API_URL + '/prices', price, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Price Created!')
    return response.data
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    return null
  }
}

MailPreviewClass.prototype.requestOfferForPrice = async function (priceId, type) {
  let self = this
  try {
    await axios.get(self.Helpers.API_URL + `/prices/${priceId}/${type}/requestOffer`, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Price Offer Requested!')
    return true
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    return false
  }
}
