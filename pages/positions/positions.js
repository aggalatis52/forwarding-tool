let PositionsClass = function () {
  dayjs.extend(window.dayjs_plugin_customParseFormat)
  this.positions = []
  this.vessels = []
  this.cities = []
  this.Validations = new ValidationsClass()
  this.Helpers = new HelpersClass()
  this.Helpers.validateLogin()
  this.Helpers.bindMovingEvents('position-modal-mover', 'position-modal', 'position-modal-content')
  this.addEventsOnButtons()
  this.initVessels()
  this.initCities()
  this.initTable()
}

PositionsClass.prototype.addEventsOnButtons = function () {
  let self = this

  $('#position_vessel_id').selectize()
  $('#position_city_id').selectize()

  $('#save-position').on('click', function () {
    let positionData = {
      position_id: parseInt($('#position_id').val()),
      position_info: $('#position_info').val(),
      position_vessel_id: parseInt($('#position_vessel_id').val()),
      position_city_id: parseInt($('#position_city_id').val()),
      position_eta: self.Helpers.changeDateToMysql($('#position_eta').val()),
      position_etd: self.Helpers.changeDateToMysql($('#position_etd').val())
    }

    let validationErrors = self.Validations.isPositionValid(positionData)
    if (validationErrors) {
      self.Helpers.toastr('error', `Validation Errors: ${validationErrors.join(',')}`)
      return
    }

    self.updatePosition(positionData)
  })
}

PositionsClass.prototype.initTable = async function () {
  let self = this
  await self.initPositions()
  let positionsTable = $('#positions-table').DataTable({
    data: self.positions,
    fixedHeader: {
      headerOffset: 100,
      header: true,
      footer: false
    },
    bLengthChange: false,
    columns: [
      { title: 'Vessel', orderable: false, data: 'vessel_description' },
      { title: 'Country', orderable: false, data: 'country_name' },
      { title: 'Port Of Birth', orderable: false, data: 'city_name' },
      { title: 'ETA', orderable: false, data: 'position_eta' },
      { title: 'ETD', orderable: false, data: 'position_etd' },
      { title: 'Notes', orderable: false, data: 'position_info' },
      { title: 'Last Update', orderable: false, data: 'position_updated_at' },
      {
        title: 'ACTIONS',
        orderable: false,
        defaultContent: "<i class='fa fa-pencil edit-job action-btn' title='modify' style='cursor: pointer' ></i>"
      }
    ],
    rowCallback: function (row, data, index, cells) {
      // Here I am changing background Color for grouped rows
      if (data.highlighted == true) $('td', row).css('background-color', '#F08080')
    },
    order: [],
    pageLength: 25
  })
  $('#positions-table').on('click', 'i.edit-job', function () {
    var data = positionsTable.row($(this).closest('tr')).data()
    console.log(data)

    $('#position_id').val(data.position_id)
    $('#position_info').val(data.position_info)
    $('#position_vessel_id')[0].selectize.setValue(data.position_vessel_id)
    $('#position_city_id')[0].selectize.setValue(data.position_city_id)
    $('#position_eta').val(data.position_eta)
    $('#position_etd').val(data.position_etd)
    $('#position-modal').modal('show')
  })
}

PositionsClass.prototype.initPositions = async function () {
  let self = this
  try {
    const positions = await axios.get(self.Helpers.API_URL + '/positions', { headers: self.Helpers.getHeaders() })
    const currentTimestamp = Math.floor(Date.now() / 1000)
    for (let position of positions.data) {
      position.etd_timestamp = dayjs(position.position_etd, 'YYYY-MM-DD').unix()
      position.highlighted = false
      if (currentTimestamp - position.etd_timestamp > 0) position.highlighted = true
      position.position_eta = self.Helpers.changeMysqlDateToNormal(position.position_eta)
      position.position_etd = self.Helpers.changeMysqlDateToNormal(position.position_etd)
      position.position_updated_at = self.Helpers.changeMysqlDateToNormal(position.position_updated_at)
    }
    self.positions = positions.data.sort((a, b) => a.etd_timestamp - b.etd_timestamp)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.positions = []
  }
}

PositionsClass.prototype.updatePosition = async function (position) {
  let self = this
  try {
    await axios.put(self.Helpers.API_URL + '/positions', position, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Position Updated!')
    $('#position-modal').modal('hide')
    self.refreshTable()
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

PositionsClass.prototype.refreshTable = function () {
  let self = this
  self.positions = []
  $('#positions-table').unbind('click')
  $('#positions-table').DataTable().clear()
  $('#positions-table').DataTable().destroy()
  self.initTable()
}

PositionsClass.prototype.initVessels = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/vessels', { headers: self.Helpers.getHeaders() })
    self.vessels = response.data
    for (i = 0; i < self.vessels.length; i++) {
      $('#position_vessel_id')[0].selectize.addOption({ text: self.vessels[i].vessel_description, value: self.vessels[i].vessel_id })
    }
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.vessels = []
  }
}

PositionsClass.prototype.initCities = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/cities', { headers: self.Helpers.getHeaders() })
    self.cities = response.data
    for (i = 0; i < self.cities.length; i++) {
      $('#position_city_id')[0].selectize.addOption({ text: `${self.cities[i].city_name} - ${self.cities[i].country_name}`, value: self.cities[i].city_id })
    }
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.cities = []
  }
}

new PositionsClass()
