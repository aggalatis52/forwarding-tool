let LoginClass = function () {
  this.Helpers = new HelpersClass()
  this.checkIfUserIsValidated()
  this.createBindings()
  $('#user_username').focus()
}

LoginClass.prototype.checkIfUserIsValidated = function () {
  let self = this
  if (self.Helpers.userTokenExists()) window.location.replace('/individuals')
}

LoginClass.prototype.createBindings = function () {
  let self = this
  $('#iso-read-more').on('click', function () {
    var dots = document.getElementById('dots')
    var moreText = document.getElementById('more')
    var btnText = document.getElementById('iso-read-more')

    if (dots.style.display === 'none') {
      dots.style.display = 'inline'
      btnText.innerHTML = 'Read more'
      moreText.style.display = 'none'
    } else {
      dots.style.display = 'none'
      btnText.innerHTML = 'Read less'
      moreText.style.display = 'inline'
    }
  })

  $('#user_username').on('keyup', function (event) {
    if (event.keyCode === 13) {
      event.preventDefault()
      self.submitLoginForm()
    }
  })

  $('#user_password').on('keyup', function (event) {
    if (event.keyCode === 13) {
      event.preventDefault()
      self.submitLoginForm()
    }
  })

  $('#sign-in-btn').on('click', () => {
    self.submitLoginForm()
  })
}

LoginClass.prototype.submitLoginForm = function () {
  let self = this
  $('#sign-in-btn').prop('disabled', 'disabled')
  $('#sign-in-btn').html('attempting connection...')

  let user = {
    username: $('#user_username').val(),
    password: $('#user_password').val()
  }

  axios
    .post(self.Helpers.API_URL + '/users/authenticate', user)
    .then(res => {
      if (!res.data.token || res.data.token == '') throw 'Anauthorized'
      window.localStorage.setItem('user-data', JSON.stringify(res.data.user))
      window.localStorage.setItem('user-token', res.data.token)
      window.location.replace('/individuals')
    })
    .catch(err => {
      console.error(err)
      $('#user_username').val('').blur()
      $('#user_password').val('').blur()
      $('#sign-in-btn').prop('disabled', null)
      $('#sign-in-btn').html('SIGN IN')
      if (err.response && err.response.status && err.response.status == 401) {
        self.Helpers.toastr('error', 'Wrong username or password. Please try again.')
      } else {
        self.Helpers.handleRequestError(err)
      }
    })
}

new LoginClass()
