let ConsolidationsClass = function () {
  this.Wizzard = new WizzardClass(['transportation', 'pieces', 'forwarders', 'group', 'group-forwarders', 'costs'])
  this.Helpers = new HelpersClass()
  this.Helpers.validateLogin()
  this.Helpers.bindCloseBtnsAlerts()
  this.Helpers.bindMovingEvents('consolidation-modal-mover', 'consolidation-modal', 'consolidation-modal-content')
  this.Wizzard = new WizzardClass(['transportation', 'pieces', 'forwarders', 'documents'])
  this.consolidations = []
  this.forwarders = []
  this.countires = []
  this.consolidationsTable = null
  this.currentPrice = null
  this.Validations = new ValidationsClass()

  this.bindEventsOnButtons()
  this.initVessels()
  this.initCities()
  this.initModes()
  this.initForwarders()
  this.initCountries()
  this.initServiceTypes()
  this.initCurrencies()
  this.initPricesTable()
  this.initConsolidationsTable()
}

ConsolidationsClass.prototype.bindEventsOnButtons = function () {
  let self = this

  $('#mode-select').selectize()
  $('#forwarder-select').selectize()
  $('#currency-select').selectize()
  $('#group-ex-select').selectize()
  $('#group-to-select').selectize()
  $('#service-type-select').selectize()

  $('#close-consolidation-modal').on('click', function () {
    self.refreshTable('consolidations-table')
  })

  $('#save-notes-btn').on('click', async function () {
    const conGroup = {
      con_group_notes: self.Helpers.validateSelectValue($('#notes').val())
    }
    await self.patchConsolidationGroup(self.currentJob.con_group_id, conGroup)
  })

  $('#save-consolidation-btn').on('click', async function () {
    const conGroup = {
      con_group_id: self.currentJob.con_group_id,
      con_group_ex: self.Helpers.validateSelectValue($('#group-ex-select').val()),
      con_group_to: self.Helpers.validateSelectValue($('#group-to-select').val()),
      con_group_deadline: self.Helpers.validateSelectValue($('#group-deadline').val()),
      con_group_service_type: self.Helpers.validateSelectValue($('#service-type-select').val()),
      con_group_mode: self.Helpers.validateSelectValue($('#mode-select').val())
    }
    await self.updateConsolidationsGroup(conGroup)
    self.renderConsolidationModal(conGroup.con_group_id)
  })

  $('#request-con-group-offer-btn').on('click', function () {
    console.log(self.currentJob)
    const countryForwardes = _.filter(self.forwarders, function (o) {
      return o.forwarder_countries.indexOf(self.currentJob.ex.city_country_id.toString()) > -1
    })

    const selectedCountry = self.countries.find(el => el.country_id == self.currentJob.ex.city_country_id)
    let forwardersHtml = `<div class="row"><div class="col-12" style="margin-botom: 5%"><p>Choose forwarders to request price offers via email. Only forwarders selected for ${selectedCountry.country_name} are visible.</p></div>`
    let showSendEmailButton = false
    for (let forwarder of self.forwarders) {
      let jobForwarderPrice = self.currentJobPrices.findIndex(el => el.forwarder.forwarder_id == forwarder.forwarder_id)
      if (jobForwarderPrice > -1) continue
      if (countryForwardes) {
        let isCountryForwarder = countryForwardes.findIndex(el => el.forwarder_id == forwarder.forwarder_id)
        if (isCountryForwarder == -1) continue
      }
      forwardersHtml += `
        <div class="checkbox">
           <label class="custom-control custom-checkbox">
             <input type="checkbox" class="custom-control-input forwarder-offer-checkbox" data-id='${forwarder.forwarder_id}'>
             <span class="custom-control-indicator"></span>
             <span class="custom-control-description" >${forwarder.forwarder_name}</span>
           </label>
        </div>`
      showSendEmailButton = true
    }
    forwardersHtml += `</div>`
    Swal.fire({
      title: 'Offer Request',
      html: forwardersHtml,
      icon: 'info',
      showCancelButton: true,
      showConfirmButton: showSendEmailButton,
      cancelButtonText: 'Cancel',
      showLoaderOnConfirm: true,
      confirmButtonColor: '#68bb69',
      confirmButtonText: 'Send Emails',
      preConfirm: async () => {
        let selectedForwarderIds = []
        $('.forwarder-offer-checkbox').each(function () {
          if ($(this).is(':checked')) selectedForwarderIds.push($(this).data('id'))
        })
        for (let forwarderId of selectedForwarderIds) {
          let dummyPrice = {
            con_price_: null,
            con_price_consolidation_id: self.currentJob.con_group_id,
            con_price_forwarder_id: forwarderId,
            con_price_locals_value: null,
            con_price_individuals_value: null,
            con_price_grouped_value: null,
            con_price_total_value: null,
            con_price_selected: false,
            con_price_currency_name: null,
            con_price_currency_rate: null,
            con_price_offer_requested: false
          }
          const priceID = await self.addConsolidationPrice(dummyPrice)
          await self.requestOfferForConsolidation(priceID)
        }
      }
    }).then(async result => {
      if (result.isConfirmed) self.refreshTable('con-group-prices-table')
    })
  })

  $('#show-price-table-btn').on('click', function () {
    $('#add-price-div').hide()
    $('#save-notes-btn').show()
    $('#notes-div').show()
    $('#request-con-group-offer-btn').show()
    $('#add-con-group-price-btn').show()
  })

  $('#add-con-group-price-btn').on('click', function () {
    self.currentPrice = null
    $('#forwarder-select')[0].selectize.setValue('')
    $('#currency-select')[0].selectize.setValue(self.Helpers.GLOBAL_CURRENCY)
    $('#local-price-value').val('')
    $('#individual-price-value').val('')
    $('#group-price-value').val('')
    $('#add-price-div').show()
    $('#save-notes-btn').hide()
    $('#notes-div').hide()
    $('#request-con-group-offer-btn').hide()
    $('#add-con-group-price-btn').hide()
  })

  $('#save-price-btn').on('click', async function () {
    const currentGroup = self.consolidationGroups[self.currentJob.con_group_id]
    let passedPricesValidation = true
    if (currentGroup.hasLocal && $('#local-price-value').val() == '') passedPricesValidation = false
    if (currentGroup.hasIndividual && $('#individual-price-value').val() == '') passedPricesValidation = false
    if (currentGroup.hasGrouped && $('#group-price-value').val() == '') passedPricesValidation = false
    if (!passedPricesValidation) {
      self.Helpers.toastr('error', 'Prices cannot be empty')
      return
    }
    const localPrice = $('#local-price-value').val() == '' ? null : parseFloat($('#local-price-value').val())
    const individualPrice = $('#individual-price-value').val() == '' ? null : parseFloat($('#individual-price-value').val())
    const groupPrice = $('#group-price-value').val() == '' ? null : parseFloat($('#group-price-value').val())
    let totalValue = 0
    if (localPrice != null) totalValue += localPrice
    if (individualPrice != null) totalValue += individualPrice
    if (groupPrice != null) totalValue += groupPrice

    let price = {
      con_price_consolidation_id: self.currentJob.con_group_id,
      con_price_forwarder_id: $('#forwarder-select').val() == '' ? null : $('#forwarder-select').val(),
      con_price_locals_value: localPrice,
      con_price_individuals_value: individualPrice,
      con_price_grouped_value: groupPrice,
      con_price_total_value: totalValue,
      con_price_selected: 0,
      con_price_currency_name: $('#currency-select').val() == '' ? null : $('#currency-select').val(),
      con_price_offer_requested: 0
    }

    let validationErrors = self.Validations.isConsolidationPriceValid(price)
    if (validationErrors) {
      self.Helpers.toastr('error', `Validation Errors: ${validationErrors.join(',')}`)
      return
    }

    const fullCurrency = self.currencies.find(el => el.currency_name == price.con_price_currency_name)
    price.con_price_currency_rate = fullCurrency.currency_rate

    if (self.currentPrice == null) {
      await self.addConsolidationPrice(price)
    } else {
      price.con_price_id = self.currentPrice.con_price_id
      await self.updateConslidationPrice(price)
    }
    self.renderPricesTab(true)
  })
}

ConsolidationsClass.prototype.initConsolidationsTable = async function () {
  let self = this
  self.visitedGroups = []
  await self.initConsolidations()
  self.consolidationsTable = $('#consolidations-table').DataTable({
    data: self.consolidations,
    bLengthChange: false,
    paging: false,
    info: false,
    fixedHeader: {
      headerOffset: 100,
      header: true,
      footer: false
    },
    columns: [
      {
        title: 'JOB ID',
        orderable: false,
        data: 'job_id',
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.job_display_id != null) $(td).html(rowData.job_display_id)
        }
      },
      {
        title: 'CON. ID',
        orderable: false,
        data: 'job_consolidation_group_id',
        createdCell: function (td, cellData, rowData, row, col) {
          $(td).css('background-color', rowData.con_group_color).css('font-weight', 'bold')
        }
      },
      {
        title: 'TYPE',
        orderable: false,
        data: 'job_type',
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.job_type == 'Individual') $(td).css('color', self.Helpers.INDIVIDUAL_COLOR).css('font-weight', 'bold')
          if (rowData.job_type == 'Grouped') $(td).css('color', self.Helpers.GROUPED_COLOR).css('font-weight', 'bold')
          if (rowData.job_type == 'Personnel') $(td).css('color', self.Helpers.PERSONNEL_COLOR).css('font-weight', 'bold')
          if (rowData.job_type == 'Local Order') $(td).css('color', self.Helpers.LOCAL_ORDER_COLOR).css('font-weight', 'bold')
        }
      },
      { title: 'REQ. DATE', orderable: false, data: 'request_date' },
      { title: 'USER', orderable: false, data: 'user.user_username' },
      { title: 'DEPT.', orderable: false, data: 'department.department_description' },
      {
        title: 'PIECES',
        orderable: false,
        data: 'pieces_count',
        createdCell: function (td, cellData, rowData, row, col) {
          let text = 'Pieces'
          if (rowData.pieces_count == 1) text = 'Piece'
          $(td).html(`<p class='piece-info' style='cursor: pointer'>${rowData.pieces_count} ${text}</p>`)
        }
      },
      {
        title: 'WEIGHT (KG)',
        orderable: false,
        data: 'job_weight'
      },
      {
        title: 'MODE',
        orderable: false,
        createdCell: function (td, cellData, rowData, row, col) {
          $(td).css('color', 'blue').css('font-weight', 'bold')
        },
        data: 'mode_description',
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.mode_description == '') {
            $(td).html('<div class="empty-table-value" ></div>')
          }
        }
      },
      {
        title: 'SERVICE',
        orderable: false,
        data: 'service_description',
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.service_description == '') {
            $(td).html('<div class="empty-table-value" ></div>')
          }
        }
      },
      {
        title: 'VESSEL',
        orderable: false,
        className: 'danger-header',
        data: 'vessel.vessel_description'
      },
      { title: 'EX', orderable: false, className: 'danger-header', data: 'ex.city_name' },
      {
        title: 'TO',
        orderable: false,
        className: 'danger-header',
        data: 'destination_description',
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.destination_description == '') {
            $(td).html('<div class="empty-table-value" ></div>')
          }
        }
      },
      {
        title: 'DEADLINE',
        orderable: false,
        className: 'danger-header',
        data: 'job_deadline',
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.job_deadline == null) {
            $(td).html('<div class="empty-table-value"></div>')
          }
        }
      },
      {
        title: 'Forwarder',
        data: 'job_forwarder_id',
        orderable: false,
        createdCell: function (td, cellData, rowData, row, col) {
          let forwarderId = rowData.job_forwarder_id
          let foundFowarder = self.forwarders.find(el => el.forwarder_id == forwarderId)
          if (!foundFowarder) return
          $(td).html(foundFowarder.forwarder_name)
        }
      },
      {
        title: 'SHARED COST ($)',
        data: 'job_shared_cost',
        orderable: false,
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.job_shared_cost == null) {
            $(td).html('<div class="empty-table-value"></div>')
          }
        }
      },

      {
        title: 'ACTIONS',
        orderable: false,
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.to == null || rowData.mode == null || rowData.job_deadline == null || rowData.service_type == null) {
            $(td).children('.confirm-job').hide()
            $(td).children('.job-costs').hide()
          }
          if (rowData.job_shared_cost == null) $(td).children('.confirm-job').hide()
        },
        defaultContent: `<i class='fa fa-pencil edit-group action-btn' title='modify'  style='cursor: pointer'></i>
        <i class='fa fa-check confirm-job action-btn' title='confirm' style='cursor: pointer' ></i>
        <i class='fa fa-trash delete-job action-btn' title='delete' style='cursor: pointer' ></i>`
      }
    ],
    rowCallback: function (row, data, index, cells) {
      $('td', row).css('background-color', data.job_color)
    },
    order: [
      [1, 'desc'],
      [2, 'asc']
    ]
  })

  self.consolidationsTable.on('click', 'p.piece-info', function () {
    const rowData = self.consolidationsTable.row($(this).closest('tr')).data()
    self.Helpers.swalPieceInfo(rowData.job_id)
  })

  self.consolidationsTable.on('click', 'i.edit-group', function () {
    const rowData = self.consolidationsTable.row($(this).closest('tr')).data()
    if (!self.Helpers.checkIfUserHasPriviledges(rowData.user.user_username)) {
      self.Helpers.swalUserPermissionError()
      return
    }
    self.currentJob = rowData
    self.renderConsolidationModal(rowData.job_consolidation_group_id)
  })

  self.consolidationsTable.on('click', 'i.confirm-job', function () {
    const rowData = self.consolidationsTable.row($(this).closest('tr')).data()
    if (!self.Helpers.checkIfUserHasPriviledges(rowData.user.user_username)) {
      self.Helpers.swalUserPermissionError()
      return
    }
    Swal.fire({
      title: ' Confirm Consolidation?',
      text: `Are you sure you want to confirm consolidation ${rowData.job_consolidation_group_id}?`,
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      showLoaderOnConfirm: true,
      confirmButtonColor: '#dc3545',
      confirmButtonText: 'Confirm',
      preConfirm: async () => {
        const response = await self.confirmConsolidation(rowData.job_consolidation_group_id)
        if (!response) throw 'Error'
      }
    }).then(result => {
      if (result.isConfirmed) {
        Swal.fire('Confirmed!', `Consolidation ${rowData.job_consolidation_group_id} is confirmed!`, 'success').then(result => {
          if (result.isConfirmed) window.location.replace('doneconsolidations')
        })
      }
    })
  })

  self.consolidationsTable.on('click', 'i.delete-job', function () {
    const rowData = self.consolidationsTable.row($(this).closest('tr')).data()
    if (!self.Helpers.checkIfUserHasPriviledges(rowData.user.user_username)) {
      self.Helpers.swalUserPermissionError()
      return
    }
    Swal.fire({
      title: ' Delete Job?',
      text: `Are you sure you want to delete job ${rowData.job_id}?`,
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      showLoaderOnConfirm: true,
      confirmButtonColor: '#dc3545',
      confirmButtonText: 'Confirm',
      preConfirm: async () => {
        const response = await self.deleteConsolidatedJob(rowData.job_id)
        if (!response) throw 'Error'
      }
    }).then(result => {
      if (result.isConfirmed) {
        Swal.fire('Confirmed!', `Consolidation ${rowData.job_id} is deleted!`, 'success').then(result => {
          if (result.isConfirmed) window.location.replace('doneindividuals')
        })
      }
    })
  })

  self.Helpers.applyMouseInteractions('consolidations-table')
  self.Helpers.applySearchInteraction(self.consolidationsTable)
}

ConsolidationsClass.prototype.initConsolidations = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/jobs/consolidations', { headers: self.Helpers.getHeaders() })
    let tempJobs = response.data
    self.consolidations = []
    self.consolidationGroups = {}
    for (let i = 0; i < tempJobs.length; i++) {
      tempJobs[i].request_date = self.Helpers.changeMysqlDateToNormal(tempJobs[i].job_created_at)
      tempJobs[i].service_description = ''
      tempJobs[i].destination_description = ''
      tempJobs[i].mode_description = ''
      if (_.has(tempJobs[i], 'service_type.service_type_description')) tempJobs[i].service_description = tempJobs[i].service_type.service_type_description
      if (_.has(tempJobs[i], 'to.city_name')) tempJobs[i].destination_description = tempJobs[i].to.city_name
      if (_.has(tempJobs[i], 'mode.mode_description')) tempJobs[i].mode_description = tempJobs[i].mode.mode_description
      self.consolidations.push(tempJobs[i])

      // Handle consolidations groups info
      if (!_.has(self.consolidationGroups, tempJobs[i].job_consolidation_group_id)) self.consolidationGroups[tempJobs[i].job_consolidation_group_id] = { hasLocal: false, hasGrouped: false, hasIndividual: false }
      switch (tempJobs[i].job_type) {
        case 'Local Order':
          self.consolidationGroups[tempJobs[i].job_consolidation_group_id].hasLocal = true
          break
        case 'Individual':
          self.consolidationGroups[tempJobs[i].job_consolidation_group_id].hasIndividual = true
          break
        case 'Grouped':
          self.consolidationGroups[tempJobs[i].job_consolidation_group_id].hasGrouped = true
          break
        default:
          break
      }
    }
    console.log(`Consolidations initialized`)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.doneJobs = []
  }
}

ConsolidationsClass.prototype.refreshTable = function (tableId) {
  let self = this
  $(`#${tableId}`).unbind('click')
  $(`#${tableId}`).DataTable().clear()
  $(`#${tableId}`).DataTable().destroy()
  if (tableId == 'consolidations-table') self.initConsolidationsTable()
  if (tableId == 'con-group-prices-table') self.initPricesTable()
}

ConsolidationsClass.prototype.initPricesTable = async function () {
  let self = this
  const tableColumns = [
    { title: 'ID', orderable: false, data: 'con_price_id' },
    { title: 'Forwarder', orderable: false, data: 'forwarder.forwarder_name' },
    { title: 'Ind. Price', visible: false, orderable: false, data: 'con_price_individuals_value' },
    { title: 'Local Price', visible: false, orderable: false, data: 'con_price_locals_value' },
    { title: 'Group Price', visible: false, orderable: false, data: 'con_price_grouped_value' },
    { title: 'Total Price ($)', orderable: false, data: 'con_price_base_value' },
    { title: 'Currency', orderable: false, data: 'con_price_currency_name' },
    {
      title: 'Offer Requested',
      orderable: false,
      data: 'con_price_offer_requested',
      createdCell: function (td, cellData, rowData, row, col) {
        if (rowData.con_price_offer_requested == true) {
          $(td).css('color', 'green').css('text-align', 'center')
          $(td).html(`<i class="fa fa-check action-btn"></i>`)
        } else {
          $(td).css('color', 'red').css('text-align', 'center')
          $(td).html(`<i class="fa fa-times action-btn"></i>`)
        }
      }
    },
    {
      title: 'Actions',
      orderable: false,
      data: null,
      createdCell: function (td, cellData, rowData, row, col) {
        if (rowData.con_price_base_value == null) $(td).children('.select-price').hide()
        if (rowData.con_price_selected == 1) {
          $(td).children('.select-price').hide()
          $(td).children('.unselect-price').show()
        } else {
          $(td).children('.select-price').show()
          $(td).children('.unselect-price').hide()
        }
      },
      defaultContent: `<i class='fa fa-pencil edit-price action-btn' title='modify'  style='cursor: pointer'></i>
      <i class='fa fa-envelope-o mail-price action-btn' title='request-offer' style='cursor: pointer' ></i>
      <i class='fa fa-check select-price action-btn' title='select' style='cursor: pointer' ></i>
      <i class='fa fa-times unselect-price action-btn' title='unselect' style='cursor: pointer' ></i>
      <i class='fa fa-trash delete-price action-btn' title='delete' style='cursor: pointer' ></i>`
    }
  ]

  self.currentJobPrices = []
  if (self.currentJob != null) {
    const consolidationGroupId = self.currentJob.con_group_id
    self.currentJobPrices = await self.getconGroupPrices(consolidationGroupId)
    if (self.consolidationGroups[self.currentJob.con_group_id].hasIndividual) tableColumns[2].visible = true
    if (self.consolidationGroups[self.currentJob.con_group_id].hasLocal) tableColumns[3].visible = true
    if (self.consolidationGroups[self.currentJob.con_group_id].hasGrouped) tableColumns[4].visible = true
  }
  self.refreshForwardersSelect(true)

  self.pricesTable = $('#con-group-prices-table').DataTable({
    data: self.currentJobPrices,
    bLengthChange: false,
    paging: false,
    ordering: false,
    info: false,
    columns: tableColumns,
    rowCallback: function (row, data, index, cells) {
      if (data.con_price_selected) $('td', row).css('background-color', '#8fbc8f')
    }
  })

  self.pricesTable.on('click', 'i.edit-price', function () {
    const rowData = self.pricesTable.row($(this).closest('tr')).data()
    self.refreshForwardersSelect(false)
    self.currentPrice = rowData
    $('#forwarder-select')[0].selectize.setValue(rowData.forwarder.forwarder_id)
    $('#forwarder-select')[0].selectize.disable()
    $('#currency-select')[0].selectize.setValue(rowData.con_price_currency_name)
    $('#local-price-value').val(rowData.con_price_locals_value)
    $('#individual-price-value').val(rowData.con_price_individuals_value)
    $('#group-price-value').val(rowData.con_price_grouped_value)
    $('#notes-div').hide()
    $('#save-notes-btn').hide()
    $('#request-con-group-offer-btn').hide()
    $('#add-con-group-price-btn').hide()

    $('#add-price-div').show()
  })

  self.pricesTable.on('click', 'i.delete-price', function () {
    const rowData = self.pricesTable.row($(this).closest('tr')).data()
    Swal.fire({
      title: 'Delete Price?',
      text: `Are you sure you want to delete price from ${rowData.forwarder.forwarder_name}?`,
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonColor: '#dc3545',
      confirmButtonText: 'Confirm'
    }).then(async result => {
      if (result.isConfirmed) {
        const priceDeleted = await self.deletePrice(rowData.con_price_id)
        if (priceDeleted) {
          self.refreshTable('con-group-prices-table')
        }
      }
    })
  })

  self.pricesTable.on('click', 'i.mail-price', function () {
    const rowData = self.pricesTable.row($(this).closest('tr')).data()
    Swal.fire({
      title: 'Request Offer?',
      text: `Are you sure you want to request offer for this conslidation from ${rowData.forwarder.forwarder_name}? An email will be sent to ${rowData.forwarder.forwarder_email}. `,
      icon: 'info',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonColor: '#dc3545',
      confirmButtonText: 'Confirm'
    }).then(async result => {
      if (result.isConfirmed) {
        const priceOfferRequested = await self.requestOfferForConsolidation(rowData.con_price_id)
        if (priceOfferRequested) {
          self.refreshTable('con-group-prices-table')
        }
      }
    })
  })

  self.pricesTable.on('click', 'i.select-price', async function () {
    const rowData = self.pricesTable.row($(this).closest('tr')).data()
    const emptyPrice = self.currentJobPrices.find(el => el.con_price_base_value == null)
    if (emptyPrice) {
      Swal.fire({
        title: 'Empty Prices Exist!',
        text: `Some prices aren't filled up for this consolidation. Are you sure you want to select price from ${rowData.forwarder.forwarder_name} regardless?`,
        icon: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancel',
        confirmButtonColor: '#dc3545',
        confirmButtonText: 'Confirm'
      }).then(async result => {
        if (result.isConfirmed) {
          await self.selectConsolidationPrice(rowData.con_price_id)
          self.refreshTable('con-group-prices-table')
        }
      })
    } else {
      await self.selectConsolidationPrice(rowData.con_price_id)
      self.refreshTable('con-group-prices-table')
    }
  })

  self.pricesTable.on('click', 'i.unselect-price', async function () {
    const rowData = self.pricesTable.row($(this).closest('tr')).data()
    await self.selectConsolidationPrice(rowData.con_price_id, 'unSelectJob')
    self.refreshTable('con-group-prices-table')
  })
}

ConsolidationsClass.prototype.initCities = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/cities', { headers: self.Helpers.getHeaders() })
    self.cities = response.data
    for (i = 0; i < self.cities.length; i++) {
      $('#group-ex-select')[0].selectize.addOption({ text: `${self.cities[i].city_name} - ${self.cities[i].country_name}`, value: self.cities[i].city_id })
      $('#group-to-select')[0].selectize.addOption({ text: `${self.cities[i].city_name} - ${self.cities[i].country_name}`, value: self.cities[i].city_id })
    }
    console.log(`Cities initialized`)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.cities = []
  }
}

ConsolidationsClass.prototype.initModes = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/modes', { headers: self.Helpers.getHeaders() })
    self.modes = response.data
    for (i = 0; i < self.modes.length; i++) {
      if (self.modes[i].mode_id == 5) continue
      $('#mode-select')[0].selectize.addOption({ text: self.modes[i].mode_description, value: self.modes[i].mode_id })
    }
    console.log(`Modes initialized`)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.modes = []
  }
}

ConsolidationsClass.prototype.initServiceTypes = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/servicetypes', { headers: self.Helpers.getHeaders() })
    for (let serviceType of response.data) {
      if (serviceType.service_type_group !== 'Consolidations') continue
      $('#service-type-select')[0].selectize.addOption({ text: serviceType.service_type_description, value: serviceType.service_type_id })
    }

    console.log(`Service Types initialized`)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.vessels = []
  }
}

ConsolidationsClass.prototype.initVessels = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/vessels', { headers: self.Helpers.getHeaders() })
    self.vessels = response.data
    console.log(`Vessels initialized`)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.vessels = []
  }
}

ConsolidationsClass.prototype.renderConsolidationModal = async function (consolidationId) {
  let self = this
  $('#consolidation-modal-title').html(`Consolidation ID: ${consolidationId}`)
  self.currentJob = await self.Helpers.getConsolidationInfo(consolidationId)
  self.handleModalTabs()
  self.renderTransportationTab()
  self.renderPricesTab(true)

  if (!$('#consolidation-modal').hasClass('show')) $('#consolidation-modal').modal('show')
}

ConsolidationsClass.prototype.handleModalTabs = async function () {
  let self = this
  self.Wizzard.showTab('transportation')
  self.Wizzard.focusTab(0)
  if (self.currentJob.con_group_to != null && self.currentJob.con_group_mode != null && self.currentJob.con_group_deadline != null && self.currentJob.con_group_service_type != null) {
    self.Wizzard.showTab('forwarders')
  } else {
    self.Wizzard.hideTab('forwarders')
  }
}

ConsolidationsClass.prototype.renderTransportationTab = function () {
  let self = this
  $('#group-ex-select')[0].selectize.setValue(self.currentJob.con_group_ex)
  $('#group-to-select')[0].selectize.setValue(self.currentJob.con_group_to)
  $('#group-deadline').val(self.currentJob.con_group_deadline)
  $('#service-type-select')[0].selectize.setValue(self.currentJob.con_group_service_type)
  $('#mode-select')[0].selectize.setValue(self.currentJob.con_group_mode)
}

ConsolidationsClass.prototype.renderPricesTab = function (hideInputs = true) {
  let self = this
  self.initPricesInputs()
  self.refreshTable('con-group-prices-table')
  $('#notes').val(self.currentJob.con_group_notes)
  $('#save-notes-btn').show()
  $('#request-con-group-offer-btn').show()
  $('#add-con-group-price-btn').show()
  if (hideInputs) $('#add-price-div').hide()
}

ConsolidationsClass.prototype.initPricesInputs = function () {
  let self = this
  $('#forwarder-select')[0].selectize.enable()

  self.refreshForwardersSelect(true)
  $('#forwarder-select')[0].selectize.setValue('')

  const currentGroup = self.consolidationGroups[self.currentJob.con_group_id]

  if (!currentGroup.hasLocal) $('#local-price-div').hide()
  else $('#local-price-div').show()

  if (!currentGroup.hasIndividual) $('#individual-price-div').hide()
  else $('#individual-price-div').show()

  if (!currentGroup.hasGrouped) $('#group-price-div').hide()
  else $('#group-price-div').show()
}

ConsolidationsClass.prototype.managePricesInputs = function () {
  let self = this
}

ConsolidationsClass.prototype.updateConsolidationsGroup = async function (conGroup) {
  let self = this

  try {
    await axios.put(self.Helpers.API_URL + `/consolidationGroups`, conGroup, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Consolidation Updated!')
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

ConsolidationsClass.prototype.patchConsolidationGroup = async function (consolidationId, conGroup) {
  let self = this

  try {
    await axios.patch(self.Helpers.API_URL + `/consolidationGroups/${consolidationId}`, conGroup, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Consolidation Updated!')
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

ConsolidationsClass.prototype.getconGroupPrices = async function (conGroupId) {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + `/consolidationPrices/consolidation/${conGroupId}`, { headers: self.Helpers.getHeaders() })
    return response.data
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    return []
  }
}

ConsolidationsClass.prototype.initForwarders = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/forwarders', { headers: self.Helpers.getHeaders() })
    self.forwarders = response.data
    for (i = 0; i < self.forwarders.length; i++) {
      $('#forwarder-select')[0].selectize.addOption({ text: self.forwarders[i].forwarder_name, value: self.forwarders[i].forwarder_id })
    }
    console.log(`Forwarders initialized`)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.forwarders = []
  }
}

ConsolidationsClass.prototype.initCountries = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/countries', { headers: self.Helpers.getHeaders() })
    self.countries = response.data

    console.log(`Countries initialized`)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.forwarders = []
  }
}

ConsolidationsClass.prototype.addConsolidationPrice = async function (price) {
  let self = this
  try {
    const response = await axios.post(self.Helpers.API_URL + '/consolidationPrices', price, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Price Added')
    return response.data.con_price_id
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    return null
  }
}

ConsolidationsClass.prototype.updateConslidationPrice = async function (price) {
  let self = this
  try {
    const response = await axios.put(self.Helpers.API_URL + '/consolidationPrices', price, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Price Updated')
    return response.data.con_price_id
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    return null
  }
}

ConsolidationsClass.prototype.requestOfferForConsolidation = async function (priceId) {
  let self = this
  try {
    await axios.get(self.Helpers.API_URL + `/consolidationPrices/${priceId}/requestOffer`, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Price Offer Requested!')
    return true
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    return false
  }
}

ConsolidationsClass.prototype.initCurrencies = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/currencies', { headers: self.Helpers.getHeaders() })
    self.currencies = response.data
    for (i = 0; i < self.currencies.length; i++) {
      $('#currency-select')[0].selectize.addOption({ text: self.currencies[i].currency_name, value: self.currencies[i].currency_name })
    }
    console.log(`Currencies initialized`)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.currencies = []
  }
}

ConsolidationsClass.prototype.deletePrice = async function (priceId) {
  let self = this
  try {
    await axios.delete(self.Helpers.API_URL + `/consolidationPrices/${priceId}`, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Price Deleted!')
    return true
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    return false
  }
}

ConsolidationsClass.prototype.selectConsolidationPrice = async function (priceId, verb = `selectJob`) {
  let self = this
  try {
    await axios.get(self.Helpers.API_URL + `/consolidationPrices/${priceId}/${verb}`, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Price Updated!')
    return true
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    return false
  }
}

ConsolidationsClass.prototype.confirmConsolidation = async function (consolidationId) {
  let self = this
  try {
    await axios.get(self.Helpers.API_URL + `/consolidationGroups/${consolidationId}/confirm`, { headers: self.Helpers.getHeaders() })
    return true
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    return false
  }
}

ConsolidationsClass.prototype.deleteConsolidatedJob = async function (jobId) {
  let self = this
  try {
    await axios.delete(self.Helpers.API_URL + `/jobs/consolidated/${jobId}`, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Job Deleted!')
    return true
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    return false
  }
}

ConsolidationsClass.prototype.refreshForwardersSelect = function (banForwarder = true) {
  let self = this
  $('#forwarder-select')[0].selectize.destroy()
  $('#forwarder-select').selectize()
  for (i = 0; i < self.forwarders.length; i++) {
    if (banForwarder) {
      let foundForwarder = self.currentJobPrices.find(el => el.forwarder.forwarder_id == self.forwarders[i].forwarder_id)
      if (foundForwarder) continue
    }

    $('#forwarder-select')[0].selectize.addOption({ text: self.forwarders[i].forwarder_name, value: self.forwarders[i].forwarder_id })
  }
}

new ConsolidationsClass()
