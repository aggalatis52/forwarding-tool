let ArchiveConsolidationsClass = function () {
  this.Helpers = new HelpersClass()
  this.Helpers.validateLogin()
  this.Helpers.bindCloseBtnsAlerts()
  this.Helpers.bindMovingEvents('consolidation-modal-mover', 'consolidation-modal', 'consolidation-modal-content')
  this.Wizzard = new WizzardClass(['transportation', 'pieces', 'forwarders', 'documents'])
  this.consolidations = []
  this.forwarders = []
  this.countires = []
  this.consolidationsTable = null
  this.currentPrice = null

  this.bindEventsOnButtons()
  this.initCities()
  this.initForwarders()
  this.initModes()
  this.initVessels()
  this.initCountries()
  this.initServiceTypes()
  this.initDoneConsolidationsTable()
  this.initPricesTable()
  this.initDocumentsTable()
}

ArchiveConsolidationsClass.prototype.bindEventsOnButtons = function () {
  let self = this

  $('#mode-select').selectize()
  $('#group-ex-select').selectize()
  $('#group-to-select').selectize()
  $('#service-type-select').selectize()
}

ArchiveConsolidationsClass.prototype.initDoneConsolidationsTable = async function () {
  let self = this
  self.visitedGroups = []
  await self.initDoneConsolidations()
  self.consolidationsTable = $('#done-consolidations-table').DataTable({
    data: self.consolidations,
    bLengthChange: false,
    paging: false,
    ordering: false,
    info: false,
    fixedHeader: {
      headerOffset: 100,
      header: true,
      footer: false
    },
    columns: [
      {
        title: 'JOB ID',
        orderable: false,
        data: 'job_id',
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.job_display_id != null) $(td).html(rowData.job_display_id)
        }
      },
      {
        title: 'CON. ID',
        orderable: false,
        data: 'job_consolidation_group_id',
        createdCell: function (td, cellData, rowData, row, col) {
          $(td).css('background-color', rowData.job_color).css('font-weight', 'bold')
        }
      },
      {
        title: 'TYPE',
        orderable: false,
        data: 'job_type',
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.job_type == 'Individual') $(td).css('color', self.Helpers.INDIVIDUAL_COLOR).css('font-weight', 'bold')
          if (rowData.job_type == 'Grouped') $(td).css('color', self.Helpers.GROUPED_COLOR).css('font-weight', 'bold')
          if (rowData.job_type == 'Personnel') $(td).css('color', self.Helpers.PERSONNEL_COLOR).css('font-weight', 'bold')
          if (rowData.job_type == 'Local Order') $(td).css('color', self.Helpers.LOCAL_ORDER_COLOR).css('font-weight', 'bold')
        }
      },
      { title: 'REQ. DATE', orderable: false, data: 'request_date' },
      { title: 'USER', orderable: false, data: 'user.user_username' },
      { title: 'DEPT.', orderable: false, data: 'department.department_description' },
      {
        title: 'PIECES',
        orderable: false,
        data: 'pieces_count',
        createdCell: function (td, cellData, rowData, row, col) {
          let text = 'Pieces'
          if (rowData.pieces_count == 1) text = 'Piece'
          $(td).html(`<p class='piece-info' style='cursor: pointer'>${rowData.pieces_count} ${text}</p>`)
        }
      },
      {
        title: 'WEIGHT (KG)',
        orderable: false,
        data: 'job_weight'
      },
      {
        title: 'HIGHLIGHTED',
        visible: false,
        data: 'job_flagged'
      },
      {
        title: 'MODE',
        orderable: false,
        createdCell: function (td, cellData, rowData, row, col) {
          $(td).css('color', 'blue').css('font-weight', 'bold')
        },
        data: 'mode_description',
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.mode_description == '') {
            $(td).html('<div class="empty-table-value" ></div>')
          }
        }
      },
      {
        title: 'SERVICE',
        orderable: false,
        data: 'service_description',
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.service_description == '') {
            $(td).html('<div class="empty-table-value" ></div>')
          }
        }
      },
      {
        title: 'VESSEL',
        orderable: false,
        className: 'danger-header',
        data: 'vessel.vessel_description'
      },
      { title: 'EX', orderable: false, className: 'danger-header', data: 'ex.city_name' },
      {
        title: 'TO',
        orderable: false,
        className: 'danger-header',
        data: 'destination_description',
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.destination_description == '') {
            $(td).html('<div class="empty-table-value" ></div>')
          }
        }
      },
      { title: 'DEADLINE', orderable: false, className: 'danger-header', data: 'job_deadline' },
      { title: 'CONF. DATE', orderable: false, data: 'confirmation_date' },
      {
        title: 'Forwarder',
        data: 'job_forwarder_id',
        orderable: false,
        createdCell: function (td, cellData, rowData, row, col) {
          let forwarderId = rowData.job_forwarder_id
          let foundFowarder = self.forwarders.find(el => el.forwarder_id == forwarderId)
          if (!foundFowarder) return
          $(td).html(foundFowarder.forwarder_name)
        }
      },
      {
        title: 'SHARED COST ($)',
        data: 'job_shared_cost',
        orderable: false,
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.job_shared_cost == null) {
            $(td).html('<div class="empty-table-value"></div>')
          }
        }
      },
      {
        title: 'ACTIONS',
        orderable: false,
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.job_proof_of_delivery == true) $(td).children('.reconsolidate').hide()
        },
        defaultContent: `
            <i class='fa fa-pencil edit-group action-btn' title='group' style='cursor: pointer'></i>`
      }
    ],
    createdRow: function (row, data, index, cells) {
      if (data.job_flagged == 1) {
        $('td', row).css('border-top', '2px solid red').css('border-bottom', '2px solid red')
        $('td:eq(0)', row).css('border-left', '2px solid red')
        $('td:eq(17)', row).css('border-right', '2px solid red')
      }
    }
  })

  self.consolidationsTable.on('click', 'p.piece-info', function () {
    const rowData = self.consolidationsTable.row($(this).closest('tr')).data()
    self.Helpers.swalPieceInfo(rowData.job_id)
  })

  self.consolidationsTable.on('click', 'i.flag-job', function () {
    const rowData = self.consolidationsTable.row($(this).closest('tr')).data()
    if (!self.Helpers.checkIfUserHasPriviledges(rowData.user.user_username)) {
      self.Helpers.swalUserPermissionError()
      return
    }
    let highligh = 'FLAG'
    if (rowData.job_flagged == 1) highligh = 'UN-FLAG'

    Swal.fire({
      title: `${highligh} Job?`,
      text: `Are you sure you want to ${highligh.toLowerCase()} JOB: ${rowData.job_id}?`,
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonColor: '#dc3545',
      confirmButtonText: 'Confirm'
    }).then(result => {
      if (result.isConfirmed) {
        const job = {
          job_flagged: rowData.job_flagged == 1 ? 0 : 1
        }
        self.patchJob(rowData.job_id, job)
        self.refreshTable('done-consolidations-table')
      }
    })
  })

  self.consolidationsTable.on('click', 'i.edit-group', function () {
    const rowData = self.consolidationsTable.row($(this).closest('tr')).data()
    if (!self.Helpers.checkIfUserHasPriviledges(rowData.user.user_username)) {
      self.Helpers.swalUserPermissionError()
      return
    }
    self.renderConsolidationModal(rowData.job_consolidation_group_id)
  })

  self.consolidationsTable.on('click', 'i.reconsolidate', function () {
    const rowData = self.consolidationsTable.row($(this).closest('tr')).data()
    if (!self.Helpers.checkIfUserHasPriviledges(rowData.user.user_username)) {
      self.Helpers.swalUserPermissionError()
      return
    }

    Swal.fire({
      title: 'Re-Consolidate?',
      text: `Are you sure you want to Re-Consolidate consolidation: ${rowData.job_consolidation_group_id}?`,
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonColor: '#dc3545',
      showLoaderOnConfirm: true,
      confirmButtonText: 'Confirm',
      preConfirm: async () => {
        const response = await self.consolidationGroupReConsolidate(rowData.job_consolidation_group_id)
        if (!response) throw 'Error'
      }
    }).then(result => {
      if (result.isConfirmed) {
        Swal.fire('Re-Consolidated!', `Consolidation ${rowData.job_consolidation_group_id} is re-consolidated!`, 'success').then(result => {
          if (result.isConfirmed) window.location.replace('consolidations')
        })
      }
    })
  })

  self.Helpers.applyMouseInteractions('done-consolidations-table')
  self.Helpers.applySearchInteraction(self.consolidationsTable)
}

ArchiveConsolidationsClass.prototype.initDoneConsolidations = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/jobs/archiveConsolidations', { headers: self.Helpers.getHeaders() })
    let tempJobs = response.data
    self.consolidations = []
    self.consolidationGroups = {}
    for (let i = 0; i < tempJobs.length; i++) {
      tempJobs[i].request_date = self.Helpers.changeMysqlDateToNormal(tempJobs[i].job_created_at)
      tempJobs[i].confirmation_date = self.Helpers.changeMysqlDateToNormal(tempJobs[i].job_consolidation_confirmed_at)
      tempJobs[i].service_description = ''
      tempJobs[i].destination_description = ''
      tempJobs[i].mode_description = ''
      if (_.has(tempJobs[i], 'service_type.service_type_description')) tempJobs[i].service_description = tempJobs[i].service_type.service_type_description
      if (_.has(tempJobs[i], 'to.city_name')) tempJobs[i].destination_description = tempJobs[i].to.city_name
      if (_.has(tempJobs[i], 'mode.mode_description')) tempJobs[i].mode_description = tempJobs[i].mode.mode_description
      self.consolidations.push(tempJobs[i])

      // Handle consolidations groups info
      if (!_.has(self.consolidationGroups, tempJobs[i].job_consolidation_group_id)) self.consolidationGroups[tempJobs[i].job_consolidation_group_id] = { hasLocal: false, hasGrouped: false, hasIndividual: false }
      switch (tempJobs[i].job_type) {
        case 'Local Order':
          self.consolidationGroups[tempJobs[i].job_consolidation_group_id].hasLocal = true
          break
        case 'Individual':
          self.consolidationGroups[tempJobs[i].job_consolidation_group_id].hasIndividual = true
          break
        case 'Grouped':
          self.consolidationGroups[tempJobs[i].job_consolidation_group_id].hasGrouped = true
          break
        default:
          break
      }
    }
    console.log(`Consolidations initialized`)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.doneJobs = []
  }
}

ArchiveConsolidationsClass.prototype.refreshTable = function (tableId) {
  let self = this
  $(`#${tableId}`).unbind('click')
  $(`#${tableId}`).DataTable().clear()
  $(`#${tableId}`).DataTable().destroy()

  if (tableId == 'done-consolidations-table') self.initDoneConsolidationsTable()
  if (tableId == 'con-group-prices-table') self.initPricesTable()
  if (tableId == 'documents-table') self.initDocumentsTable()
}

ArchiveConsolidationsClass.prototype.initPricesTable = async function () {
  let self = this
  const tableColumns = [
    { title: 'ID', orderable: false, data: 'con_price_id' },
    { title: 'Forwarder', orderable: false, data: 'forwarder.forwarder_name' },
    { title: 'Ind. Price', visible: false, orderable: false, data: 'con_price_individuals_value' },
    { title: 'Local Price', visible: false, orderable: false, data: 'con_price_locals_value' },
    { title: 'Group Price', visible: false, orderable: false, data: 'con_price_grouped_value' },
    { title: 'Total Price ($)', orderable: false, data: 'con_price_base_value' },
    { title: 'Currency', orderable: false, data: 'con_price_currency_name' },
    {
      title: 'Offer Requested',
      orderable: false,
      data: 'con_price_offer_requested',
      createdCell: function (td, cellData, rowData, row, col) {
        if (rowData.con_price_offer_requested == true) {
          $(td).css('color', 'green').css('text-align', 'center')
          $(td).html(`<i class="fa fa-check action-btn"></i>`)
        } else {
          $(td).css('color', 'red').css('text-align', 'center')
          $(td).html(`<i class="fa fa-times action-btn"></i>`)
        }
      }
    }
  ]

  self.currentJobPrices = []
  if (self.currentJob != null) {
    const consolidationGroupId = self.currentJob.con_group_id
    self.currentJobPrices = await self.getconGroupPrices(consolidationGroupId)
    if (self.consolidationGroups[self.currentJob.con_group_id].hasIndividual) tableColumns[2].visible = true
    if (self.consolidationGroups[self.currentJob.con_group_id].hasLocal) tableColumns[3].visible = true
    if (self.consolidationGroups[self.currentJob.con_group_id].hasGrouped) tableColumns[4].visible = true
  }

  self.pricesTable = $('#con-group-prices-table').DataTable({
    data: self.currentJobPrices,
    bLengthChange: false,
    paging: false,
    ordering: false,
    info: false,
    columns: tableColumns,
    rowCallback: function (row, data, index, cells) {
      if (data.con_price_selected) $('td', row).css('background-color', '#8fbc8f')
    }
  })
}

ArchiveConsolidationsClass.prototype.initDocumentsTable = async function () {
  let self = this
  self.currnetJobDocuments = []
  if (self.currentJob != null) self.currnetJobDocuments = await self.getConsolidationDocuments(self.currentJob.con_group_id)
  self.documentsTable = $('#documents-table').DataTable({
    data: self.currnetJobDocuments,
    bLengthChange: false,
    paging: false,
    ordering: false,
    info: false,
    columns: [
      { title: 'ID', orderable: false, data: 'document_id' },
      { title: 'File Name', orderable: false, data: 'document_original_name' },
      { title: 'Upload Date', orderable: false, data: 'document_created_at' },
      {
        title: 'Actions',
        orderable: false,
        defaultContent: "<i class='fa fa-download download-file action-btn' title='download' style='cursor: pointer'></i><i class='fa fa-trash delete-document action-btn' title='delete' style='cursor: pointer' ></i>"
      }
    ]
  })

  self.documentsTable.on('click', 'i.delete-document', function () {
    const rowData = self.documentsTable.row($(this).closest('tr')).data()

    Swal.fire({
      title: ' Delete Document?',
      text: `Are you sure you want to delete ${rowData.document_original_name}?`,
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonColor: '#dc3545',
      confirmButtonText: 'Confirm'
    }).then(result => {
      if (result.isConfirmed) self.deleteDocument(rowData.document_id)
    })
  })

  self.documentsTable.on('click', 'i.download-file', function () {
    const rowData = self.documentsTable.row($(this).closest('tr')).data()
    self.downloadDocument(rowData.document_id, rowData.document_original_name)
  })
}

ArchiveConsolidationsClass.prototype.getConsolidationDocuments = async function (consolidationId) {
  let self = this
  try {
    let documentsURL = `/documents/group/${consolidationId}/4`
    const response = await axios.get(self.Helpers.API_URL + documentsURL, { headers: self.Helpers.getHeaders() })
    return response.data
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

ArchiveConsolidationsClass.prototype.initCities = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/cities', { headers: self.Helpers.getHeaders() })
    self.cities = response.data
    for (i = 0; i < self.cities.length; i++) {
      $('#group-ex-select')[0].selectize.addOption({ text: `${self.cities[i].city_name} - ${self.cities[i].country_name}`, value: self.cities[i].city_id })
      $('#group-to-select')[0].selectize.addOption({ text: `${self.cities[i].city_name} - ${self.cities[i].country_name}`, value: self.cities[i].city_id })
    }
    console.log(`Cities initialized`)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.cities = []
  }
}

ArchiveConsolidationsClass.prototype.initCountries = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/countries', { headers: self.Helpers.getHeaders() })
    self.countries = response.data

    console.log(`Countries initialized`)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.countries = []
  }
}

ArchiveConsolidationsClass.prototype.initForwarders = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/forwarders', { headers: self.Helpers.getHeaders() })
    self.forwarders = response.data

    console.log(`Forwarders initialized`)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.forwarders = []
  }
}

ArchiveConsolidationsClass.prototype.initModes = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/modes', { headers: self.Helpers.getHeaders() })
    self.modes = response.data
    for (i = 0; i < self.modes.length; i++) {
      if (self.modes[i].mode_id == 5) continue
      $('#mode-select')[0].selectize.addOption({ text: self.modes[i].mode_description, value: self.modes[i].mode_id })
    }
    console.log(`Modes initialized`)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.modes = []
  }
}

ArchiveConsolidationsClass.prototype.initServiceTypes = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/servicetypes', { headers: self.Helpers.getHeaders() })
    for (let serviceType of response.data) {
      if (serviceType.service_type_group !== 'Consolidations') continue
      $('#service-type-select')[0].selectize.addOption({ text: serviceType.service_type_description, value: serviceType.service_type_id })
    }

    console.log(`Service Types initialized`)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.vessels = []
  }
}

ArchiveConsolidationsClass.prototype.initVessels = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/vessels', { headers: self.Helpers.getHeaders() })
    self.vessels = response.data
    console.log(`Vessels initialized`)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.vessels = []
  }
}

ArchiveConsolidationsClass.prototype.renderConsolidationModal = async function (consolidationId) {
  let self = this
  $('#consolidation-modal-title').html(`Consolidation ID: ${consolidationId}`)
  self.currentJob = await self.Helpers.getConsolidationInfo(consolidationId)
  self.Wizzard.showTab('transportation')
  self.Wizzard.showTab('forwarders')
  self.Wizzard.focusTab(0)

  self.renderTransportationTab()
  self.refreshTable('con-group-prices-table')
  self.refreshTable('documents-table')
  if (!$('#consolidation-modal').hasClass('show')) $('#consolidation-modal').modal('show')
}

ArchiveConsolidationsClass.prototype.renderTransportationTab = function () {
  let self = this
  $('#group-ex-select')[0].selectize.setValue(self.currentJob.con_group_ex)
  $('#group-to-select')[0].selectize.setValue(self.currentJob.con_group_to)
  $('#group-deadline').val(self.currentJob.con_group_deadline)
  $('#service-type-select')[0].selectize.setValue(self.currentJob.con_group_service_type)
  $('#mode-select')[0].selectize.setValue(self.currentJob.con_group_mode)
}

ArchiveConsolidationsClass.prototype.getconGroupPrices = async function (conGroupId) {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + `/consolidationPrices/consolidation/${conGroupId}`, { headers: self.Helpers.getHeaders() })
    return response.data
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    return []
  }
}

ArchiveConsolidationsClass.prototype.downloadDocument = async function (documentId, documentName) {
  let self = this
  try {
    await self.Helpers.downloadFile(self.Helpers.API_URL + `/documents/download/${documentId}`, documentName)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.vessels = []
  }
}

new ArchiveConsolidationsClass()
