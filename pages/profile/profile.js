let ProfileClass = function () {
  this.departments = []
  this.user = null
  this.Validations = new ValidationsClass()
  this.Helpers = new HelpersClass()
  this.Helpers.validateLogin()
  this.initPage()
}

ProfileClass.prototype.initPage = async function () {
  let self = this

  $('#department-select').selectize()

  await self.initDepartments()
  await self.initProfile()

  if (self.Helpers.user.user_role_id === 5) $('#restore-system').show()

  $('#save-profile').on('click', function (e) {
    e.preventDefault()
    let userData = {
      user_id: self.Helpers.user.user_id,
      user_username: $('#username').val(),
      departments: $('#department-select').val(),
      user_role_id: self.Helpers.user.user_role_id,
      user_email: $('#email').val(),
      user_fullname: $('#fullname').val(),
      user_password: $('#password').val(),
      user_password_repeat: $('#repeat-password').val()
    }

    let validationErrors = self.Validations.isUserValid(userData)
    if (validationErrors) {
      self.Helpers.toastr('error', `Validation Errors: ${validationErrors.join(',')}`)
      return
    }
    self.updateUser(userData)
  })

  $('#restore-system').on('click', function (e) {
    e.preventDefault()
    Swal.fire({
      title: 'System Restore',
      text: `All current jobs will be lost permantly! The app will go back to previous stable state. ACCEPT ONLY IF THIS IS A DEMO!!`,
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonColor: '#dc3545',
      confirmButtonText: 'RESTORE SYSTEM'
    }).then(result => {
      if (result.isConfirmed) self.requestSystemRestore()
    })
  })
}

ProfileClass.prototype.initProfile = async function () {
  let self = this
  const userId = self.Helpers.user.user_id
  try {
    const response = await axios.get(self.Helpers.API_URL + `/users/${userId}`, { headers: self.Helpers.getHeaders() })
    const user = response.data
    let userDepartments = []
    for (let i = 0; i < user.departments.length; i++) {
      userDepartments.push(user.departments[i].department_id)
    }
    $('#department-select')[0].selectize.setValue(userDepartments)
    $('#username').val(user.user_username)
    $('#fullname').val(user.user_fullname)
    $('#email').val(user.user_email)
    $('#password').val('')
    $('#repeat-password').val('')
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.user = null
  }
}

ProfileClass.prototype.initDepartments = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/departments', { headers: self.Helpers.getHeaders() })
    self.departments = response.data
    for (i = 0; i < self.departments.length; i++) {
      $('#department-select')[0].selectize.addOption({ text: self.departments[i].department_description, value: self.departments[i].department_id })
    }
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.departments = []
  }
}

ProfileClass.prototype.updateUser = async function (user) {
  let self = this
  try {
    await axios.put(self.Helpers.API_URL + '/users', user, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'User Updated!')
    self.Helpers.handleLogout()
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

ProfileClass.prototype.requestSystemRestore = async function () {
  let self = this
  try {
    await axios.get(self.Helpers.API_URL + '/operations/systemrestore', { headers: self.Helpers.getHeaders() })
    $('#logout-ref').click()
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

new ProfileClass()
