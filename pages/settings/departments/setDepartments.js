let SettingsDepartmentsClass = function () {
  this.departments = []
  this.Helpers = new HelpersClass()
  this.Helpers.bindMovingEvents('department-modal-mover', 'department-modal', 'department-modal-content')
  this.Helpers.validateLogin()
  this.addEventsOnButtons()
  this.initTable()
}

SettingsDepartmentsClass.prototype.addEventsOnButtons = function () {
  let self = this

  $('#save-department').on('click', function () {
    let departmentData = {
      department_id: $('#department_id').val(),
      department_description: $('#department_description').val()
    }
    if (departmentData.department_description == '') {
      self.Helpers.toastr('error', 'Some fields are empty..')
      return
    }

    if (departmentData.department_id == 0) {
      delete departmentData.department_id
      self.createDepartment(departmentData)
    } else {
      self.updateDepartment(departmentData)
    }
  })

  $('#create-department').on('click', function () {
    $('#department_id').val(0)
    $('#department_description').val('')
    $('#department-modal').modal('show')
  })
}

SettingsDepartmentsClass.prototype.initTable = async function () {
  let self = this
  await self.initDepartments()
  let departmentsTable = $('#departments-table').DataTable({
    data: self.departments,
    fixedHeader: {
      headerOffset: 100,
      header: true,
      footer: false
    },
    bLengthChange: false,
    columns: [
      { title: 'ID', orderable: false, data: 'department_id' },
      { title: 'Description', orderable: false, data: 'department_description' },
      { title: 'Created At', orderable: false, data: 'department_created_at' },
      {
        title: 'Actions',
        orderable: false,
        defaultContent: "<i class='fa fa-pencil edit-job action-btn' title='modify' style='cursor: pointer' ></i><i class='fa fa-trash delete-job action-btn' title='delete' style='cursor: pointer' ></i>"
      }
    ],
    order: [[1, 'asc']],
    pageLength: 25
  })
  $('#departments-table').on('click', 'i.edit-job', function () {
    var data = departmentsTable.row($(this).closest('tr')).data()
    $('#department_id').val(data.department_id)
    $('#department_description').val(data.department_description)
    $('#department-modal').modal('show')
  })
  $('#departments-table').on('click', 'i.delete-job', function () {
    var data = departmentsTable.row($(this).closest('tr')).data()
    Swal.fire({
      title: 'Delete Department?',
      text: `Are you sure you want to delete ${data.department_description}? You won't be able to revert it!`,
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonColor: '#dc3545',
      confirmButtonText: 'Confirm'
    }).then(result => {
      if (result.isConfirmed) self.deleteDepartment(data.department_id)
    })
  })
}

SettingsDepartmentsClass.prototype.initDepartments = async function () {
  let self = this
  try {
    const departments = await axios.get(self.Helpers.API_URL + '/departments', { headers: self.Helpers.getHeaders() })
    for (let department of departments.data) {
      department.department_created_at = self.Helpers.changeMysqlDateToNormal(department.department_created_at)
      self.departments.push(department)
    }
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.departments = []
  }
}

SettingsDepartmentsClass.prototype.createDepartment = async function (department) {
  let self = this
  try {
    const cities = await axios.post(self.Helpers.API_URL + '/departments', { department_description: department.department_description }, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Department Created!')
    $('#department-modal').modal('hide')
    self.refreshTable()
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

SettingsDepartmentsClass.prototype.updateDepartment = async function (department) {
  let self = this
  try {
    await axios.put(self.Helpers.API_URL + '/departments', department, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Department Updated!')
    $('#department-modal').modal('hide')
    self.refreshTable()
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

SettingsDepartmentsClass.prototype.deleteDepartment = async function (department_id) {
  let self = this

  try {
    await axios.delete(`${self.Helpers.API_URL}/departments/${department_id}`, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Department Deleted!')
    self.refreshTable()
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

SettingsDepartmentsClass.prototype.refreshTable = function () {
  let self = this
  self.departments = []
  $('#departments-table').unbind('click')
  $('#departments-table').DataTable().clear()
  $('#departments-table').DataTable().destroy()
  self.initTable()
}

new SettingsDepartmentsClass()
