let SettingsServiceTypesClass = function () {
  this.serviceTypes = []
  this.Validations = new ValidationsClass()
  this.Helpers = new HelpersClass()
  this.Helpers.validateLogin()
  this.Helpers.bindMovingEvents('servicetype-modal-mover', 'servicetype-modal', 'servicetype-modal-content')
  this.addEventsOnButtons()
  this.initTable()
}

SettingsServiceTypesClass.prototype.addEventsOnButtons = function () {
  let self = this

  $('#service_type_group').append(new Option('Individuals', 'Individuals'))
  $('#service_type_group').append(new Option('Consolidations', 'Consolidations'))
  $('#service_type_group').selectize()

  $('#save-service-type').on('click', function () {
    let serviceTypeData = {
      service_type_id: parseInt($('#service_type_id').val()),
      service_type_description: $('#service_type_description').val(),
      service_type_group: $('#service_type_group').val()
    }
    let validationErrors = self.Validations.isServiceTypeValid(serviceTypeData)
    if (validationErrors) {
      self.Helpers.toastr('error', `Validation Errors: ${validationErrors.join(',')}`)
      return
    }

    if (serviceTypeData.service_type_id == 0) {
      delete serviceTypeData.service_type_id
      self.createServiceType(serviceTypeData)
    } else {
      self.updateServiceType(serviceTypeData)
    }
  })

  $('#create-service-type').on('click', function () {
    $('#service_type_id').val(0)
    $('#service_type_description').val('')
    $('#service_type_group')[0].selectize.setValue('')
    $('#servicetype-modal').modal('show')
  })
}

SettingsServiceTypesClass.prototype.initTable = async function () {
  let self = this
  await self.initServiceTypes()
  let serviceTypesTable = $('#service-types-table').DataTable({
    data: self.serviceTypes,
    fixedHeader: {
      headerOffset: 100,
      header: true,
      footer: false
    },
    bLengthChange: false,
    columns: [
      { title: 'ID', orderable: false, data: 'service_type_id' },
      { title: 'Description', orderable: false, data: 'service_type_description' },
      { title: 'Group', orderable: false, data: 'service_type_group' },
      { title: 'Created At', orderable: false, data: 'created_at' },
      {
        title: 'ACTIONS',
        orderable: false,
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.service_type_id == 1) {
            $(td).children('.delete-job').hide()
            $(td).children('.edit-job').hide()
          }
        },
        defaultContent: "<i class='fa fa-pencil edit-job action-btn' title='modify' style='cursor: pointer' ></i><i class='fa fa-trash delete-job action-btn' title='delete' style='cursor: pointer' ></i>"
      }
    ],
    order: [[0, 'asc']],
    pageLength: 25
  })
  $('#service-types-table').on('click', 'i.edit-job', function () {
    var data = serviceTypesTable.row($(this).closest('tr')).data()
    $('#service_type_id').val(data.service_type_id)
    $('#service_type_description').val(data.service_type_description)
    $('#service_type_group')[0].selectize.setValue(data.service_type_group)
    $('#servicetype-modal').modal('show')
  })
  $('#service-types-table').on('click', 'i.delete-job', function () {
    var data = serviceTypesTable.row($(this).closest('tr')).data()
    Swal.fire({
      title: 'Delete Service Type?',
      text: `Are you sure you want to delete ${data.service_type_description}? You won't be able to revert it!`,
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonColor: '#dc3545',
      confirmButtonText: 'Confirm'
    }).then(result => {
      if (result.isConfirmed) self.deleteServiceType(data.service_type_id)
    })
  })
}

SettingsServiceTypesClass.prototype.initServiceTypes = async function () {
  let self = this
  try {
    const serviceTypes = await axios.get(self.Helpers.API_URL + '/servicetypes', { headers: self.Helpers.getHeaders() })
    for (let serviceType of serviceTypes.data) {
      serviceType.created_at = self.Helpers.changeMysqlDateToNormal(serviceType.service_type_created_at)
      self.serviceTypes.push(serviceType)
    }
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.serviceTypes = []
  }
}

SettingsServiceTypesClass.prototype.createServiceType = async function (serviceType) {
  let self = this
  try {
    await axios.post(self.Helpers.API_URL + '/servicetypes', serviceType, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Service Type Created!')
    $('#servicetype-modal').modal('hide')
    self.refreshTable()
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

SettingsServiceTypesClass.prototype.updateServiceType = async function (serviceType) {
  let self = this
  try {
    await axios.put(self.Helpers.API_URL + '/servicetypes', serviceType, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Service Type Updated!')
    $('#servicetype-modal').modal('hide')
    self.refreshTable()
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

SettingsServiceTypesClass.prototype.deleteServiceType = async function (service_type_id) {
  let self = this

  try {
    await axios.delete(`${self.Helpers.API_URL}/servicetypes/${service_type_id}`, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Service Type Deleted!')
    self.refreshTable()
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

SettingsServiceTypesClass.prototype.refreshTable = function () {
  let self = this
  self.serviceTypes = []
  $('#service-types-table').unbind('click')
  $('#service-types-table').DataTable().clear()
  $('#service-types-table').DataTable().destroy()
  self.initTable()
}

new SettingsServiceTypesClass()
