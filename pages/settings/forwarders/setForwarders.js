let SettingsForwardersClass = function () {
  this.forwarders = []
  this.countries = []
  this.Validations = new ValidationsClass()
  this.Helpers = new HelpersClass()
  this.Helpers.validateLogin()
  this.Helpers.bindMovingEvents('forwarder-modal-mover', 'forwarder-modal', 'forwarder-modal-content')
  this.addEventsOnButtons()
  this.initTable()
}

SettingsForwardersClass.prototype.addEventsOnButtons = function () {
  let self = this

  $('#forwarder_countries').selectize()

  $('#save-forwarder').on('click', function () {
    let forwarderData = {
      forwarder_id: $('#forwarder_id').val(),
      forwarder_name: $('#forwarder_name').val(),
      forwarder_email: $('#forwarder_email').val(),
      forwarder_countries: $('#forwarder_countries').val()
    }

    let validationErrors = self.Validations.isForwarderValid(forwarderData)
    if (validationErrors) {
      self.Helpers.toastr('error', `Validation Errors: ${validationErrors.join(',')}`)
      return
    }
    if (forwarderData.forwarder_id == 0) {
      delete forwarderData.forwarder_id
      self.createForwarder(forwarderData)
    } else {
      self.updateForwarder(forwarderData)
    }
  })

  $('#create-forwarder').on('click', function () {
    $('#forwarder_id').val(0)
    $('#forwarder_name').val('')
    $('#forwarder_email').val('')
    $('#forwarder_countries')[0].selectize.setValue('')
    $('#forwarder-modal').modal('show')
  })
}

SettingsForwardersClass.prototype.initTable = async function () {
  let self = this
  await self.initCountries()
  await self.initForwarders()
  let forwardersTable = $('#forwarders-table').DataTable({
    data: self.forwarders,
    fixedHeader: {
      headerOffset: 100,
      header: true,
      footer: false
    },
    bLengthChange: false,
    columns: [
      { title: 'ID', orderable: false, data: 'forwarder_id' },
      { title: 'Name', orderable: false, data: 'forwarder_name' },
      { title: 'Email', orderable: false, data: 'forwarder_email' },
      { title: 'Countries', orderable: false, data: 'forwarder_countries_names' },
      { title: 'Created At', orderable: false, data: 'created_at' },
      {
        title: 'Actions',
        orderable: false,
        defaultContent: "<i class='fa fa-pencil edit-job action-btn' title='modify' style='cursor: pointer' ></i><i class='fa fa-trash delete-job action-btn' title='delete' style='cursor: pointer' ></i>"
      }
    ],
    order: [[1, 'asc']],
    pageLength: 25
  })
  $('#forwarders-table').on('click', 'i.edit-job', function () {
    var data = forwardersTable.row($(this).closest('tr')).data()
    console.log(data)
    $('#forwarder_id').val(data.forwarder_id)
    $('#forwarder_name').val(data.forwarder_name)
    $('#forwarder_email').val(data.forwarder_email)
    $('#forwarder_countries')[0].selectize.setValue(data.forwarder_countries)
    $('#forwarder-modal').modal('show')
  })
  $('#forwarders-table').on('click', 'i.delete-job', function () {
    var data = forwardersTable.row($(this).closest('tr')).data()
    Swal.fire({
      title: 'Delete Forwarder?',
      text: `Are you sure you want to delete ${data.forwarder_name}? You won't be able to revert it!`,
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonColor: '#dc3545',
      confirmButtonText: 'Confirm'
    }).then(result => {
      if (result.isConfirmed) self.deleteForwarder(data.forwarder_id)
    })
  })
}

SettingsForwardersClass.prototype.initForwarders = async function () {
  let self = this
  try {
    const forwarders = await axios.get(self.Helpers.API_URL + '/forwarders', { headers: self.Helpers.getHeaders() })
    for (let forwarder of forwarders.data) {
      forwarder.created_at = self.Helpers.changeMysqlDateToNormal(forwarder.forwarder_created_at)
      let forwarder_countries_names = []
      for (let country_id of forwarder.forwarder_countries) {
        let country = self.countries.find(el => el.country_id == country_id)
        if (country) forwarder_countries_names.push(country.country_name)
      }
      forwarder.forwarder_countries_names = forwarder_countries_names.join(',')
      self.forwarders.push(forwarder)
    }
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.forwarders = []
  }
}

SettingsForwardersClass.prototype.createForwarder = async function (forwarder) {
  let self = this
  try {
    await axios.post(self.Helpers.API_URL + '/forwarders', forwarder, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Forwarder Created!')
    $('#forwarder-modal').modal('hide')
    self.refreshTable()
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

SettingsForwardersClass.prototype.updateForwarder = async function (forwarder) {
  let self = this
  try {
    await axios.put(self.Helpers.API_URL + '/forwarders', forwarder, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Forwarder Updated!')
    $('#forwarder-modal').modal('hide')
    self.refreshTable()
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

SettingsForwardersClass.prototype.deleteForwarder = async function (forwarder_id) {
  let self = this

  try {
    await axios.delete(`${self.Helpers.API_URL}/forwarders/${forwarder_id}`, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Forwarder Deleted!')
    self.refreshTable()
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

SettingsForwardersClass.prototype.refreshTable = function () {
  let self = this
  self.forwarders = []
  $('#forwarders-table').unbind('click')
  $('#forwarders-table').DataTable().clear()
  $('#forwarders-table').DataTable().destroy()
  self.initTable()
}

SettingsForwardersClass.prototype.initCountries = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/countries', { headers: self.Helpers.getHeaders() })
    self.countries = response.data
    for (i = 0; i < self.countries.length; i++) {
      $('#forwarder_countries')[0].selectize.addOption({ text: self.countries[i].country_name, value: self.countries[i].country_id })
    }
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.countries = []
  }
}

new SettingsForwardersClass()
