let SettingsVesselsClass = function () {
  this.vessels = []
  this.Helpers = new HelpersClass()
  this.Helpers.validateLogin()
  this.Helpers.bindMovingEvents('vessel-modal-mover', 'vessel-modal', 'vessel-modal-content')
  this.addEventsOnButtons()
  this.initTable()
}

SettingsVesselsClass.prototype.addEventsOnButtons = function () {
  let self = this

  $('#save-vessel').on('click', function () {
    let vesselData = {
      vessel_id: parseInt($('#vessel_id').val()),
      vessel_description: $('#vessel_description').val()
    }
    if (vesselData.vessel_description == '') {
      self.Helpers.toastr('error', 'Some fields are empty..')
      return
    }

    if (vesselData.vessel_id == 0) {
      delete vesselData.vessel_id
      self.createVessel(vesselData)
    } else {
      self.updateVessel(vesselData)
    }
  })

  $('#create-vessel').on('click', function () {
    $('#vessel_id').val(0)
    $('#vessel_description').val('')
    $('#vessel-modal').modal('show')
  })
}

SettingsVesselsClass.prototype.initTable = async function () {
  let self = this
  await self.initVessels()
  let vesselsTable = $('#vessels-table').DataTable({
    data: self.vessels,
    fixedHeader: {
      headerOffset: 100,
      header: true,
      footer: false
    },
    bLengthChange: false,
    columns: [
      { title: 'ID', orderable: false, data: 'vessel_id' },
      { title: 'NAME', orderable: false, data: 'vessel_description' },
      { title: 'Created At', orderable: false, data: 'created_at' },
      {
        title: 'ACTIONS',
        orderable: false,
        defaultContent: "<i class='fa fa-pencil edit-job action-btn' title='modify' style='cursor: pointer' ></i><i class='fa fa-trash delete-job action-btn' title='delete' style='cursor: pointer' ></i>"
      }
    ],
    order: [[1, 'asc']],
    pageLength: 25
  })
  $('#vessels-table').on('click', 'i.edit-job', function () {
    var data = vesselsTable.row($(this).closest('tr')).data()
    $('#vessel_id').val(data.vessel_id)
    $('#vessel_description').val(data.vessel_description)
    $('#vessel-modal').modal('show')
  })
  $('#vessels-table').on('click', 'i.delete-job', function () {
    var data = vesselsTable.row($(this).closest('tr')).data()
    Swal.fire({
      title: 'Delete Vessel?',
      text: `Are you sure you want to delete ${data.vessel_description}? You won't be able to revert it!`,
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonColor: '#dc3545',
      confirmButtonText: 'Confirm'
    }).then(result => {
      if (result.isConfirmed) self.deleteVessel(data.vessel_id)
    })
  })
}

SettingsVesselsClass.prototype.initVessels = async function () {
  let self = this
  try {
    const vessels = await axios.get(self.Helpers.API_URL + '/vessels', { headers: self.Helpers.getHeaders() })
    for (let vessel of vessels.data) {
      vessel.created_at = self.Helpers.changeMysqlDateToNormal(vessel.vessel_created_at)
      self.vessels.push(vessel)
    }
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.vessels = []
  }
}

SettingsVesselsClass.prototype.createVessel = async function (vessel) {
  let self = this
  try {
    await axios.post(self.Helpers.API_URL + '/vessels', vessel, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Vessel Created!')
    $('#vessel-modal').modal('hide')
    self.refreshTable()
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

SettingsVesselsClass.prototype.updateVessel = async function (vessel) {
  let self = this
  try {
    await axios.put(self.Helpers.API_URL + '/vessels', vessel, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Vessel Updated!')
    $('#vessel-modal').modal('hide')
    self.refreshTable()
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

SettingsVesselsClass.prototype.deleteVessel = async function (vessel_id) {
  let self = this

  try {
    await axios.delete(`${self.Helpers.API_URL}/vessels/${vessel_id}`, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Vessel Deleted!')
    self.refreshTable()
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

SettingsVesselsClass.prototype.refreshTable = function () {
  let self = this
  self.vessels = []
  $('#vessels-table').unbind('click')
  $('#vessels-table').DataTable().clear()
  $('#vessels-table').DataTable().destroy()
  self.initTable()
}

new SettingsVesselsClass()
