let SettingsColorsClass = function () {
  this.colors = []
  this.Helpers = new HelpersClass()
  this.Helpers.bindMovingEvents('color-modal-mover', 'color-modal', 'color-modal-content')
  this.Helpers.validateLogin()
  this.addEventsOnButtons()
  this.initTable()
}

SettingsColorsClass.prototype.addEventsOnButtons = function () {
  let self = this

  $('#save-color').on('click', function () {
    let colorData = {
      color_id: $('#color_id').val(),
      color_code: $('#color_code').val().toUpperCase()
    }
    if (colorData.color_code == '') {
      self.Helpers.toastr('error', 'Some fields are empty.')
      return
    }

    if (colorData.color_id == 0) {
      delete colorData.color_id
      self.createColor(colorData.color_code)
    } else {
      self.updateColor(colorData)
    }
  })

  $('#create-color').on('click', function () {
    $('#color_id').val(0)
    $('#color_code').val('')
    $('#color-modal').modal('show')
  })
}

SettingsColorsClass.prototype.initTable = async function () {
  let self = this
  await self.initColors()
  let colorsTable = $('#colors-table').DataTable({
    data: self.colors,
    fixedHeader: {
      headerOffset: 100,
      header: true,
      footer: false
    },
    bLengthChange: false,
    columns: [
      { title: 'ID', orderable: false, data: 'color_id' },
      {
        title: 'Color Code',
        createdCell: function (td, cellData, rowData, row, col) {
          $(td).css('background-color', rowData.color_code).html(rowData.color_code)
        },
        orderable: false,
        data: 'color_code'
      },
      { title: 'Created At', orderable: false, data: 'created_at' },
      {
        title: 'Actions',
        orderable: false,
        defaultContent: "<i class='fa fa-pencil edit-job action-btn' title='modify' style='cursor: pointer' ></i><i class='fa fa-trash delete-job action-btn' title='delete' style='cursor: pointer' ></i>"
      }
    ],
    order: [[0, 'asc']],
    pageLength: 25
  })
  $('#colors-table').on('click', 'i.edit-job', function () {
    var data = colorsTable.row($(this).closest('tr')).data()
    $('#color_id').val(data.color_id)
    $('#color_code').val(data.color_code)
    $('#color-modal').modal('show')
  })
  $('#colors-table').on('click', 'i.delete-job', function () {
    var data = colorsTable.row($(this).closest('tr')).data()
    Swal.fire({
      title: 'Delete Color?',
      text: `Are you sure you want to delete ${data.color_code}? You won't be able to revert it!`,
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonColor: '#dc3545',
      confirmButtonText: 'Confirm'
    }).then(result => {
      if (result.isConfirmed) self.deleteColor(data.color_id)
    })
  })
}

SettingsColorsClass.prototype.initColors = async function () {
  let self = this
  try {
    const colors = await axios.get(self.Helpers.API_URL + '/colors', { headers: self.Helpers.getHeaders() })
    for (let color of colors.data) {
      color.created_at = self.Helpers.changeMysqlDateToNormal(color.color_created_at)
      self.colors.push(color)
    }
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.colors = []
  }
}

SettingsColorsClass.prototype.createColor = async function (color_code) {
  let self = this
  try {
    await axios.post(self.Helpers.API_URL + '/colors', { color_code: color_code }, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Color Created!')
    $('#color-modal').modal('hide')
    self.refreshTable()
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

SettingsColorsClass.prototype.updateColor = async function (color) {
  let self = this
  try {
    await axios.put(self.Helpers.API_URL + '/colors', color, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Color Updated!')
    $('#color-modal').modal('hide')
    self.refreshTable()
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

SettingsColorsClass.prototype.deleteColor = async function (color_id) {
  let self = this

  try {
    await axios.delete(`${self.Helpers.API_URL}/colors/${color_id}`, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Color Deleted!')
    self.refreshTable()
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

SettingsColorsClass.prototype.refreshTable = function () {
  let self = this
  self.colors = []
  $('#colors-table').unbind('click')
  $('#colors-table').DataTable().clear()
  $('#colors-table').DataTable().destroy()
  self.initTable()
}

new SettingsColorsClass()
