let SettingsUsersClass = function () {
  this.users = []
  this.departments = []
  this.roles = []
  this.Validations = new ValidationsClass()
  this.Helpers = new HelpersClass()
  this.Helpers.validateLogin()
  this.Helpers.bindMovingEvents('user-modal-mover', 'user-modal', 'user-modal-content')
  this.addEventsOnButtons()
  this.initDepartments()
  this.initRoles()
  this.initTable()
}

SettingsUsersClass.prototype.addEventsOnButtons = function () {
  let self = this

  $('#departments_select').selectize()
  $('#user_role_id').selectize()

  $('#save-user').on('click', function () {
    let userData = {
      user_id: parseInt($('#user_id').val()),
      user_username: $('#user_username').val(),
      user_role_id: parseInt($('#user_role_id').val()),
      user_email: $('#user_email').val(),
      user_fullname: $('#user_fullname').val(),
      user_password: $('#user_password').val(),
      user_password_repeat: $('#user_password_repeat').val(),
      departments: $('#departments_select').val().map(Number)
    }

    let validationErrors = self.Validations.isUserValid(userData)
    if (validationErrors) {
      self.Helpers.toastr('error', `Validation Errors: ${validationErrors.join(',')}`)
      return
    }

    if (userData.user_id == 0) {
      delete userData.user_id
      self.createUser(userData)
    } else {
      self.updateUser(userData)
    }
  })

  $('#create-user').on('click', function () {
    $('#user_id').val(0)
    $('#user_username').val('')
    $('#user_fullname').val('')
    $('#user_password').val('')
    $('#user_email').val('')
    $('#user_password_repeat').val('')
    $('#departments_select')[0].selectize.setValue('')
    $('#user_role_id')[0].selectize.setValue('')
    $('#user-modal').modal('show')
  })
}

SettingsUsersClass.prototype.initTable = async function () {
  let self = this
  await self.initUsers()
  let usersTable = $('#users-table').DataTable({
    data: self.users,
    fixedHeader: {
      headerOffset: 100,
      header: true,
      footer: false
    },
    bLengthChange: false,
    columns: [
      { title: 'ID', orderable: false, data: 'user_id' },
      { title: 'Username', orderable: false, data: 'user_username' },
      { title: 'Fullname', orderable: false, data: 'user_fullname' },
      { title: 'Email', orderable: false, data: 'user_email' },
      { title: 'Departments', orderable: false, data: 'department_descriptions' },
      { title: 'Role', orderable: false, data: 'user_role_description' },
      { title: 'Created At', orderable: false, data: 'user_created_at' },
      {
        title: 'ACTIONS',
        orderable: false,
        defaultContent: "<i class='fa fa-pencil edit-job action-btn' title='modify' style='cursor: pointer' ></i><i class='fa fa-trash delete-job action-btn' title='delete' style='cursor: pointer' ></i>"
      }
    ],
    order: [[1, 'asc']],
    pageLength: 25
  })
  $('#users-table').on('click', 'i.edit-job', function () {
    var data = usersTable.row($(this).closest('tr')).data()

    let departmentIds = []
    for (let department of data.departments) departmentIds.push(department.department_id)
    $('#user_id').val(data.user_id)
    $('#user_username').val(data.user_username)
    $('#user_fullname').val(data.user_fullname)
    $('#user_email').val(data.user_email)
    $('#departments_select')[0].selectize.setValue(departmentIds)
    $('#user_role_id')[0].selectize.setValue(data.user_role_id)
    $('#user_password').val('')
    $('#user_password_repeat').val('')
    $('#user-modal').modal('show')
  })
  $('#users-table').on('click', 'i.delete-job', function () {
    var data = usersTable.row($(this).closest('tr')).data()
    Swal.fire({
      title: 'Delete User?',
      text: `Are you sure you want to delete ${data.user_fullname}? You won't be able to revert it!`,
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonColor: '#dc3545',
      confirmButtonText: 'Confirm'
    }).then(result => {
      if (result.isConfirmed) self.deleteUser(data.user_id)
    })
  })
}

SettingsUsersClass.prototype.initUsers = async function () {
  let self = this
  try {
    const users = await axios.get(self.Helpers.API_URL + '/users', { headers: self.Helpers.getHeaders() })
    for (let user of users.data) {
      let departmentDescriptions = []
      for (let department of user.departments) {
        departmentDescriptions.push(department.department_description)
      }
      user.department_descriptions = departmentDescriptions.join(',')
      user.user_created_at = self.Helpers.changeMysqlDateToNormal(user.user_created_at)
      self.users.push(user)
    }
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.users = []
  }
}

SettingsUsersClass.prototype.createUser = async function (user) {
  let self = this
  try {
    await axios.post(self.Helpers.API_URL + '/users', user, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'User Created!')
    $('#user-modal').modal('hide')
    self.refreshTable()
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

SettingsUsersClass.prototype.updateUser = async function (user) {
  let self = this
  try {
    await axios.put(self.Helpers.API_URL + '/users', user, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'User Updated!')
    $('#user-modal').modal('hide')
    self.refreshTable()
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

SettingsUsersClass.prototype.deleteUser = async function (user_id) {
  let self = this

  try {
    await axios.delete(`${self.Helpers.API_URL}/users/${user_id}`, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'User Deleted!')
    self.refreshTable()
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

SettingsUsersClass.prototype.refreshTable = function () {
  let self = this
  self.users = []
  $('#users-table').unbind('click')
  $('#users-table').DataTable().clear()
  $('#users-table').DataTable().destroy()
  self.initTable()
}

SettingsUsersClass.prototype.initDepartments = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/departments', { headers: self.Helpers.getHeaders() })
    self.departments = response.data
    for (i = 0; i < self.departments.length; i++) {
      $('#departments_select')[0].selectize.addOption({ text: self.departments[i].department_description, value: self.departments[i].department_id })
    }
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.departments = []
  }
}

SettingsUsersClass.prototype.initRoles = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/userRoles', { headers: self.Helpers.getHeaders() })
    self.roles = response.data
    for (i = 0; i < self.roles.length; i++) {
      if (self.roles[i].users_role_id == 5) continue
      $('#user_role_id')[0].selectize.addOption({ text: self.roles[i].user_role_description, value: self.roles[i].user_role_id })
    }
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.roles = []
  }
}

new SettingsUsersClass()
