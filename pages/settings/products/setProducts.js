let SettingsProductsClass = function () {
  this.products = []
  this.departments = []
  this.Validations = new ValidationsClass()
  this.Helpers = new HelpersClass()
  this.Helpers.validateLogin()
  this.Helpers.bindMovingEvents('product-modal-mover', 'product-modal', 'product-modal-content')
  this.addEventsOnButtons()
  this.initDepartments()
  this.initTable()
}

SettingsProductsClass.prototype.addEventsOnButtons = function () {
  let self = this

  $('#product_department_id').selectize()

  $('#save-product').on('click', function () {
    let productData = {
      product_id: parseInt($('#product_id').val()),
      product_description: $('#product_description').val(),
      product_department_id: parseInt($('#product_department_id').val())
    }

    let validationErrors = self.Validations.isProductValid(productData)
    if (validationErrors) {
      self.Helpers.toastr('error', `Validation Errors: ${validationErrors.join(',')}`)
      return
    }

    if (productData.product_id == 0) {
      delete productData.product_id
      self.createProduct(productData)
    } else {
      self.updateProduct(productData)
    }
  })

  $('#create-product').on('click', function () {
    $('#product_id').val(0)
    $('#product_description').val('')
    $('#product_department_id')[0].selectize.setValue('')
    $('#product-modal').modal('show')
  })
}

SettingsProductsClass.prototype.initTable = async function () {
  let self = this
  await self.initProducts()
  let productstable = $('#products-table').DataTable({
    data: self.products,
    fixedHeader: {
      headerOffset: 100,
      header: true,
      footer: false
    },
    bLengthChange: false,
    columns: [
      { title: 'ID', orderable: false, data: 'product_id' },
      { title: 'Product Name', orderable: false, data: 'product_description' },
      { title: 'Department', orderable: false, data: 'department_description' },
      { title: 'Created At', orderable: false, data: 'created_at' },
      {
        title: 'ACTIONS',
        orderable: false,
        defaultContent: "<i class='fa fa-pencil edit-job action-btn' title='modify' style='cursor: pointer' ></i><i class='fa fa-trash delete-job action-btn' title='delete' style='cursor: pointer' ></i>"
      }
    ],
    order: [[1, 'asc']],
    pageLength: 25
  })
  $('#products-table').on('click', 'i.edit-job', function () {
    var data = productstable.row($(this).closest('tr')).data()
    $('#product_id').val(data.product_id)
    $('#product_description').val(data.product_description)
    $('#product_department_id')[0].selectize.setValue(data.product_department_id)
    $('#product-modal').modal('show')
  })
  $('#products-table').on('click', 'i.delete-job', function () {
    var data = productstable.row($(this).closest('tr')).data()
    Swal.fire({
      title: 'Delete Product?',
      text: `Are you sure you want to delete ${data.product_description}? You won't be able to revert it!`,
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonColor: '#dc3545',
      confirmButtonText: 'Confirm'
    }).then(result => {
      if (result.isConfirmed) self.deleteProduct(data.product_id)
    })
  })
}

SettingsProductsClass.prototype.initProducts = async function () {
  let self = this
  try {
    const products = await axios.get(self.Helpers.API_URL + '/products', { headers: self.Helpers.getHeaders() })
    for (let product of products.data) {
      product.created_at = self.Helpers.changeMysqlDateToNormal(product.product_created_at)
      self.products.push(product)
    }
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.products = []
  }
}

SettingsProductsClass.prototype.createProduct = async function (product) {
  let self = this
  try {
    await axios.post(self.Helpers.API_URL + '/products', product, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Product Created!')
    $('#product-modal').modal('hide')
    self.refreshTable()
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

SettingsProductsClass.prototype.updateProduct = async function (product) {
  let self = this
  try {
    await axios.put(self.Helpers.API_URL + '/products', product, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Product Updated!')
    $('#product-modal').modal('hide')
    self.refreshTable()
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

SettingsProductsClass.prototype.deleteProduct = async function (product_id) {
  let self = this

  try {
    await axios.delete(`${self.Helpers.API_URL}/products/${product_id}`, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Product Deleted!')
    self.refreshTable()
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

SettingsProductsClass.prototype.refreshTable = function () {
  let self = this
  self.products = []
  $('#products-table').unbind('click')
  $('#products-table').DataTable().clear()
  $('#products-table').DataTable().destroy()
  self.initTable()
}

SettingsProductsClass.prototype.initDepartments = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/departments', { headers: self.Helpers.getHeaders() })
    self.departments = response.data
    for (i = 0; i < self.departments.length; i++) {
      $('#product_department_id')[0].selectize.addOption({ text: self.departments[i].department_description, value: self.departments[i].department_id })
    }
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.departments = []
  }
}
new SettingsProductsClass()
