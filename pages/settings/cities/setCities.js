let SettingsCitiesClass = function () {
  this.cities = []
  this.countries = []
  this.Validations = new ValidationsClass()
  this.Helpers = new HelpersClass()
  this.Helpers.bindMovingEvents('city-modal-mover', 'city-modal', 'city-modal-content')
  this.Helpers.validateLogin()
  this.addEventsOnButtons()
  this.initCountries()
  this.initTable()
}

SettingsCitiesClass.prototype.addEventsOnButtons = function () {
  let self = this

  $('#city_country_id').selectize()

  $('#save-city').on('click', function () {
    let cityData = {
      city_id: parseInt($('#city_id').val()),
      city_country_id: parseInt($('#city_country_id').val()),
      city_name: $('#city_name').val()
    }

    let validationErrors = self.Validations.isCityValid(cityData)
    if (validationErrors) {
      self.Helpers.toastr('error', `Validation Errors: ${validationErrors.join(',')}`)
      return
    }

    if ($('#city_associate').val() != '') cityData.city_name += ` (${$('#city_associate').val()})`
    if (cityData.city_id == 0) {
      delete cityData.city_id
      self.createCity(cityData)
    } else {
      self.updateCity(cityData)
    }
  })

  $('#create-city').on('click', function () {
    $('#city_id').val(0)
    $('#city_country_id')[0].selectize.setValue('')
    $('#city_name').val('')
    $('#city_associate').val('')
    $('#city-modal').modal('show')
  })
}

SettingsCitiesClass.prototype.initTable = async function () {
  let self = this
  await self.initCities()
  let citiesTable = $('#cities-table').DataTable({
    data: self.cities,
    fixedHeader: {
      headerOffset: 100,
      header: true,
      footer: false
    },
    bLengthChange: false,
    columns: [
      { title: 'ID', orderable: false, data: 'city_id' },
      { title: 'Name', orderable: false, data: 'city_name' },
      { title: 'Country', orderable: false, data: 'country_name' },
      { title: 'Created At', orderable: false, data: 'created_at' },
      {
        title: 'Actions',
        orderable: false,
        defaultContent: "<i class='fa fa-pencil edit-job action-btn' title='modify' style='cursor: pointer' ></i><i class='fa fa-trash delete-job action-btn' title='delete' style='cursor: pointer' ></i>"
      }
    ],
    order: [[1, 'asc']],
    pageLength: 25
  })
  $('#cities-table').on('click', 'i.edit-job', function () {
    var data = citiesTable.row($(this).closest('tr')).data()
    $('#city_id').val(data.city_id)
    $('#city_name').val(data.city_name)
    $('#city_country_id')[0].selectize.setValue(data.city_country_id)
    $('#city-modal').modal('show')
  })
  $('#cities-table').on('click', 'i.delete-job', function () {
    var data = citiesTable.row($(this).closest('tr')).data()
    Swal.fire({
      title: 'Delete City?',
      text: `Are you sure you want to delete ${data.city_name}? You won't be able to revert it!`,
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonColor: '#dc3545',
      confirmButtonText: 'Confirm'
    }).then(result => {
      if (result.isConfirmed) self.deleteCity(data.city_id)
    })
  })
}

SettingsCitiesClass.prototype.initCities = async function () {
  let self = this
  try {
    const cities = await axios.get(self.Helpers.API_URL + '/cities', { headers: self.Helpers.getHeaders() })
    for (let city of cities.data) {
      city.created_at = self.Helpers.changeMysqlDateToNormal(city.created_at)
      self.cities.push(city)
    }
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.cities = []
  }
}

SettingsCitiesClass.prototype.createCity = async function (city) {
  let self = this
  try {
    const cities = await axios.post(self.Helpers.API_URL + '/cities', city, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'City Created!')
    $('#city-modal').modal('hide')
    self.refreshTable()
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

SettingsCitiesClass.prototype.updateCity = async function (city) {
  let self = this
  try {
    await axios.put(self.Helpers.API_URL + '/cities', city, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'City Updated!')
    $('#city-modal').modal('hide')
    self.refreshTable()
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

SettingsCitiesClass.prototype.deleteCity = async function (city_id) {
  let self = this

  try {
    await axios.delete(`${self.Helpers.API_URL}/cities/${city_id}`, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'City Deleted!')
    self.refreshTable()
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

SettingsCitiesClass.prototype.refreshTable = function () {
  let self = this
  self.cities = []
  $('#cities-table').unbind('click')
  $('#cities-table').DataTable().clear()
  $('#cities-table').DataTable().destroy()
  self.initTable()
}

SettingsCitiesClass.prototype.initCountries = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/countries', { headers: self.Helpers.getHeaders() })
    self.countries = response.data
    for (i = 0; i < self.countries.length; i++) {
      $('#city_country_id')[0].selectize.addOption({ text: self.countries[i].country_name, value: self.countries[i].country_id })
    }
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.countries = []
  }
}

new SettingsCitiesClass()
