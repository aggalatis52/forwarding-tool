let ForgotPasswordClass = function () {
  this.Helpers = new HelpersClass()
  this.createBindings()
  $('#username_or_email').focus()
}

ForgotPasswordClass.prototype.createBindings = function () {
  let self = this

  $('#username_or_email').on('keyup', function (event) {
    if (event.keyCode === 13) {
      event.preventDefault()
      self.submitLoginForm()
    }
  })

  $('#restore-password-btn').on('click', () => {
    self.submitResetPassword()
  })
}

ForgotPasswordClass.prototype.submitResetPassword = function () {
  let self = this
  $('#restore-password-btn').prop('disabled', 'disabled')
  $('#restore-password-btn').html('Attempting password reset...')

  let userInfo = {
    user_info: $('#username_or_email').val()
  }

  axios
    .post(self.Helpers.API_URL + '/users/resetpassword', userInfo)
    .then(res => {
      $('#restore-password-btn').prop('disabled', null)
      $('#restore-password-btn').html('RESET PASSWORD')
      Swal.fire({
        title: 'Password Reseted!',
        text: 'An email has been set with your new password. You can change your password after your login.',
        icon: 'success',
        showCancelButton: false,
        showConfirmButton: true
      }).then(async result => {
        if (result.isConfirmed) {
          window.location.replace('/login')
        }
      })
    })
    .catch(err => {
      console.error(err)
      self.Helpers.toastr('error', 'Unknown User! Please try again.')
      $('#restore-password-btn').prop('disabled', null)
      $('#restore-password-btn').html('RESET PASSWORD')
    })
}

new ForgotPasswordClass()
