let StatisticsClass = function () {
  this.Helpers = new HelpersClass()
  this.Helpers.validateLogin()
}

StatisticsClass.prototype.addEventsOnButtons = function () {
  let self = this

  $('#position_vessel_id').chosen()

  $('#save-position').on('click', function () {
    let positionData = {
      position_id: parseInt($('#position_id').val()),
      position_info: $('#position_info').val(),
      position_vessel_id: parseInt($('#position_vessel_id').val())
    }

    let validationErrors = self.Validations.isPositionValid(positionData)
    if (validationErrors) {
      self.Helpers.toastr('error', `Validation Errors: ${validationErrors.join(',')}`)
      return
    }

    if (positionData.position_id == 0) {
      delete positionData.position_id
      self.createPosition(positionData)
    } else {
      self.updatePosition(positionData)
    }
  })

  $('#create-position').on('click', function () {
    $('#position_id').val(0)
    $('#position_info').val('')

    $('#position_vessel_id').val(0)
    $('#position_vessel_id').trigger('chosen:updated')
    $('#position-modal').modal('show')
  })
}

StatisticsClass.prototype.initTable = async function () {
  let self = this
  await self.initPositions()
  let positionsTable = $('#positions-table').DataTable({
    data: self.positions,
    fixedHeader: {
      headerOffset: 100,
      header: true,
      footer: false
    },
    bLengthChange: false,
    columns: [
      { title: 'ID', orderable: false, data: 'position_id' },
      { title: 'Vessel', orderable: false, data: 'vessel_description' },
      { title: 'Information', orderable: false, data: 'position_info' },
      { title: 'Created At', orderable: false, data: 'created_at' },
      {
        title: 'ACTIONS',
        orderable: false,
        defaultContent: "<i class='fa fa-pencil edit-job action-btn' title='modify' style='cursor: pointer' ></i><i class='fa fa-trash delete-job action-btn' title='delete' style='cursor: pointer' ></i>"
      }
    ],
    order: [[1, 'asc']],
    pageLength: 25
  })
  $('#positions-table').on('click', 'i.edit-job', function () {
    var data = positionsTable.row($(this).closest('tr')).data()
    $('#position_id').val(data.position_id)
    $('#position_info').val(data.position_info)
    $('#position_vessel_id').val(data.position_vessel_id)
    $('#position_vessel_id').trigger('chosen:updated')
    $('#position-modal').modal('show')
  })
  $('#positions-table').on('click', 'i.delete-job', function () {
    var data = positionsTable.row($(this).closest('tr')).data()
    Swal.fire({
      title: 'Delete Position?',
      text: `Are you sure you want to delete position ${data.position_id}? You won't be able to revert it!`,
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonColor: '#dc3545',
      confirmButtonText: 'Confirm'
    }).then(result => {
      if (result.isConfirmed) self.deletePosition(data.position_id)
    })
  })
}

StatisticsClass.prototype.initPositions = async function () {
  let self = this
  try {
    const positions = await axios.get(self.Helpers.API_URL + '/positions', { headers: self.Helpers.getHeaders() })
    for (let position of positions.data) {
      position.created_at = self.Helpers.changeMysqlDateToNormal(position.created_at)
      self.positions.push(position)
    }
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.positions = []
  }
}

StatisticsClass.prototype.createPosition = async function (position) {
  let self = this
  try {
    await axios.post(self.Helpers.API_URL + '/positions', position, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Position Created!')
    $('#position-modal').modal('hide')
    self.refreshTable()
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

StatisticsClass.prototype.updatePosition = async function (position) {
  let self = this
  try {
    await axios.put(self.Helpers.API_URL + '/positions', position, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Position Updated!')
    $('#position-modal').modal('hide')
    self.refreshTable()
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

StatisticsClass.prototype.deletePosition = async function (position_id) {
  let self = this

  try {
    await axios.delete(`${self.Helpers.API_URL}/positions/${position_id}`, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Position Deleted!')
    self.refreshTable()
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

StatisticsClass.prototype.refreshTable = function () {
  let self = this
  self.positions = []
  $('#positions-table').unbind('click')
  $('#positions-table').DataTable().clear()
  $('#positions-table').DataTable().destroy()
  self.initTable()
}

StatisticsClass.prototype.initVessels = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/vessels', { headers: self.Helpers.getHeaders() })
    self.vessels = response.data
    $('#position_vessel_id').empty()
    $('#position_vessel_id').append('<option></option>')
    for (i = 0; i < self.vessels.length; i++) {
      $('#position_vessel_id').append(new Option(self.vessels[i].vessel_description, self.vessels[i].vessel_id))
    }
    $('#position_vessel_id').trigger('chosen:updated')
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.vessels = []
  }
}
new StatisticsClass()
