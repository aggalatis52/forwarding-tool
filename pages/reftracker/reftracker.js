let RefTrackerClass = function (Helpers) {
  this.Helpers = Helpers
}

RefTrackerClass.prototype.bindEventsOnBtn = function () {
  let self = this
  $('#search-reference-number').on('click', function () {
    const referenceText = $('#reference-number').val()
    self.constructJourneyCards(referenceText)
  })

  $('#reference-number').on('keyup', function () {
    const currentText = this.value
    if (currentText.length > 2) {
      $('#search-reference-number').attr('disabled', null)
    } else {
      $('#search-reference-number').attr('disabled', 'disabled')
    }
  })
}

RefTrackerClass.prototype.openModal = function () {
  $('#reference-number').val('')
  $('#journey-path').html('')
  $('#search-reference-number').attr('disabled', 'disabled')
  $('#ref-tracker-modal').modal('show')
}

RefTrackerClass.prototype.constructJourneyCards = async function (referenceText) {
  let self = this
  $('#journey-path').html('')
  const response = await self.getReferenceInfo(referenceText)
  if (response == null) {
    self.Helpers.toastr('error', 'Unable to locate any job for this reference pattern')
    return
  }
  if (response.created == null) return
  const jobId = response.created.job_display_id == null ? response.created.job_id : response.created.job_display_id
  const createdTab = `
        <li>
          <div class="task secondary align-items-center">
            <div class="task-desc">
              <p class="task-title text-truncate">Job Created</p>
              <p class="task-sub-title text-truncate">ID: ${jobId}, Reference: ${response.created.piece_reference}</p>
              <p class="task-sub-title text-truncate">Department: ${response.created.department_description}, User: ${response.created.user_username}</p>
            </div>
            <div class="members single">
              <span class="end-time text-truncate">${self.Helpers.changeMysqlDateToNormal(response.created.job_created_at)}</span>
            </div>
          </div>
        </li>`
  await self.Helpers.makeTimeout(500)
  $('#journey-path').append(createdTab)

  if (response.created.job_confirmed_at != null) {
    const createdTab = `
        <li>
          <div class="task primary align-items-center">
            <div class="task-desc">
              <p class="task-title text-truncate">Individual Confirmed</p>
              <p class="task-sub-title text-truncate">EX: ${response.created.ex_city_name}, ${response.created.ex_country_name}</p>
              <p class="task-sub-title text-truncate">TO: ${response.created.to_city_name}, ${response.created.to_country_name}</p>
              <p class="task-sub-title text-truncate">Type: ${response.created.job_type}, Forwarder: ${response.created.forwarder.forwarder_name}</p>
            </div>
            <div class="members single">
              <span class="end-time text-truncate">${self.Helpers.changeMysqlDateToNormal(response.created.job_confirmed_at)}</span>
            </div>
          </div>
        </li>`
    await self.Helpers.makeTimeout(500)
    $('#journey-path').append(createdTab)
  }

  if (response.consolidations != null) {
    for (let i = 0; i < response.consolidations.length; i++) {
      let pendConsolidationTab = `
        <li>
          <div class="task urgent align-items-center">
            <div class="task-desc">
              <p class="task-title text-truncate">Assigned to Consolidation</p>
              <p class="task-sub-title text-truncate">Vessel: ${response.consolidations[i].vessel_description}</p>
              <p class="task-sub-title text-truncate">EX: ${response.consolidations[i].ex_city_name}, ${response.consolidations[i].ex_country_name}</p>`
      if (response.consolidations[i].to_city_name != null) pendConsolidationTab += `<p class="task-sub-title text-truncate">TO: ${response.consolidations[i].to_city_name}, ${response.consolidations[i].to_country_name}</p>`
      if (response.consolidations[i].service_type_description != null) pendConsolidationTab += `<p class="task-sub-title text-truncate">Service: ${response.consolidations[i].service_type_description}</p>`
      pendConsolidationTab += `</div>
            <div class="members single">
            <span class="end-time text-truncate">${self.Helpers.changeMysqlDateToNormal(response.consolidations[i].assign_date)}</span>
            </div>
          </div>
        </li>`
      await self.Helpers.makeTimeout(500)
      $('#journey-path').append(pendConsolidationTab)

      if (response.consolidations[i].con_group_confirmed == 1) {
        let doneConsolidationTab = `
        <li>
          <div class="task over-due align-items-center">
            <div class="task-desc">
              <p class="task-title text-truncate">${i == 0 ? 'Consolidated' : 'Re Consolidation ' + i}</p>
              <p class="task-sub-title text-truncate">Vessel: ${response.consolidations[i].vessel_description}</p>
              <p class="task-sub-title text-truncate">EX: ${response.consolidations[i].ex_city_name}, ${response.consolidations[i].ex_country_name}</p>`
        if (response.consolidations[i].to_city_name != null) doneConsolidationTab += `<p class="task-sub-title text-truncate">TO: ${response.consolidations[i].to_city_name}, ${response.consolidations[i].to_country_name}</p>`

        if (response.consolidations[i].service_type_description != null) doneConsolidationTab += `<p class="task-sub-title text-truncate">Service: ${response.consolidations[i].service_type_description}</p>`
        doneConsolidationTab += `</div>
            <div class="members single">
              <span class="end-time text-truncate">${self.Helpers.changeMysqlDateToNormal(response.consolidations[i].con_group_confirmation_date)}</span>
            </div>
          </div>
        </li>`
        await self.Helpers.makeTimeout(500)
        $('#journey-path').append(doneConsolidationTab)
      }
    }
  }

  if (response.delivered != null) {
    const delivertaB = `
        <li>
          <div class="task success align-items-center">
            <div class="task-desc">
              <p class="task-title text-truncate">Delivered</p>
              <p class="task-sub-title text-truncate">Vessel: ${response.delivered.vessel_description}</p>
            </div>
            <div class="members single">
              <span class="end-time text-truncate">${self.Helpers.changeMysqlDateToNormal(response.delivered.delivery_date)}</span>
            </div>
          </div>
        </li>`
    await self.Helpers.makeTimeout(500)
    $('#journey-path').append(delivertaB)
  }
  self.Helpers.toastr('success', 'Journey Completed!')
}

RefTrackerClass.prototype.getReferenceInfo = async function (reference) {
  let self = this
  try {
    const payload = {
      reference: reference
    }
    const response = await axios.post(`${self.Helpers.API_URL}/jobs/search/reference`, payload, { headers: self.Helpers.getHeaders() })
    return response.data
  } catch (err) {
    console.error(err)
    if (err.response.status == 400) return null
    self.Helpers.handleRequestError(err)
  }
}
