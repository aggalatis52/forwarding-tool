let IndividualsClass = function () {
  this.currentJob = null
  this.currentPrice = null
  this.currentGroupPrice = null
  this.currentJobPieces = []
  this.currentJobPrices = []
  this.currentJobGroupPrices = []
  this.jobPieces = []
  this.piecesTable = null
  this.pricesTable = null
  this.groupPricesTable = null
  this.cities = []
  this.modes = []
  this.vessels = []
  this.products = []
  this.serviceTypes = []
  this.forwarders = []
  this.departments = []
  this.countries = []
  this.currencies = []
  this.userDepartments = []
  this.departmentProducts = []
  this.Wizzard = new WizzardClass(['transportation', 'pieces', 'forwarders', 'group', 'group-forwarders', 'costs'])
  this.Validations = new ValidationsClass()
  this.Helpers = new HelpersClass()
  this.Helpers.validateLogin()
  this.Helpers.bindCloseBtnsAlerts()
  this.Helpers.bindMovingEvents('job-modal-mover', 'add-job-modal', 'add-job-modal-content')
  this.bindEventsOnButtons()
  this.initCities()

  this.initPiecesTable()
  this.initPricesTable()
  this.initGroupPricesTable()
  this.initModes()
  this.initUserDepartments()
  this.initVessels()
  this.initServiceTypes()
  this.initForwarders()
  this.initProducts()
  this.initCountries()
  this.initCurrencies()
  this.initJobsTable()
}

IndividualsClass.prototype.bindEventsOnButtons = function () {
  let self = this

  $('#forwarder-select').selectize()
  $('#group-forwarder-select').selectize()
  $('#products-select').selectize()
  $('#currency-select').selectize()
  $('#group-currency-select').selectize()
  $('#group-to-select').selectize()
  $('#group-ex-select').selectize()
  $('#ex-select').selectize()
  $('#to-select').selectize()

  $('#add-job-btn').attr('disabled', true)

  $('#add-job-btn').on('click', function () {
    self.renderAddJobModal(null, 0)
  })

  $('#remove-date').on('click', function () {
    $('#deadline_date').val(self.Helpers.TBA)
  })

  $('#request-price-offer-btn').on('click', function () {
    const countryForwardes = _.filter(self.forwarders, function (o) {
      return o.forwarder_countries.indexOf(self.currentJob.ex.city_country_id.toString()) > -1
    })

    // const selectedCountry = self.countries.find(el => el.country_id == self.currentJob.ex.city_country_id)
    // let forwardersHtml = `<div class="row"><div class="col-12" style="margin-botom: 5%"><p>Choose forwarders to request price offers via email. Only forwarders selected for ${selectedCountry.country_name} are visible.</p></div>`
    // let showSendEmailButton = false
    let validForwarders = []
    for (let forwarder of self.forwarders) {
      let jobForwarderPrice = self.currentJobPrices.findIndex(el => el.forwarder.forwarder_id == forwarder.forwarder_id)
      if (jobForwarderPrice > -1) continue
      if (countryForwardes) {
        let isCountryForwarder = countryForwardes.findIndex(el => el.forwarder_id == forwarder.forwarder_id)
        if (isCountryForwarder == -1) continue
      }
      validForwarders.push(forwarder)
    }

    window.MailPreview.renderJobPreviewModal(self.currentJob.job_id, validForwarders)
  })

  $('#close-add-job-modal').on('click', function () {
    self.refreshTable('jobs')
  })

  $('#request-group-offer-btn').on('click', function () {
    const countryForwardes = _.filter(self.forwarders, function (o) {
      return o.forwarder_countries.indexOf(self.currentJob.group.group_ex.city_country_id.toString()) > -1
    })

    let validForwarders = []
    for (let forwarder of self.forwarders) {
      let jobForwarderPrice = self.currentJobGroupPrices.findIndex(el => el.forwarder.forwarder_id == forwarder.forwarder_id)
      if (jobForwarderPrice > -1) continue
      if (countryForwardes) {
        let isCountryForwarder = countryForwardes.findIndex(el => el.forwarder_id == forwarder.forwarder_id)
        if (isCountryForwarder == -1) continue
      }
      validForwarders.push(forwarder)
    }

    window.MailPreview.renderGroupPreviewModal(self.currentJob.group.group_id, validForwarders)
    // forwardersHtml += `</div>`
    // Swal.fire({
    //   title: 'Offer Request',
    //   html: forwardersHtml,
    //   icon: 'info',
    //   showCancelButton: true,
    //   showConfirmButton: showSendEmailButton,
    //   cancelButtonText: 'Cancel',
    //   showLoaderOnConfirm: true,
    //   confirmButtonColor: '#68bb69',
    //   confirmButtonText: 'Send Emails',
    //   preConfirm: async () => {
    //     let selectedForwarderIds = []
    //     $('.forwarder-offer-checkbox').each(function () {
    //       if ($(this).is(':checked')) selectedForwarderIds.push($(this).data('id'))
    //     })
    //     for (let forwarderId of selectedForwarderIds) {
    //       let dummyPrice = {
    //         price_job_group_id: self.currentJob.group.group_id,
    //         price_job_id: null,
    //         price_forwarder_id: forwarderId,
    //         price_value: null,
    //         price_currency_name: null,
    //         price_currency_rate: null,
    //         price_selected: false,
    //         price_offer_requested: false
    //       }
    //       const priceID = await self.addPrice(dummyPrice)
    //       await self.requestOfferForPrice(priceID, 'group')
    //     }
    //   }
    // }).then(async result => {
    //   if (result.isConfirmed) self.refreshTable('group-prices')
    // })
  })

  $('#add-piece-btn').on('click', function () {
    self.initOrderInputs()
    $(this).hide()
  })

  $('#close-additional-orders-btn').on('click', function () {
    $('.add-piece-form-div').each(function () {
      $(this).hide()
    })

    $('#add-piece-btn').show()
  })

  $('#add-price-btn').on('click', function () {
    self.currentPrice = null
    self.initPriceInputs()
    $(this).hide()
    $('#request-price-offer-btn').hide()
    $('#save-job-notes-btn').hide()
    $('#job-notes-div').hide()
  })

  $('#save-job-notes-btn').on('click', async function () {
    let payload = {
      job_notes: $('#job-notes').val() == '' ? null : $('#job-notes').val()
    }
    await self.patchJob(self.currentJob.job_id, payload)
  })

  $('#save-group-notes-btn').on('click', async function () {
    let payload = {
      job_group_notes: $('#group-notes').val() == '' ? null : $('#group-notes').val()
    }
    await self.patchJobGroup(self.currentJob.group.group_id, payload)
  })

  $('#add-group-price-btn').on('click', function () {
    self.currentGroupPrice = null
    self.initGroupPriceInputs(false, false)
    $(this).hide()
    $('#request-group-offer-btn').hide()
    $('#group-notes-div').hide()
    $('#save-group-notes-btn').hide()
  })

  $('#save-piece-btn').on('click', async function () {
    let piece = {
      piece_job_id: self.currentJob.job_id,
      piece_vessel_id: parseInt($('#vessel-select').val()),
      piece_products: $('#products-select').val().map(Number),
      piece_description: $('#piece-description').val(),
      piece_dimensions: $('#piece-dimensions').val(),
      piece_value_of_goods: $('#piece-value-of-goods').val() == '' ? null : parseFloat($('#piece-value-of-goods').val()),
      piece_weight: parseFloat($('#piece-weight').val()),
      piece_reference: $('#piece-reference').val()
    }

    let validationErrors = self.Validations.isPieceValid(piece)
    if (validationErrors) {
      self.Helpers.toastr('error', `Validation Errors: ${validationErrors.join(',')}`)
      return
    }
    const pieceId = await self.addPiece(piece)
    if (pieceId == null) return
    await self.initJobs()
    self.renderAddJobModal(self.currentJob.job_id, 1, false)
  })

  $('#save-price-btn').on('click', async function () {
    const selectedCurrency = $('#currency-select').val()
    const fullCurrency = self.currencies.find(el => el.currency_name == selectedCurrency)
    const selectedForwarder = parseInt($('#forwarder-select').val())
    let fullForwarder = self.forwarders.find(el => el.forwarder_id == selectedForwarder)
    let price = null
    if (self.currentPrice == null) {
      price = {
        price_job_group_id: null,
        price_job_id: self.currentJob.job_id,
        price_forwarder_id: selectedForwarder,
        price_value: $('#price-value').val() == '' ? null : parseFloat($('#price-value').val()),
        price_currency_name: selectedCurrency,
        price_currency_rate: fullCurrency.currency_rate,
        price_selected: false,
        price_offer_requested: false
      }
    } else {
      price = {
        price_id: self.currentPrice.price_id,
        price_job_group_id: null,
        price_job_id: self.currentJob.job_id,
        price_forwarder_id: selectedForwarder,
        price_value: $('#price-value').val() == '' ? null : parseFloat($('#price-value').val()),
        price_currency_name: selectedCurrency,
        price_currency_rate: fullCurrency.currency_rate,
        price_selected: self.currentPrice.price_selected,
        price_offer_requested: self.currentPrice.price_offer_requested
      }
    }
    price.price_base_value = null
    if (price.price_value != null) price.price_base_value = self.Helpers.applyRate(price.price_value, price.price_currency_rate)
    price.forwarder = fullForwarder

    let validationErrors = self.Validations.isPriceValid(price)
    if (validationErrors) {
      self.Helpers.toastr('error', `Validation Errors: ${validationErrors.join(',')}`)
      return
    }

    if (self.currentPrice == null) {
      await self.addPrice(price)
    } else {
      await self.updatePrice(price)
    }
    self.renderAddJobModal(self.currentJob.job_id, 2)
  })

  $('#save-group-price-btn').on('click', async function () {
    const selectedCurrency = $('#group-currency-select').val()
    const fullCurrency = self.currencies.find(el => el.currency_name == selectedCurrency)
    const selectedForwarder = parseInt($('#group-forwarder-select').val())
    let fullForwarder = self.forwarders.find(el => el.forwarder_id == selectedForwarder)
    let price = null
    if (self.currentGroupPrice == null) {
      price = {
        price_job_group_id: self.currentJob.group.group_id,
        price_job_id: null,
        price_forwarder_id: selectedForwarder,
        price_value: $('#group-price-value').val() == '' ? null : parseFloat($('#group-price-value').val()),
        price_currency_name: selectedCurrency,
        price_currency_rate: fullCurrency.currency_rate,
        price_selected: false,
        price_offer_requested: false
      }
    } else {
      price = {
        price_id: self.currentGroupPrice.price_id,
        price_job_group_id: self.currentGroupPrice.price_job_group_id,
        price_job_id: null,
        price_forwarder_id: selectedForwarder,
        price_value: $('#group-price-value').val() == '' ? null : parseFloat($('#group-price-value').val()),
        price_currency_name: selectedCurrency,
        price_currency_rate: fullCurrency.currency_rate,
        price_selected: self.currentGroupPrice.price_selected,
        price_offer_requested: self.currentGroupPrice.price_offer_requested
      }
    }

    price.price_base_value = null
    if (price.price_value != null) price.price_base_value = self.Helpers.applyRate(price.price_value, price.price_currency_rate)
    price.forwarder = fullForwarder

    let validationErrors = self.Validations.isPriceValid(price)
    if (validationErrors) {
      self.Helpers.toastr('error', `Validation Errors: ${validationErrors.join(',')}`)
      return
    }

    if (self.currentGroupPrice == null) {
      const priceId = await self.addPrice(price)
      if (priceId == null) return
    } else {
      await self.updatePrice(price)
    }
    self.renderGroupPricesTab(true)
  })

  $('#show-price-table-btn').on('click', function () {
    $('.add-price-form-div').each(function () {
      $(this).hide()
    })
    $('#request-price-offer-btn').show()
    $('#add-price-btn').show()
    $('#save-job-notes-btn').show()
    $('#job-notes-div').show()
  })

  $('#show-group-price-table-btn').on('click', function () {
    $('.add-group-price-form-div').each(function () {
      $(this).hide()
    })
    $('#request-group-offer-btn').show()
    $('#add-group-price-btn').show()
    $('#group-notes-div').show()
    $('#save-group-notes-btn').show()
  })

  $('#create-job-btn').on('click', async function () {
    $(this).prop('disabled', 'disabled')
    $(this).html('saving job..')
    let job = {
      job_user_id: self.Helpers.user.user_id,
      job_department_id: parseInt($('#department-select').val()),
      job_ex_city_id: parseInt($('#ex-select').val()),
      job_to_city_id: parseInt($('#to-select').val()),
      job_mode_id: parseInt($('#mode-select').val()),
      job_deadline: $('#deadline_date').val(),
      job_service_type_id: $('#service-type-select').val() == '' ? null : parseInt($('#service-type-select').val()),
      job_status: 1
    }
    let validationErrors = self.Validations.isJobValid(job)
    if (validationErrors) {
      self.Helpers.toastr('error', `Validation Errors: ${validationErrors.join(',')}`)
      $(this).attr('disabled', null)
      return
    }

    let response = null
    if (self.currentJob == null) {
      response = await self.addJob(job)
      if (response != null) {
        self.refreshTable('jobs')
        self.renderAddJobModal(response.job_id, 1)
      }
    } else {
      job.job_id = self.currentJob.job_id
      response = await self.updateJob(job)
      if (response != null) {
        self.renderAddJobModal(response.job_id, -1)
      }
    }

    $(this).prop('disabled', null)
    $(this).html('SAVE JOB')
  })

  $('#save-group-btn').on('click', async function () {
    $(this).attr('disabled', true)

    let manipulatedPrices = []
    for (let price of self.currentJobGroupPrices) manipulatedPrices.push(_.omit(price, 'forwarder'))
    let group = {
      job_group_id: self.currentJob.group.group_id,
      job_group_color: self.currentJob.group.group_color,
      job_group_ex_city_id: parseInt($('#group-ex-select').val()),
      job_group_to_city_id: parseInt($('#group-to-select').val()),
      job_group_deadline: $('#group-deadline').val()
    }

    let validationErrors = self.Validations.isJobGroupValid(group)
    if (validationErrors) {
      self.Helpers.toastr('error', `Validation Errors: ${validationErrors.join(',')}`)
      $(this).attr('disabled', null)
      return
    }

    await self.updateJobGroup(group)
    $(this).attr('disabled', null)
  })
}

IndividualsClass.prototype.initJobsTable = async function () {
  let self = this
  await self.initJobs()
  self.jobsTable = $('#jobs-table').DataTable({
    data: self.jobs,
    bLengthChange: false,
    paging: false,
    ordering: false,
    info: false,
    fixedHeader: {
      headerOffset: 100,
      header: true,
      footer: false
    },
    columns: [
      { title: 'JOB ID', orderable: false, data: 'job_id' },
      {
        title: 'GROUP ID',
        orderable: false,
        data: null,
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.group == null) {
            $(td).html('')
          } else {
            $(td).html(rowData.group.group_id)
          }
        }
      },
      { title: 'REQ. DATE', orderable: false, data: 'request_date' },
      { title: 'USER', orderable: false, data: 'user.user_username' },
      { title: 'DEPT.', orderable: false, data: 'department.department_description' },
      {
        title: 'PIECES',
        orderable: false,
        data: 'pieces_count',
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.pieces_count == 0) {
            $(td).html('<div class="empty-table-value"></div>')
          } else {
            let text = 'Pieces'
            if (rowData.pieces_count == 1) text = 'Piece'
            $(td).html(`<p class='piece-info' style='cursor: pointer'>${rowData.pieces_count} ${text}</p>`)
          }
        }
      },
      {
        title: 'WEIGHT (KG)',
        orderable: false,
        data: 'job_weight'
      },
      {
        title: 'MODE',
        orderable: false,
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.mode.mode_id == 5) $(td).css('color', self.Helpers.PERSONNEL_COLOR).css('font-weight', 'bold')
        },
        data: 'mode.mode_description'
      },
      {
        title: 'SERVICE',
        orderable: false,
        data: 'service_description',
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.service_description == '') {
            $(td).html('<div class="empty-table-value" ></div>')
          } else {
            if (rowData.service_description == self.Helpers.LOCAL_ORDER_TEXT) $(td).css('color', self.Helpers.LOCAL_ORDER_COLOR).css('font-weight', 'bold')
          }
        }
      },
      {
        title: 'VESSELS',
        orderable: false,
        data: 'vessels',
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.vessels_count == 0) {
            $(td).html('<div class="empty-table-value" ></div>')
          }
        }
      },
      { title: 'EX', orderable: false, className: 'danger-header', data: 'ex.city_name' },
      { title: 'TO', orderable: false, className: 'danger-header', data: 'to.city_name' },
      { title: 'DEADLINE', orderable: false, className: 'danger-header', data: 'job_deadline' },
      {
        title: 'OFFERS',
        orderable: false,
        data: 'prices_count',
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.prices_count == 0) {
            if (rowData.group == null) {
              $(td).html('<div class="empty-table-value" ></div>')
            } else {
              $(td).html('')
            }
          } else {
            let text = 'Offers'
            if (rowData.prices_count == 1) text = 'Offer'
            $(td).html(`<p>${rowData.prices_count} ${text}</p>`)
          }
        }
      },
      {
        title: 'FORWARDER',
        orderable: false,
        data: 'job_forwarder_id',
        createdCell: function (td, cellData, rowData, row, col) {
          let forwarderId = rowData.job_forwarder_id
          if (rowData.group != null) {
            forwarderId = rowData.group.group_forwarder_id
          }
          let foundFowarder = self.forwarders.find(el => el.forwarder_id == forwarderId)
          if (!foundFowarder) return
          $(td).html(foundFowarder.forwarder_name)
        }
      },
      {
        title: 'PRICE ($)',
        orderable: false,
        data: 'job_offer_cost',
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.group == null) {
            if (rowData.costs == null) {
              $(td).html('<div class="empty-table-value" ></div>')
            }
          }
        }
      },
      {
        title: 'SHARED COST ($)',
        orderable: false,
        data: 'costs',
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.group != null) {
            if (rowData.costs == null) {
              $(td).html('<div class="empty-table-value"></div>')
            } else {
              $(td).html(rowData.costs.job_shared_cost)
            }
          } else {
            $(td).html('')
          }
        }
      },
      {
        title: 'ACTIONS',
        orderable: false,
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.service_type == null) $(td).children('.confirm-job').hide()

          if (rowData.costs == null) {
            $(td).children('.confirm-job').hide()
          }
          if (rowData.job_deadline == self.Helpers.TBA) $(td).children('.confirm-job').hide()
          if (rowData.group != null) {
            if (rowData.costs == null || rowData.group.costs == null || rowData.group.group_deadline == self.Helpers.TBA) {
              $(td).children('.confirm-job').hide()
            } else {
              $(td).children('.delete-job').hide()
            }
          }
        },
        defaultContent: `<i class='fa fa-pencil job-edit action-btn' title='modify' id='delete-me' style='cursor: pointer'></i>
        <i class='fa fa-check confirm-job action-btn' title='confirm' style='cursor: pointer' ></i>
        </i><i class='fa fa-trash delete-job action-btn' title='delete' style='cursor: pointer' ></i>`
      }
    ],
    rowCallback: function (row, data, index, cells) {
      if (_.has(data, 'group.group_color')) $('td', row).css('background-color', data.group.group_color)
    }
  })

  self.jobsTable.on('click', 'i.job-edit', function () {
    const rowData = self.jobsTable.row($(this).closest('tr')).data()
    if (!self.Helpers.checkIfUserHasPriviledges(rowData.user.user_username)) {
      self.Helpers.swalUserPermissionError()
      return
    }
    self.renderAddJobModal(rowData.job_id, 0)
  })

  self.jobsTable.on('click', 'i.confirm-job', function () {
    const rowData = self.jobsTable.row($(this).closest('tr')).data()
    if (!self.Helpers.checkIfUserHasPriviledges(rowData.user.user_username)) {
      self.Helpers.swalUserPermissionError()
      return
    }

    if (rowData.group == null) {
      Swal.fire({
        title: ' Confirm Job?',
        text: `Are you sure you want to confirm job ${rowData.job_id}?`,
        icon: 'warning',
        showCancelButton: true,
        showLoaderOnConfirm: true,
        cancelButtonText: 'Cancel',
        confirmButtonColor: '#dc3545',
        confirmButtonText: 'Confirm',
        preConfirm: async () => {
          const result = await self.confirmJob(rowData.job_id)
          if (!result) throw 'Error'
        }
      }).then(result => {
        if (result.isConfirmed) {
          Swal.fire('Confirmed!', `Job ${rowData.job_id} is confirmed!`, 'success').then(result => {
            if (result.isConfirmed) window.location.replace('doneindividuals')
          })
        }
      })
    } else {
      Swal.fire({
        title: ' Confirm Job Group?',
        text: `Are you sure you want to confirm group ${rowData.group.group_id}?`,
        icon: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancel',
        showLoaderOnConfirm: true,
        confirmButtonColor: '#dc3545',
        confirmButtonText: 'Confirm',
        preConfirm: async () => {
          const result = await self.confirmJobGroup(rowData.group.group_id)
          if (!result) throw 'Error'
        }
      }).then(result => {
        if (result.isConfirmed) {
          Swal.fire('Confirmed!', `Group ${rowData.group.group_id} is confirmed!`, 'success').then(result => {
            if (result.isConfirmed) window.location.replace('doneindividuals')
          })
        }
      })
    }
  })

  self.jobsTable.on('click', 'i.delete-job', function () {
    const rowData = self.jobsTable.row($(this).closest('tr')).data()
    if (!self.Helpers.checkIfUserHasPriviledges(rowData.user.user_username)) {
      self.Helpers.swalUserPermissionError()
      return
    }

    Swal.fire({
      title: ' Delete Job?',
      text: `Are you sure you want to delete job ${rowData.job_id}?`,
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonColor: '#dc3545',
      confirmButtonText: 'Confirm',
      preConfirm: async () => {
        const result = await self.deleteJob(rowData.job_id)
        if (!result) throw 'Error'
      }
    }).then(result => {
      if (result.isConfirmed) {
        Swal.fire('Deleted!', `Job ${rowData.job_id} is deleted!`, 'success').then(result => {
          if (result.isConfirmed) self.refreshTable('jobs')
        })
      }
    })
  })

  self.jobsTable.on('click', 'p.piece-info', async function () {
    const rowData = self.jobsTable.row($(this).closest('tr')).data()
    self.Helpers.swalPieceInfo(rowData.job_id)
  })

  self.Helpers.applyMouseInteractions('jobs-table')
  self.Helpers.applySearchInteraction(self.jobsTable)
}

IndividualsClass.prototype.initPiecesTable = async function () {
  let self = this
  self.currentJobPieces = []
  if (self.currentJob != null) self.currentJobPieces = await self.getJobPieces(self.currentJob.job_id)
  self.piecesTable = $('#pieces-table').DataTable({
    data: self.currentJobPieces,
    bLengthChange: false,
    paging: false,
    info: false,
    columns: [
      { title: 'ID', orderable: false, data: 'piece_id', visible: false },
      { title: 'Vessel', orderable: false, data: 'vessel.vessel_description', visible: false },
      { title: 'Dimensions', orderable: false, data: 'piece_dimensions' },
      { title: 'Weight (kg)', orderable: false, data: 'piece_weight' },
      { title: 'Reference', orderable: false, data: 'piece_reference' },
      { title: 'Contents', orderable: false, data: 'piece_description' },
      {
        title: 'Products',
        orderable: false,
        data: 'piece_products',
        createdCell: function (td, cellData, rowData, row, col) {
          let productDescriptions = []
          for (let product of rowData.piece_products) productDescriptions.push(product.product_description)
          $(td).html(productDescriptions.join(','))
        }
      },
      { title: 'Value ($)', orderable: false, data: 'piece_value_of_goods' },
      {
        title: 'Actions',
        orderable: false,
        createdCell: function (td, cellData, rowData, row, col) {
          let selectedPrice = self.currentJobPrices.find(el => el.price_selected == 1)
          if (self.currentJob.costs != null || selectedPrice) $(td).children('.delete-piece').hide()
        },
        defaultContent: "<i class='fa fa-qrcode qrcode-piece action-btn' title='qr-code' style='cursor: pointer' ></i><i class='fa fa-trash delete-piece action-btn' title='delete' style='cursor: pointer' ></i>"
      }
    ],
    rowGroup: {
      dataSrc: 'vessel.vessel_description',
      startRender: function (rows, group) {
        let ordersString = 'Order'
        if (rows.count() > 1) ordersString = 'Orders'

        let totalValue = 0
        let totalWeight = 0
        for (let i = 0; i < rows.count(); i++) {
          totalWeight += rows.data()[i].piece_weight
          if (rows.data()[i].piece_value_of_goods != null) totalValue += rows.data()[i].piece_value_of_goods
        }
        return `${group} (${rows.count()} ${ordersString}, ${totalWeight} KG , ${totalValue} $)`
      }
    },
    order: [
      [1, 'asc'],
      [0, 'asc']
    ]
  })

  self.piecesTable.on('click', 'i.delete-piece', function () {
    const rowIndex = $(this).closest('tr').index()
    const rowData = self.piecesTable.row($(this).closest('tr')).data()
    Swal.fire({
      title: 'Delete Piece?',
      text: 'Are you sure you want to delete this piece?',
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonColor: '#dc3545',
      confirmButtonText: 'Confirm'
    }).then(async result => {
      if (result.isConfirmed) {
        const pieceDeleted = await self.deletePiece(rowData.piece_id)
        if (pieceDeleted) {
          self.refreshTable('pieces')
          await self.initJobs()
          self.renderAddJobModal(self.currentJob.job_id, 1, false)
        }
      }
    })
  })

  self.piecesTable.on('click', 'i.qrcode-piece', function () {
    const rowData = self.piecesTable.row($(this).closest('tr')).data()

    var qrcode = new QRCode('qrcode-holder', {
      text: rowData.piece_description,
      width: 128,
      height: 128,
      colorDark: '#000000',
      colorLight: '#ffffff',
      correctLevel: QRCode.CorrectLevel.H
    })

    setTimeout(() => {
      var pdf = new jsPDF({
        orientation: 'landscape',
        unit: 'mm',
        format: [50, 50]
      })

      let base64Image = $('#qrcode-holder img').attr('src')

      pdf.addImage(base64Image, 'png', 5, 5, 40, 40)
      pdf.save('forward-tool-qrcode.pdf')
      qrcode.clear()
      $('#qrcode-holder').html('')
    }, 1000)
  })
}

IndividualsClass.prototype.initPricesTable = async function () {
  let self = this
  self.currentJobPrices = []
  if (self.currentJob != null) self.currentJobPrices = await self.getJobPrices(self.currentJob.job_id)
  self.refreshJobForwardersSelect()
  let showActions = false
  if (self.currentJob != null && self.currentJob.group == null) showActions = true
  self.pricesTable = $('#prices-table').DataTable({
    data: self.currentJobPrices,
    bLengthChange: false,
    paging: false,
    ordering: false,
    info: false,
    columns: [
      { title: 'ID', orderable: false, data: 'price_id' },
      { title: 'Forwarder', orderable: false, data: 'forwarder.forwarder_name' },
      { title: 'Price', orderable: false, data: 'price_value' },
      { title: 'Currency', orderable: false, data: 'price_currency_name' },
      { title: 'Price ($)', orderable: false, data: 'price_base_value' },
      {
        title: 'Offer Requested',
        orderable: false,
        data: 'price_offer_requested',
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.price_offer_requested == true) {
            $(td).css('color', 'green').css('text-align', 'center')
            $(td).html(`<i class="fa fa-check action-btn"></i>`)
          } else {
            $(td).css('color', 'red').css('text-align', 'center')
            $(td).html(`<i class="fa fa-times action-btn"></i>`)
          }
        }
      },
      {
        title: 'Actions',
        orderable: false,
        visible: showActions,
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.price_value == null) $(td).children('.select-price').hide()
          if (rowData.price_selected == 1) {
            $(td).children('.select-price').hide()
            $(td).children('.unselect-price').show()
          } else {
            $(td).children('.select-price').show()
            $(td).children('.unselect-price').hide()
          }
        },
        defaultContent:
          "<i class='fa fa-pencil edit-price action-btn' title='modify'  style='cursor: pointer'></i><i class='fa fa-envelope-o mail-price action-btn' title='request-offer' style='cursor: pointer' ></i><i class='fa fa-check select-price action-btn' title='select' style='cursor: pointer' ></i><i class='fa fa-times unselect-price action-btn' title='unselect' style='cursor: pointer' ></i><i class='fa fa-trash delete-price action-btn' title='delete' style='cursor: pointer' ></i>"
      }
    ],
    rowCallback: function (row, data, index, cells) {
      if (data.price_selected) $('td', row).css('background-color', '#8fbc8f')
    }
  })

  self.pricesTable.on('click', 'i.edit-price', function () {
    const rowData = self.pricesTable.row($(this).closest('tr')).data()
    self.currentPrice = rowData
    self.refreshJobForwardersSelect(false)
    $('#forwarder-select')[0].selectize.setValue(rowData.forwarder.forwarder_id)
    $('#forwarder-select')[0].selectize.disable()
    $('#currency-select')[0].selectize.setValue(rowData.price_currency_name)
    $('#price-value').val(rowData.price_value).attr('disabled', null)
    $('.add-price-form-div').each(function () {
      $(this).show()
    })
    $('#add-price-btn').hide()
    $('#request-price-offer-btn').hide()
    $('#save-job-notes-btn').hide()
    $('#job-notes-div').hide()
  })

  self.pricesTable.on('click', 'i.delete-price', function () {
    const rowData = self.pricesTable.row($(this).closest('tr')).data()
    Swal.fire({
      title: 'Delete Price?',
      text: `Are you sure you want to delete price from ${rowData.forwarder.forwarder_name}?`,
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonColor: '#dc3545',
      confirmButtonText: 'Confirm'
    }).then(async result => {
      if (result.isConfirmed) {
        const priceDeleted = await self.deletePrice(rowData.price_id)
        if (priceDeleted) {
          self.refreshTable('prices')
        }
      }
    })
  })

  self.pricesTable.on('click', 'i.mail-price', function () {
    const rowData = self.pricesTable.row($(this).closest('tr')).data()
    Swal.fire({
      title: 'Request Offer?',
      text: `Are you sure you want to request offer from ${rowData.forwarder.forwarder_name}? An email will be sent to ${rowData.forwarder.forwarder_email}. Keep in mind that records for certain price will be eraised!`,
      icon: 'info',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonColor: '#dc3545',
      confirmButtonText: 'Confirm'
    }).then(async result => {
      if (result.isConfirmed) {
        const priceOfferRequested = await self.requestOfferForPrice(rowData.price_id, 'job')
        if (priceOfferRequested) {
          self.refreshTable('prices')
        }
      }
    })
  })

  self.pricesTable.on('click', 'i.select-price', async function () {
    const rowData = self.pricesTable.row($(this).closest('tr')).data()
    const emptyPrice = self.currentJobPrices.find(el => el.price_value == null)
    if (emptyPrice) {
      Swal.fire({
        title: 'Empty Prices Exist!',
        text: `Some prices aren't filled up for this job. Are you sure you want to select price from ${rowData.forwarder.forwarder_name} regardless?`,
        icon: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancel',
        confirmButtonColor: '#dc3545',
        confirmButtonText: 'Confirm'
      }).then(async result => {
        if (result.isConfirmed) {
          await self.selectPrice(rowData.price_id, 'selectJob')
          self.renderAddJobModal(self.currentJob.job_id, 2)
        }
      })
    } else {
      await self.selectPrice(rowData.price_id, 'selectJob')
      self.renderAddJobModal(self.currentJob.job_id, 2)
    }
  })

  self.pricesTable.on('click', 'i.unselect-price', async function () {
    const rowData = self.pricesTable.row($(this).closest('tr')).data()
    await self.selectPrice(rowData.price_id, 'unselectJob')
    self.renderAddJobModal(self.currentJob.job_id, 2)
  })
}

IndividualsClass.prototype.initGroupPricesTable = async function () {
  let self = this
  self.currentJobGroupPrices = []
  if (self.currentJob != null && self.currentJob.group != null) self.currentJobGroupPrices = await self.Helpers.getJobGroupPrices(self.currentJob.group.group_id)
  self.refreshGroupForwardersSelect()
  self.groupPricesTable = $('#group-prices-table').DataTable({
    data: self.currentJobGroupPrices,
    bLengthChange: false,
    paging: false,
    ordering: false,
    info: false,
    columns: [
      { title: 'ID', orderable: false, data: 'price_id' },
      { title: 'Forwarder', orderable: false, data: 'forwarder.forwarder_name' },
      { title: 'Price', orderable: false, data: 'price_value' },
      { title: 'Currency', orderable: false, data: 'price_currency_name' },
      { title: 'Price ($)', orderable: false, data: 'price_base_value' },
      {
        title: 'Offer Requested',
        orderable: false,
        data: 'price_offer_requested',
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.price_offer_requested == true) {
            $(td).css('color', 'green').css('text-align', 'center')
            $(td).html(`<i class="fa fa-check action-btn"></i>`)
          } else {
            $(td).css('color', 'red').css('text-align', 'center')
            $(td).html(`<i class="fa fa-times action-btn"></i>`)
          }
        }
      },
      {
        title: 'Actions',
        orderable: false,
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.price_value == null) $(td).children('.select-price').hide()
          if (rowData.price_selected == 1) {
            $(td).children('.select-price').hide()
            $(td).children('.unselect-price').show()
          } else {
            $(td).children('.select-price').show()
            $(td).children('.unselect-price').hide()
          }
          if (rowData.price_value == null) $(td).children('.select-price').hide()
        },
        defaultContent:
          "<i class='fa fa-pencil edit-price action-btn' title='modify'  style='cursor: pointer'></i><i class='fa fa-envelope-o mail-price action-btn' title='request-offer' style='cursor: pointer' ></i><i class='fa fa-check select-price action-btn' title='select' style='cursor: pointer' ></i><i class='fa fa-times unselect-price action-btn' title='select' style='cursor: pointer' ></i><i class='fa fa-trash delete-price action-btn' title='delete' style='cursor: pointer' ></i>"
      }
    ],
    rowCallback: function (row, data, index, cells) {
      if (data.price_selected) $('td', row).css('background-color', '#8fbc8f')
    }
  })

  self.groupPricesTable.on('click', 'i.edit-price', function () {
    const rowData = self.groupPricesTable.row($(this).closest('tr')).data()
    self.currentGroupPrice = rowData
    self.refreshGroupForwardersSelect(false)
    $('#group-forwarder-select')[0].selectize.setValue(rowData.forwarder.forwarder_id)
    $('#group-forwarder-select')[0].selectize.disable()
    $('#group-currency-select')[0].selectize.setValue(rowData.price_currency_name == null ? self.Helpers.GLOBAL_CURRENCY : rowData.price_currency_name)
    $('#group-price-value').val(rowData.price_value)

    $('#group-forwarders-table-div').hide()
    $('.add-group-price-form-div').each(function () {
      $(this).show()
    })
    $('#group-notes-div').hide()
    $('#save-group-notes-btn').hide()
    $('#request-group-offer-btn').hide()
    $('#add-group-price-btn').hide()
  })

  self.groupPricesTable.on('click', 'i.delete-price', function () {
    const rowData = self.groupPricesTable.row($(this).closest('tr')).data()
    Swal.fire({
      title: 'Delete Price?',
      text: `Are you sure you want to delete price from ${rowData.forwarder.forwarder_name}?`,
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonColor: '#dc3545',
      confirmButtonText: 'Confirm'
    }).then(async result => {
      if (result.isConfirmed) {
        const priceDeleted = await self.deletePrice(rowData.price_id)
        if (priceDeleted) {
          self.refreshTable('group-prices')
        }
      }
    })
  })

  self.groupPricesTable.on('click', 'i.mail-price', function () {
    const rowData = self.groupPricesTable.row($(this).closest('tr')).data()
    Swal.fire({
      title: 'Request Offer?',
      text: `Are you sure you want to request offer for this group from ${rowData.forwarder.forwarder_name}? An email will be sent to ${rowData.forwarder.forwarder_email}. Keep in mind that records for certain price will be eraised!`,
      icon: 'info',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonColor: '#dc3545',
      confirmButtonText: 'Confirm'
    }).then(async result => {
      if (result.isConfirmed) {
        const priceOfferRequested = await self.requestOfferForPrice(rowData.price_id, 'group')
        if (priceOfferRequested) {
          self.refreshTable('group-prices')
        }
      }
    })
  })

  self.groupPricesTable.on('click', 'i.select-price', async function () {
    const rowData = self.groupPricesTable.row($(this).closest('tr')).data()
    const emptyPrice = self.currentJobGroupPrices.find(el => el.price_value == null)
    if (emptyPrice) {
      Swal.fire({
        title: 'Empty Prices Exist!',
        text: `Some prices aren't filled up for this job. Are you sure you want to select price from ${rowData.forwarder.forwarder_name} regardless?`,
        icon: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancel',
        confirmButtonColor: '#dc3545',
        confirmButtonText: 'Confirm'
      }).then(async result => {
        if (result.isConfirmed) {
          await self.selectPrice(rowData.price_id, 'selectGroup')
          self.refreshTable('group-prices')
        }
      })
    } else {
      await self.selectPrice(rowData.price_id, 'selectGroup')
      self.refreshTable('group-prices')
    }
  })

  self.groupPricesTable.on('click', 'i.unselect-price', async function () {
    const rowData = self.groupPricesTable.row($(this).closest('tr')).data()
    await self.selectPrice(rowData.price_id, 'unSelectJob')
    self.refreshTable('group-prices')
  })
}

IndividualsClass.prototype.refreshTable = function (tableName) {
  let self = this
  $(`#${tableName}-table`).unbind('click')
  $(`#${tableName}-table`).DataTable().clear()
  $(`#${tableName}-table`).DataTable().destroy()
  if (tableName == 'jobs') self.initJobsTable()
  if (tableName == 'pieces') self.initPiecesTable()
  if (tableName == 'prices') self.initPricesTable()
  if (tableName == 'group-prices') self.initGroupPricesTable()
}

IndividualsClass.prototype.initCities = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/cities', { headers: self.Helpers.getHeaders() })
    self.cities = response.data
    for (i = 0; i < self.cities.length; i++) {
      $('#ex-select')[0].selectize.addOption({ text: `${self.cities[i].city_name} - ${self.cities[i].country_name}`, value: self.cities[i].city_id })
      $('#to-select')[0].selectize.addOption({ text: `${self.cities[i].city_name} - ${self.cities[i].country_name}`, value: self.cities[i].city_id })
      $('#group-ex-select')[0].selectize.addOption({ text: `${self.cities[i].city_name} - ${self.cities[i].country_name}`, value: self.cities[i].city_id })
      $('#group-to-select')[0].selectize.addOption({ text: `${self.cities[i].city_name} - ${self.cities[i].country_name}`, value: self.cities[i].city_id })
    }
    console.log(`Cities initialized`)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.cities = []
  }
}

IndividualsClass.prototype.initModes = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/modes', { headers: self.Helpers.getHeaders() })
    self.modes = response.data
    for (i = 0; i < self.modes.length; i++) {
      $('#mode-select').append(new Option(self.modes[i].mode_description, self.modes[i].mode_id))
    }
    $('#mode-select').selectize()
    console.log(`Modes initialized`)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.modes = []
  }
}

IndividualsClass.prototype.initUserDepartments = async function (userId) {
  let self = this
  try {
    const response = await axios.get(`${self.Helpers.API_URL}/departments/user/${self.Helpers.user.user_id}`, { headers: self.Helpers.getHeaders() })
    self.userDepartments = response.data
    for (i = 0; i < self.userDepartments.length; i++) {
      $('#department-select').append(new Option(self.userDepartments[i].department_description, self.userDepartments[i].department_id))
    }
    $('#department-select').selectize()

    console.log(`User Departments initialized`)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.userDepartments = []
  }
}

IndividualsClass.prototype.initVessels = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/vessels', { headers: self.Helpers.getHeaders() })
    self.vessels = response.data
    for (i = 0; i < self.vessels.length; i++) {
      $('#vessel-select').append(new Option(self.vessels[i].vessel_description, self.vessels[i].vessel_id))
    }
    $('#vessel-select').selectize()
    console.log(`Vessels initialized`)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.vessels = []
  }
}

IndividualsClass.prototype.initServiceTypes = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/servicetypes', { headers: self.Helpers.getHeaders() })
    for (let serviceType of response.data) {
      if (serviceType.service_type_group !== 'Individuals') continue
      self.serviceTypes.push(serviceType)
      $('#service-type-select').append(new Option(serviceType.service_type_description, serviceType.service_type_id))
    }
    $('#service-type-select').selectize({
      plugins: ['clear_button']
    })
    console.log(`Service Types initialized`)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.vessels = []
  }
}

IndividualsClass.prototype.initForwarders = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/forwarders', { headers: self.Helpers.getHeaders() })
    self.forwarders = response.data
    console.log(`Forwarders initialized`)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.forwarders = []
  }
}

IndividualsClass.prototype.initProducts = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/products', { headers: self.Helpers.getHeaders() })
    self.products = response.data
    console.log(`Products initialized`)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.products = []
  }
}

IndividualsClass.prototype.initCountries = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/countries', { headers: self.Helpers.getHeaders() })
    self.countries = response.data

    console.log(`Countries initialized`)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.forwarders = []
  }
}

IndividualsClass.prototype.initCurrencies = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/currencies', { headers: self.Helpers.getHeaders() })
    self.currencies = response.data
    for (i = 0; i < self.currencies.length; i++) {
      $('#currency-select')[0].selectize.addOption({ text: self.currencies[i].currency_name, value: self.currencies[i].currency_name })
      $('#group-currency-select')[0].selectize.addOption({ text: self.currencies[i].currency_name, value: self.currencies[i].currency_name })
    }
    console.log(`Currencies initialized`)
    $('#add-job-btn').attr('disabled', null)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.currencies = []
  }
}

IndividualsClass.prototype.initJobs = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/jobs/individuals', { headers: self.Helpers.getHeaders() })
    let tempJobs = response.data
    let groupedJobs = []
    let individualJobs = []
    self.jobs = []
    for (let i = 0; i < tempJobs.length; i++) {
      tempJobs[i].request_date = self.Helpers.changeMysqlDateToNormal(tempJobs[i].job_created_at)
      tempJobs[i].service_description = ''
      if (_.has(tempJobs[i], 'service_type.service_type_description')) tempJobs[i].service_description = tempJobs[i].service_type.service_type_description

      if (tempJobs[i].group != null) {
        groupedJobs.push(tempJobs[i])
      } else {
        individualJobs.push(tempJobs[i])
      }

      let jobTempVessels = []
      for (let vesselId of tempJobs[i].vessel_ids) {
        let vessel = self.vessels.find(el => el.vessel_id == parseInt(vesselId))
        if (jobTempVessels.indexOf(vessel.vessel_description) == -1) jobTempVessels.push(vessel.vessel_description)
      }
      tempJobs[i].vessels = jobTempVessels.join(',')
    }

    let sortedGroupedJobs = _.orderBy(groupedJobs, ['group.group_id', 'job_id'], ['desc'])
    let sortedIndJobs = _.orderBy(individualJobs, ['job_id'], ['desc'])
    self.jobs = _.concat(sortedGroupedJobs, sortedIndJobs)
    console.log(`Jobs initialized`)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.jobs = []
  }
}

IndividualsClass.prototype.confirmJob = async function (jobId) {
  let self = this
  try {
    await axios.get(self.Helpers.API_URL + `/jobs/individuals/confirm/${jobId}`, { headers: self.Helpers.getHeaders() })
    return true
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    return false
  }
}

IndividualsClass.prototype.confirmJobGroup = async function (groupId) {
  let self = this
  try {
    await axios.get(self.Helpers.API_URL + `/jobgroups/confirm/${groupId}`, { headers: self.Helpers.getHeaders() })
    return true
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    return false
  }
}

IndividualsClass.prototype.addJob = async function (job) {
  let self = this
  try {
    const response = await axios.post(self.Helpers.API_URL + '/jobs', job, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', `Job Created!`)
    return response.data
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    return null
  }
}

IndividualsClass.prototype.updateJob = async function (job) {
  let self = this
  try {
    const response = await axios.put(self.Helpers.API_URL + '/jobs', job, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Job Updated!')
    return response.data
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    return false
  }
}

IndividualsClass.prototype.patchJob = async function (jobId, jobContent) {
  let self = this
  try {
    const response = await axios.patch(`${self.Helpers.API_URL}/jobs/${jobId}`, jobContent, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Job Notes Updated!')
    return response.data
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    return null
  }
}

IndividualsClass.prototype.patchJobGroup = async function (groupId, groupContent) {
  let self = this
  try {
    const response = await axios.patch(`${self.Helpers.API_URL}/jobgroups/${groupId}`, groupContent, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Group Notes Updated!')
    return response.data
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    return null
  }
}

IndividualsClass.prototype.addPiece = async function (piece) {
  let self = this
  try {
    const response = await axios.post(self.Helpers.API_URL + '/pieces', piece, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Order Added!')
    return response.data.piece_id
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    return null
  }
}

IndividualsClass.prototype.addPrice = async function (price) {
  let self = this
  try {
    const response = await axios.post(self.Helpers.API_URL + '/prices', price, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Price Created!')
    return response.data
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    return null
  }
}

IndividualsClass.prototype.updatePrice = async function (price) {
  let self = this
  try {
    await axios.put(self.Helpers.API_URL + '/prices', price, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Price Updated!')
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

IndividualsClass.prototype.updateJobGroup = async function (group) {
  let self = this
  try {
    await axios.put(self.Helpers.API_URL + `/jobgroups`, group, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Group Updated!')
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

IndividualsClass.prototype.deleteJob = async function (jobId) {
  let self = this
  try {
    await axios.delete(self.Helpers.API_URL + `/jobs/${jobId}`, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Job Deleted!')
    return true
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    return false
  }
}

IndividualsClass.prototype.deletePiece = async function (pieceId) {
  let self = this
  try {
    await axios.delete(self.Helpers.API_URL + `/pieces/${pieceId}`, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Piece Deleted!')
    return true
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    return false
  }
}

IndividualsClass.prototype.deletePrice = async function (priceId) {
  let self = this
  try {
    await axios.delete(self.Helpers.API_URL + `/prices/${priceId}`, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Price Deleted!')
    return true
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    return false
  }
}

IndividualsClass.prototype.requestOfferForPrice = async function (priceId, type) {
  let self = this
  try {
    await axios.get(self.Helpers.API_URL + `/prices/${priceId}/${type}/requestOffer`, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Price Offer Requested!')
    return true
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    return false
  }
}

IndividualsClass.prototype.selectPrice = async function (priceId, uri = 'selectJob') {
  let self = this
  try {
    await axios.get(self.Helpers.API_URL + `/prices/${priceId}/${uri}`, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Price Updated!')
    return true
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    return false
  }
}

IndividualsClass.prototype.getJobPieces = async function (jobId) {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + `/pieces/job/${jobId}`, { headers: self.Helpers.getHeaders() })
    return response.data
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    return []
  }
}

IndividualsClass.prototype.getJobPrices = async function (jobId) {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + `/prices/job/${jobId}`, { headers: self.Helpers.getHeaders() })
    return response.data
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    return []
  }
}

IndividualsClass.prototype.renderAddJobModal = async function (jobId = null, focusIndex = 0, hidePiecesInputs = true, hidePricesInputs = true, hideGroupInputs = true, hideGroupPricesInputs = true) {
  let self = this
  $('#show-price-table-btn').click()
  $('#save-job-notes-btn').show()
  $('#job-notes-div').show()
  $('#request-price-offer-btn').show()
  self.currentJob = await self.Helpers.getJobInfo(jobId)

  self.handleModalTabs()
  self.renderTransportationTab()
  if (jobId == null) {
    $('#modal-title-text').html('Add Job')
  } else {
    $('#modal-title-text').html(`Edit Job ID: ${jobId}`)
    self.renderPiecesTab(hidePiecesInputs)
    self.renderPricesTab(hidePricesInputs)
    self.renderGroupTab()
    self.renderGroupPricesTab(hideGroupPricesInputs)
    self.renderCostsTab()
  }
  self.Wizzard.focusTab(focusIndex)
  if (!$('#add-job-modal').hasClass('show')) $('#add-job-modal').modal('show')
}

IndividualsClass.prototype.renderTransportationTab = function () {
  let self = this
  if (self.userDepartments.length == 1) {
    $('#department-select')[0].selectize.setValue(self.userDepartments[0].department_id)
  } else {
    $('#department-select')[0].selectize.setValue('')
  }
  $('#ex-select')[0].selectize.setValue('')
  $('#to-select')[0].selectize.setValue('')
  $('#deadline_date').val('')
  $('#service-type-select')[0].selectize.setValue('')
  $('#mode-select')[0].selectize.setValue('')

  if (self.currentJob != null) {
    $('#department-select')[0].selectize.setValue(!self.currentJob.department ? self.currentJob.job_department_id : self.currentJob.department.department_id)
    $('#ex-select')[0].selectize.setValue(!self.currentJob.ex ? self.currentJob.job_ex_city_id : self.currentJob.ex.city_id)
    $('#to-select')[0].selectize.setValue(!self.currentJob.to ? self.currentJob.job_to_city_id : self.currentJob.to.city_id)
    $('#mode-select')[0].selectize.setValue(!self.currentJob.mode ? self.currentJob.job_mode_id : self.currentJob.mode.mode_id)
    $('#deadline_date').val(self.currentJob.job_deadline)

    if (_.has(self.currentJob, 'service_type.service_type_id')) {
      $('#service-type-select')[0].selectize.setValue(self.currentJob.service_type.service_type_id)
    }
    if (self.currentJob.job_service_type_id && self.currentJob.job_service_type_id != null) {
      $('#service-type-select')[0].selectize.setValue(self.currentJob.job_service_type_id)
    }
  }
}

IndividualsClass.prototype.renderPiecesTab = async function (hideInputs) {
  let self = this
  self.refreshTable('pieces')
  self.initOrderInputs(hideInputs)
}

IndividualsClass.prototype.renderPricesTab = function (hideInputs) {
  let self = this
  self.refreshTable('prices')
  self.initPriceInputs(hideInputs)

  $('#job-notes').val(self.currentJob.job_notes)

  if (self.currentJob != null && self.currentJob.group != null) {
    $('#add-price-btn').hide()
    $('#request-price-offer-btn').hide()
  }
}

IndividualsClass.prototype.renderGroupTab = function (hideInputs = true) {
  let self = this
  if (self.currentJob == null || self.currentJob.group == null) return
  $('#group-ex-select')[0].selectize.setValue(self.currentJob.group.group_ex.city_id)
  $('#group-to-select')[0].selectize.setValue(self.currentJob.group.group_to.city_id)
  $('#group-deadline').val(self.currentJob.group.group_deadline)
}

IndividualsClass.prototype.renderGroupPricesTab = function (hideInputs) {
  let self = this
  self.initGroupPriceInputs(hideInputs)
  if (self.currentJob == null || self.currentJob.group == null) return
  self.refreshTable('group-prices')
  $('#add-group-price-btn').show()
  $('#request-group-offer-btn').show()
  $('#group-notes-div').show()
  $('#save-group-notes-btn').show()
  $('#group-notes').val(self.currentJob.group.group_notes)
}

IndividualsClass.prototype.renderCostsTab = function () {
  let self = this
  if (self.currentJob == null || self.currentJob.costs == null) return
  $('.group-costs-data').each(function () {
    $(this).hide()
  })
  $('#costs-job-weight').val(self.currentJob.costs.job_weight)
  $('#costs-job-cost').val(self.currentJob.costs.job_cost)
  $('#costs-job-cost-per-kg').val(self.currentJob.costs.job_cost_per_kg)
  $('#costs-shared-cost').val(self.currentJob.costs.job_shared_cost)
  if (self.currentJob.group != null && self.currentJob.group.costs != null) {
    $('.group-costs-data').each(function () {
      $(this).show()
    })
    $('#costs-group-cost').val(self.currentJob.group.costs.group_cost)
    $('#costs-group-cost-per-kg').val(self.currentJob.group.costs.group_cost_per_kg)
    $('#costs-group-weight').val(self.currentJob.group.costs.group_weight)
  }
}

IndividualsClass.prototype.handleModalTabs = function () {
  let self = this
  self.Wizzard.showTab('transportations')
  self.Wizzard.focusTab(0)

  if (self.currentJob == null) {
    self.Wizzard.hideTab('pieces')
    self.Wizzard.hideTab('forwarders')
    self.Wizzard.hideTab('group')
    self.Wizzard.hideTab('costs')
    self.Wizzard.hideTab('group-forwarders')
    return
  }

  self.Wizzard.showTab('pieces')
  if (self.currentJob.pieces_count > 0) {
    self.Wizzard.showTab('forwarders')
  } else {
    self.Wizzard.hideTab('forwarders')
  }
  if (self.currentJob.group != null) {
    self.Wizzard.showTab('group')

    let emptyPiecesJob = self.jobs.find(el => el.group != null && el.group.group_id == self.currentJob.group.group_id && el.pieces_count == 0)
    if (!emptyPiecesJob) {
      self.Wizzard.showTab('group-forwarders')
    } else {
      self.Wizzard.hideTab('group-forwarders')
    }
  } else {
    self.Wizzard.hideTab('group')
    self.Wizzard.hideTab('group-forwarders')
  }
  if (self.currentJob.costs != null) {
    self.Wizzard.showTab('costs')
  } else {
    self.Wizzard.hideTab('costs')
  }
}

IndividualsClass.prototype.initOrderInputs = function (hideInputs = false) {
  let self = this
  self.initProductsSelectForDepartment(self.currentJob.department.department_id)
  $('#vessel-select')[0].selectize.setValue('')
  $('#piece-quantity').val('')
  $('#piece-weight').val('')
  $('#piece-dimensions').val('')
  $('#piece-reference').val('')
  $('#piece-value-of-goods').val('')
  $('#piece-description').val('')
  if (hideInputs) {
    $('#add-piece-btn').show()
  } else {
    $('#add-piece-btn').hide()
  }

  $('.add-piece-form-div').each(function () {
    if (!hideInputs) {
      $(this).show()
    } else {
      $(this).hide()
    }
  })
}

IndividualsClass.prototype.initPriceInputs = function (hideInputs = false) {
  let self = this
  $('#forwarder-select')[0].selectize.enable()
  self.refreshJobForwardersSelect(true)
  $('#forwarder-select')[0].selectize.setValue('')
  $('#currency-select')[0].selectize.setValue(self.Helpers.GLOBAL_CURRENCY)
  $('#price-value').val('')
  $('.add-price-form-div').each(function () {
    if (!hideInputs) {
      $(this).show()
    } else {
      $(this).hide()
    }
  })
}

IndividualsClass.prototype.initGroupPriceInputs = function (hideInputs = false, changeSelect = true) {
  let self = this
  $('#group-forwarder-select')[0].selectize.enable()
  if (changeSelect) self.refreshGroupForwardersSelect(true)
  $('#group-forwarder-select')[0].selectize.setValue('')
  $('#group-currency-select')[0].selectize.setValue(self.Helpers.GLOBAL_CURRENCY)
  $('#group-price-value').val('')
  $('.add-group-price-form-div').each(function () {
    if (!hideInputs) {
      $(this).show()
    } else {
      $(this).hide()
    }
  })
}

IndividualsClass.prototype.refreshJobForwardersSelect = function (banForwarder = true) {
  let self = this
  $('#forwarder-select')[0].selectize.destroy()
  $('#forwarder-select').selectize()
  for (i = 0; i < self.forwarders.length; i++) {
    if (banForwarder) {
      let foundForwarder = self.currentJobPrices.find(el => el.forwarder.forwarder_id == self.forwarders[i].forwarder_id)
      if (foundForwarder) continue
    }

    $('#forwarder-select')[0].selectize.addOption({ text: self.forwarders[i].forwarder_name, value: self.forwarders[i].forwarder_id })
  }
}

IndividualsClass.prototype.refreshGroupForwardersSelect = function (banForwarder = true) {
  let self = this
  $('#group-forwarder-select')[0].selectize.destroy()
  $('#group-forwarder-select').selectize()
  for (i = 0; i < self.forwarders.length; i++) {
    if (banForwarder) {
      let foundForwarder = self.currentJobGroupPrices.find(el => el.forwarder.forwarder_id == self.forwarders[i].forwarder_id)
      if (foundForwarder) continue
    }
    $('#group-forwarder-select')[0].selectize.addOption({ text: self.forwarders[i].forwarder_name, value: self.forwarders[i].forwarder_id })
  }
}

IndividualsClass.prototype.initProductsSelectForDepartment = async function (departmentId) {
  let self = this
  $('#products-select')[0].selectize.destroy()
  $('#products-select').empty()
  for (i = 0; i < self.products.length; i++) {
    if (self.products[i].product_department_id != departmentId) continue
    $('#products-select').append(new Option(self.products[i].product_description, self.products[i].product_id))
  }
  $('#products-select').selectize()
}

window.IndividualsClass = new IndividualsClass()
