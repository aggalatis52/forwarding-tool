let DeletedIndividualsClass = function () {
  this.cities = []
  this.modes = []
  this.vessels = []
  this.products = []
  this.serviceTypes = []
  this.forwarders = []
  this.currencies = []
  this.Helpers = new HelpersClass()
  this.Helpers.validateLogin()
  this.Helpers.bindCloseBtnsAlerts()
  this.Helpers.bindMovingEvents('job-modal-header')
  this.Helpers.bindMovingEvents('group-modal-header')
  this.Helpers.bindMovingEvents('costs-modal-header')
  this.bindEventsOnButtons()
  this.initWizzard()
  this.initPiecesTable()
  this.initPricesTable()
  this.initGroupPricesTable()
  this.initDeletedJobsTable()
  this.initCities()
  this.initModes()
  this.initVessels()
  this.initProducts()
  this.initServiceTypes()
  this.initForwarders()
  this.initCurrencies()
}

DeletedIndividualsClass.prototype.bindEventsOnButtons = function () {
  let self = this

  $('#products-select').chosen()
  $('#forwarder-select').chosen()
  $('#group-forwarder-select').chosen()
  $('#currency-select').chosen()
  $('#group-currency-select').chosen()
  $('#mode-select').chosen()
  $('#service-type-select').chosen()
  $('#vessel-select').chosen()
  $('#ex-select').chosen()
  $('#to-select').chosen()
  $('#group-to-select').chosen()
  $('#group-ex-select').chosen()

  $('#logout-ref').on('click', function () {
    self.Helpers.handleLogout()
  })

  $('#clear-jobs-btn').on('click', function () {
    Swal.fire({
      title: 'Clear Deleted Jobs?',
      text: `Are you sure you want to permantly remove all deleted jobs?`,
      icon: 'info',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonColor: '#dc3545',
      confirmButtonText: 'Confirm'
    }).then(result => {
      if (result.isConfirmed) self.clearAllJobs()
    })
  })
}

DeletedIndividualsClass.prototype.initWizzard = function () {
  let self = this

  $('.job-wizzard-tab').on('click', function () {
    const selectedTab = $(this).data('wizzard')
    const selectedWizzardIndex = self.wizzardPages.indexOf(selectedTab)
    if (self.activeWizzardIndex == selectedWizzardIndex) return
    self.activeWizzardIndex = selectedWizzardIndex
    self.updateWizzard()
  })
}

DeletedIndividualsClass.prototype.updateWizzard = function () {
  let self = this
  const activeTab = self.wizzardPages[self.activeWizzardIndex]
  $('#previous-tab-btn').show()
  $('#next-tab-btn').show()

  if (self.activeWizzardIndex == 0) $('#previous-tab-btn').hide()

  if (self.activeWizzardIndex == self.wizzardPages.length - 1) $('#next-tab-btn').hide()

  $('.job-wizzard-tab').each(function () {
    $(this).removeClass('active')
  })
  $(`#wizzard-${activeTab}-tab`).addClass('active')
  $('.wizzard-content').each(function () {
    $(this).hide()
  })
  $(`#${activeTab}-content`).show()
}

DeletedIndividualsClass.prototype.initDeletedJobsTable = async function () {
  let self = this
  await self.initJobs()
  self.jobsTable = $('#jobs-table').DataTable({
    data: self.jobs,
    bLengthChange: false,
    paging: false,
    ordering: false,
    info: false,
    fixedHeader: {
      headerOffset: 100,
      header: true,
      footer: false
    },
    columns: [
      { title: 'JOB ID', orderable: false, data: 'job_id' },
      {
        title: 'GROUP ID',
        orderable: false,
        data: null,
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.group == null) {
            $(td).html('')
          } else {
            $(td).html(rowData.group.group_id)
          }
        }
      },
      { title: 'REQ. DATE', orderable: false, data: 'request_date' },
      { title: 'USER', orderable: false, data: 'user.user_username' },
      { title: 'DEPARTMENT', orderable: false, data: 'department.department_description' },
      {
        title: 'PIECES',
        orderable: false,
        data: 'pieces_count'
      },
      {
        title: 'MODE',
        orderable: false,
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.mode.mode_id == 5) $(td).css('color', 'blue').css('font-weight', 'bold')
        },
        data: 'mode.mode_description'
      },
      {
        title: 'SERVICE',
        orderable: false,
        data: 'service_description'
      },
      { title: 'VESSELS', orderable: false, data: 'vessels_description' },
      { title: 'EX', orderable: false, data: 'ex.city_name' },
      { title: 'TO', orderable: false, data: 'to.city_name' },
      { title: 'DEADLINE', orderable: false, data: 'job_deadline' },
      {
        title: 'FORWARDERS',
        orderable: false,
        data: 'forwarders_description'
      },
      { title: 'REFERENCE', orderable: false, data: 'job_reference' },
      {
        title: 'SHARED COST ($)',
        orderable: false,
        data: 'costs',
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.costs != null) {
            $(td).html(rowData.costs.job_shared_cost)
          }
        }
      },
      {
        title: 'ACTIONS',
        orderable: false,
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.service_type == null) $(td).children('.confirm-job').hide()
          if (rowData.group == null) $(td).children('.edit-group').hide()

          if (rowData.costs == null) {
            $(td).children('.confirm-job').hide()
            $(td).children('.job-costs').hide()
          }

          if (rowData.group != null) {
            if (rowData.costs == null || rowData.costs.group == null) {
              $(td).children('.confirm-job').hide()
            }
          }
        },
        defaultContent: "<i class='fa fa-pencil job-edit action-btn' title='modify' id='delete-me' style='cursor: pointer'></i><i class='fa fa-usd job-costs action-btn' title='costs' style='cursor: pointer'></i><i class='fa fa-cubes edit-group action-btn' title='group' style='cursor: pointer'></i>"
      }
    ]
  })

  self.jobsTable.on('click', 'i.job-edit', function () {
    const rowData = self.jobsTable.row($(this).closest('tr')).data()
    if (!self.Helpers.checkIfUserHasPriviledges(rowData.user.user_username)) {
      self.Helpers.swalUserPermissionError()
      return
    }
    self.currentJob = rowData
    self.renderAddJobModal()
  })

  self.jobsTable.on('click', 'i.job-costs', function () {
    const rowData = self.jobsTable.row($(this).closest('tr')).data()
    if (!self.Helpers.checkIfUserHasPriviledges(rowData.user.user_username)) {
      self.Helpers.swalUserPermissionError()
      return
    }
    self.renderCostsModal(rowData)
  })

  self.jobsTable.on('click', 'i.edit-group', function () {
    const rowData = self.jobsTable.row($(this).closest('tr')).data()
    $('#show-group-price-table-btn').click()
    if (!self.Helpers.checkIfUserHasPriviledges(rowData.user.user_username)) {
      self.Helpers.swalUserPermissionError()
      return
    }
    self.currentJobGroup = rowData.group
    self.renderGroupModal()
  })
}

DeletedIndividualsClass.prototype.initPiecesTable = function () {
  let self = this
  self.piecesTable = $('#pieces-table').DataTable({
    data: self.currentJobPieces,
    bLengthChange: false,
    paging: false,
    ordering: false,
    info: false,
    columns: [
      { title: 'ID', orderable: false, data: 'piece_id' },
      { title: 'Vessel', orderable: false, data: 'vessel_description' },
      { title: 'Quantity', orderable: false, data: 'piece_quantity' },
      { title: 'Description', orderable: false, data: 'piece_description' },
      { title: 'Products', orderable: false, data: 'products_description' },
      { title: 'Weight (kg)', orderable: false, data: 'piece_weight' },
      { title: 'Dimensions', orderable: false, data: 'piece_dimensions' },
      {
        title: 'Actions',
        orderable: false,
        defaultContent: "<i class='fa fa-qrcode qrcode-piece action-btn' title='qr-code' style='cursor: pointer' ></i><i class='fa fa-trash delete-piece action-btn' title='delete' style='cursor: pointer' ></i>"
      }
    ]
  })
}

DeletedIndividualsClass.prototype.initPricesTable = function () {
  let self = this
  self.pricesTable = $('#prices-table').DataTable({
    data: self.currentJobPrices,
    bLengthChange: false,
    paging: false,
    ordering: false,
    info: false,
    columns: [
      { title: 'ID', orderable: false, data: 'price_id' },
      { title: 'Forwarder', orderable: false, data: 'forwarder.forwarder_name' },
      { title: 'Price', orderable: false, data: 'price_value' },
      { title: 'Currency', orderable: false, data: 'price_currency' },
      { title: 'Price ($)', orderable: false, data: 'price_base_value' },
      {
        title: 'Offer Requested',
        orderable: false,
        data: 'price_offer_requested',
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.price_offer_requested == true) {
            $(td).css('color', 'green').css('text-align', 'center')
            $(td).html(`<i class="fa fa-check action-btn"></i>`)
          } else {
            $(td).css('color', 'red').css('text-align', 'center')
            $(td).html(`<i class="fa fa-times action-btn"></i>`)
          }
        }
      },
      {
        title: 'Actions',
        orderable: false,
        defaultContent:
          "<i class='fa fa-pencil edit-price action-btn' title='modify'  style='cursor: pointer'></i><i class='fa fa-envelope-o mail-price action-btn' title='request-offer' style='cursor: pointer' ></i><i class='fa fa-check select-price action-btn' title='select' style='cursor: pointer' ></i><i class='fa fa-trash delete-price action-btn' title='delete' style='cursor: pointer' ></i>"
      }
    ],
    rowCallback: function (row, data, index, cells) {
      if (data.price_selected) $('td', row).css('background-color', '#8fbc8f')
    }
  })

  self.pricesTable.on('click', 'i.edit-price', function () {
    const rowData = self.pricesTable.row($(this).closest('tr')).data()
    self.currentPrice = rowData
    $('#forwarder-select').val(rowData.forwarder.forwarder_id)
    $('#forwarder-select').trigger('chosen:updated')
    $('#currency-select').val(rowData.price_currency)
    $('#currency-select').trigger('chosen:updated')
    $('#forwarders-table-div').hide()
    $('#price-value').val(rowData.price_value).attr('disabled', null)
    $('#price-request-offer').prop('checked', false)
    $('.add-price-form-div').each(function () {
      $(this).show()
    })
  })

  self.pricesTable.on('click', 'i.delete-price', function () {
    const rowIndex = $(this).closest('tr').index()
    const rowData = self.pricesTable.row($(this).closest('tr')).data()
    Swal.fire({
      title: 'Delete Price?',
      text: 'Are you sure you want to delete this price?',
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonColor: '#dc3545',
      confirmButtonText: 'Confirm'
    }).then(async result => {
      if (result.isConfirmed) {
        const priceDeleted = await self.deletePrice(rowData.price_id)
        if (priceDeleted) {
          self.currentJobPrices.splice(rowIndex, 1)
          self.refreshPricesTable()
        }
      }
    })
  })

  self.pricesTable.on('click', 'i.select-price', function () {
    const rowIndex = $(this).closest('tr').index()
    for (let i = 0; i < self.currentJobPrices.length; i++) {
      self.currentJobPrices[i].price_selected = false
    }
    self.currentJobPrices[rowIndex].price_selected = true
    self.refreshPricesTable()
  })
}

DeletedIndividualsClass.prototype.initGroupPricesTable = function () {
  let self = this
  self.groupPricesTable = $('#group-prices-table').DataTable({
    data: self.currentJobGroupPrices,
    bLengthChange: false,
    paging: false,
    ordering: false,
    info: false,
    columns: [
      { title: 'Forwarder', orderable: false, data: 'forwarder.forwarder_name' },
      { title: 'Price', orderable: false, data: 'price_value' },
      { title: 'Currency', orderable: false, data: 'price_currency' },
      { title: 'Price ($)', orderable: false, data: 'price_base_value' },
      {
        title: 'Offer Requested',
        orderable: false,
        data: 'price_offer_requested',
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.price_offer_requested == true) {
            $(td).css('color', 'green').css('text-align', 'center')
            $(td).html(`<i class="fa fa-check action-btn"></i>`)
          } else {
            $(td).css('color', 'red').css('text-align', 'center')
            $(td).html(`<i class="fa fa-times action-btn"></i>`)
          }
        }
      },
      {
        title: 'Actions',
        orderable: false,
        defaultContent:
          "<i class='fa fa-pencil edit-price action-btn' title='modify'  style='cursor: pointer'></i><i class='fa fa-envelope-o mail-price action-btn' title='request-offer' style='cursor: pointer' ></i><i class='fa fa-check select-price action-btn' title='select' style='cursor: pointer' ></i><i class='fa fa-trash delete-price action-btn' title='delete' style='cursor: pointer' ></i>"
      }
    ],
    rowCallback: function (row, data, index, cells) {
      if (data.price_selected) $('td', row).css('background-color', '#8fbc8f')
    }
  })

  self.groupPricesTable.on('click', 'i.edit-price', function () {
    const rowData = self.groupPricesTable.row($(this).closest('tr')).data()
    self.currentGroupPrice = rowData
    $('#group-forwarder-select').val(rowData.forwarder.forwarder_id)
    $('#group-forwarder-select').trigger('chosen:updated')
    $('#group-currency-select').val(rowData.price_currency)
    $('#group-currency-select').trigger('chosen:updated')
    $('#group-price-value').val(rowData.price_value)
    $('#group-price-request-offer').prop('checked', false)

    $('#group-forwarders-table-div').hide()
    $('.add-group-price-form-div').each(function () {
      $(this).show()
    })
  })

  self.groupPricesTable.on('click', 'i.delete-price', function () {
    const rowIndex = $(this).closest('tr').index()
    Swal.fire({
      title: 'Delete Price?',
      text: 'Are you sure you want to delete this price?',
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonColor: '#dc3545',
      confirmButtonText: 'Confirm'
    }).then(result => {
      if (result.isConfirmed) {
        self.currentJobGroupPrices.splice(rowIndex, 1)
        self.refreshGroupPricesTable()
      }
    })
  })

  self.groupPricesTable.on('click', 'i.select-price', function () {
    const rowIndex = $(this).closest('tr').index()
    for (let i = 0; i < self.currentJobGroupPrices.length; i++) {
      self.currentJobGroupPrices[i].price_selected = false
    }
    self.currentJobGroupPrices[rowIndex].price_selected = true
    self.refreshGroupPricesTable()
  })
}

DeletedIndividualsClass.prototype.refreshDeletedJobsTable = function () {
  let self = this
  $('#jobs-table').unbind('click')
  $('#jobs-table').DataTable().clear()
  $('#jobs-table').DataTable().destroy()
  self.initDeletedJobsTable()
}

DeletedIndividualsClass.prototype.refreshPiecesTable = function () {
  let self = this
  $('#pieces-table').unbind('click')
  $('#pieces-table').DataTable().clear()
  $('#pieces-table').DataTable().destroy()
  self.initPiecesTable()
}

DeletedIndividualsClass.prototype.refreshPricesTable = function () {
  let self = this
  $('#prices-table').unbind('click')
  $('#prices-table').DataTable().clear()
  $('#prices-table').DataTable().destroy()
  self.initPricesTable()
}

DeletedIndividualsClass.prototype.refreshGroupPricesTable = function () {
  let self = this
  $('#group-prices-table').unbind('click')
  $('#group-prices-table').DataTable().clear()
  $('#group-prices-table').DataTable().destroy()
  self.initGroupPricesTable()
}

DeletedIndividualsClass.prototype.initCities = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/cities', { headers: self.Helpers.getHeaders() })
    self.cities = response.data
    $('#ex-select').empty()
    $('#to-select').empty()
    $('#group-ex-select').empty()
    $('#group-to-select').empty()
    $('#ex-select').append('<option></option>')
    $('#to-select').append('<option></option>')
    $('#group-ex-select').append('<option></option>')
    $('#group-to-select').append('<option></option>')
    for (i = 0; i < self.cities.length; i++) {
      $('#ex-select').append(new Option(self.cities[i].city_name, self.cities[i].city_id))
      $('#to-select').append(new Option(self.cities[i].city_name, self.cities[i].city_id))
      $('#group-ex-select').append(new Option(self.cities[i].city_name, self.cities[i].city_id))
      $('#group-to-select').append(new Option(self.cities[i].city_name, self.cities[i].city_id))
    }
    $('#ex-select').trigger('chosen:updated')
    $('#to-select').trigger('chosen:updated')
    $('#group-ex-select').trigger('chosen:updated')
    $('#group-to-select').trigger('chosen:updated')
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.cities = []
  }
}

DeletedIndividualsClass.prototype.initModes = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/modes', { headers: self.Helpers.getHeaders() })
    self.modes = response.data
    $('#mode-select').empty()
    $('#mode-select').append('<option></option>')
    for (i = 0; i < self.modes.length; i++) {
      $('#mode-select').append(new Option(self.modes[i].mode_description, self.modes[i].mode_id))
    }
    $('#mode-select').trigger('chosen:updated')
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.modes = []
  }
}

DeletedIndividualsClass.prototype.initVessels = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/vessels', { headers: self.Helpers.getHeaders() })
    self.vessels = response.data
    $('#vessel-select').empty()
    $('#vessel-select').append('<option></option>')
    for (i = 0; i < self.vessels.length; i++) {
      $('#vessel-select').append(new Option(self.vessels[i].vessel_description, self.vessels[i].vessel_id))
    }
    $('#vessel-select').trigger('chosen:updated')
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.vessels = []
  }
}

DeletedIndividualsClass.prototype.initServiceTypes = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/servicetypes', { headers: self.Helpers.getHeaders() })
    $('#service-type-select').empty()
    $('#service-type-select').append('<option></option>')
    for (let serviceType of response.data) {
      if (serviceType.service_type_group !== 'Individuals') continue
      self.serviceTypes.push(serviceType)
      $('#service-type-select').append(new Option(serviceType.service_type_description, serviceType.service_type_id))
    }
    $('#service-type-select').trigger('chosen:updated')
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.vessels = []
  }
}

DeletedIndividualsClass.prototype.initProducts = async function () {
  let self = this
  try {
    const userDepartmentId = self.Helpers.user.user_department_id
    const response = await axios.get(`${self.Helpers.API_URL}/products/${userDepartmentId}`, { headers: self.Helpers.getHeaders() })
    self.products = response.data
    $('#products-select').empty()
    $('#products-select').append('<option></option>')
    for (i = 0; i < self.products.length; i++) {
      $('#products-select').append(new Option(self.products[i].product_name, self.products[i].product_id))
    }
    $('#products-select').trigger('chosen:updated')
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.products = []
  }
}

DeletedIndividualsClass.prototype.initForwarders = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/forwarders', { headers: self.Helpers.getHeaders() })
    self.forwarders = response.data
    $('#forwarder-select').empty()
    $('#group-forwarder-select').empty()
    $('#forwarder-select').append('<option></option>')
    $('#group-forwarder-select').append('<option></option>')
    for (i = 0; i < self.forwarders.length; i++) {
      $('#forwarder-select').append(new Option(self.forwarders[i].forwarder_name, self.forwarders[i].forwarder_id))
      $('#group-forwarder-select').append(new Option(self.forwarders[i].forwarder_name, self.forwarders[i].forwarder_id))
    }
    $('#forwarder-select').trigger('chosen:updated')
    $('#group-forwarder-select').trigger('chosen:updated')
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.forwarders = []
  }
}

DeletedIndividualsClass.prototype.initCurrencies = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/currencies', { headers: self.Helpers.getHeaders() })
    self.currencies = response.data
    $('#currency-select').empty()
    $('#group-currency-select').empty()
    $('#currency-select').append('<option></option>')
    $('#group-currency-select').append('<option></option>')
    for (i = 0; i < self.currencies.length; i++) {
      $('#currency-select').append(new Option(self.currencies[i].currency_name, self.currencies[i].currency_name))
      $('#group-currency-select').append(new Option(self.currencies[i].currency_name, self.currencies[i].currency_name))
    }
    $('#currency-select').trigger('chosen:updated')
    $('#group-currency-select').trigger('chosen:updated')
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.currencies = []
  }
}

DeletedIndividualsClass.prototype.initJobs = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/jobs/deleted', { headers: self.Helpers.getHeaders() })
    let tempJobs = response.data
    let groupedJobs = []
    let individualJobs = []
    self.jobs = []
    for (let i = 0; i < tempJobs.length; i++) {
      tempJobs[i].pieces_count = `${tempJobs[i].pieces.length} Pieces`
      tempJobs[i].prices_count = `${tempJobs[i].prices.length} Forwarders`
      tempJobs[i].request_date = self.Helpers.changeMysqlDateToNormal(tempJobs[i].job_created_at)
      tempJobs[i].service_description = ''
      if (_.has(tempJobs[i], 'service_type.service_type_description')) tempJobs[i].service_description = tempJobs[i].service_type.service_type_description
      // Manipulate Vessels
      tempJobs[i].vessels = []
      for (let piece of tempJobs[i].pieces) {
        if (tempJobs[i].vessels.indexOf(piece.vessel.vessel_description) === -1) tempJobs[i].vessels.push(piece.vessel.vessel_description)
      }
      tempJobs[i].vessels_description = tempJobs[i].vessels.join(',')

      // Manipulate Forwarders
      tempJobs[i].forwarders = []
      for (let price of tempJobs[i].prices) {
        if (tempJobs[i].forwarders.indexOf(price.forwarder.forwarder_name) === -1) tempJobs[i].forwarders.push(price.forwarder.forwarder_name)
      }
      tempJobs[i].forwarders_description = tempJobs[i].forwarders.join(',')
      if (tempJobs[i].group != null) {
        groupedJobs.push(tempJobs[i])
      } else {
        individualJobs.push(tempJobs[i])
      }
    }

    let sortedGroupedJobs = _.orderBy(groupedJobs, ['group.group_id', 'job_id'], ['desc'])
    let sortedIndJobs = _.orderBy(individualJobs, ['job_id'], ['desc'])
    self.jobs = _.concat(sortedGroupedJobs, sortedIndJobs)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.jobs = []
  }
}

DeletedIndividualsClass.prototype.renderAddJobModal = function () {
  let self = this
  $('#show-piece-table-btn').click()
  $('#show-price-table-btn').click()
  if (self.currentJob == null) {
    // Render Modal to add new job
    $('#modal-title-text').html('Add Job')
    $('#wizzard-forwarders-tab').hide()
    $('#wizzard-pieces-tab').hide()
    $('#reference').val('')
    $('#notes').val('')
    $('#deadline_date').val('')
    $('#service-type-select').val(0).trigger('chosen:updated')
    $('#vessel-select').val('').trigger('chosen:updated')
    $('#products-select').val('').trigger('chosen:updated')
    $('#mode-select').val('').trigger('chosen:updated')
    $('#ex-select').val('').trigger('chosen:updated')
    $('#to-select').val('').trigger('chosen:updated')
    $('#piece-quantity').val('')
    $('#piece-weight').val('')
    $('#piece-dimensions').val('')
    $('#piece-value-of-goods').val('')
    $('#piece-description').val('')

    self.currentJobPrices = []
    self.refreshPricesTable()

    self.currentJobPieces = []
    self.refreshPiecesTable()

    self.activeWizzardIndex = 0
    self.updateWizzard()
  } else {
    // Render modal from currentJob
    $('#modal-title-text').html(`Edit Job ID: ${self.currentJob.job_id}`)
    $('#wizzard-forwarders-tab').show()
    $('#wizzard-pieces-tab').show()
    $('#reference').val(self.currentJob.job_reference)
    $('#notes').val(self.currentJob.job_notes)
    $('#deadline_date').val(self.currentJob.job_deadline)
    $('#vessel-select').val('').trigger('chosen:updated')
    $('#products-select').val('').trigger('chosen:updated')
    $('#mode-select').val(self.currentJob.mode.mode_id).trigger('chosen:updated')
    $('#ex-select').val(self.currentJob.ex.city_id).trigger('chosen:updated')
    $('#to-select').val(self.currentJob.to.city_id).trigger('chosen:updated')
    $('#piece-quantity').val('')
    $('#piece-weight').val('')
    $('#piece-dimensions').val('')
    $('#piece-value-of-goods').val('')
    $('#piece-description').val('')

    if (_.has(self.currentJob, 'service_type.service_type_id')) {
      $('#service-type-select').val(self.currentJob.service_type.service_type_id).trigger('chosen:updated')
    } else {
      $('#service-type-select').val(0).trigger('chosen:updated')
    }
    $('#service-type-select').trigger('chosen:updated')

    // Handle Pieces TAB
    let manipulatedPieces = []
    for (let piece of self.currentJob.pieces) {
      manipulatedPieces.push(self.manipulatePiece(piece))
    }
    self.currentJobPieces = manipulatedPieces
    self.refreshPiecesTable()

    // Handle Forwarders TAB

    let availableForwarders = self.getForwardersForCountry(self.currentJob.ex.city_country_id)
    $('#country-forwarders').val(availableForwarders.join(','))
    self.currentJobPrices = self.currentJob.prices
    self.refreshPricesTable()

    self.activeWizzardIndex = 0
    self.updateWizzard()
  }
  $('#add-job-modal').modal('show')
}

DeletedIndividualsClass.prototype.renderGroupModal = function () {
  let self = this
  $('#group-modal-title').html(`Edit Group ID: ${self.currentJobGroup.group_id}`)
  $('#group-ex-select').val(self.currentJobGroup.group_ex.city_id).trigger('chosen:updated')
  $('#group-to-select').val(self.currentJobGroup.group_to.city_id).trigger('chosen:updated')
  $('#group-deadline').val(self.currentJobGroup.group_deadline)
  self.currentJobGroupPrices = self.currentJobGroup.prices
  self.refreshGroupPricesTable()

  $('#group-modal').modal('show')
}

DeletedIndividualsClass.prototype.renderCostsModal = function (job) {
  let self = this

  console.log(job)
  $('#costs-modal-title').html(`Costs Job ID: ${job.job_id}`)
  $('#group-costs-div').hide()
  $('#costs-job-weight').val(job.costs.job_weight)
  $('#costs-job-cost').val(job.costs.job_cost)
  $('#costs-job-cost-per-kg').val(job.costs.job_cost_per_kg)
  $('#costs-shared-cost').val(job.costs.job_shared_cost)
  if (job.costs.group != null) {
    $('#group-costs-div').show()
    $('#costs-group-cost').val(job.costs.group.job_group_cost)
    $('#costs-group-cost-per-kg').val(job.costs.group.job_group_cost_per_kg)
    $('#costs-group-weight').val(job.costs.group.job_group_weight)
  }

  $('#costs-modal').modal('show')
}

DeletedIndividualsClass.prototype.manipulatePiece = function (piece) {
  let self = this

  let vesselId = piece.piece_vessel_id
  if (_.has(piece, 'vessel.vessel_id')) vesselId = piece.vessel.vessel_id
  const selectedVessel = self.vessels.find(el => el.vessel_id == vesselId)
  piece.vessel_description = selectedVessel.vessel_description
  let products_description = []
  for (let prod of piece.piece_products) {
    let productId = prod
    if (typeof prod === 'object') productId = prod.product_id
    let fullProduct = self.products.find(el => el.product_id == productId)
    products_description.push(fullProduct.product_name)
  }
  piece.products_description = products_description.join(',')
  return piece
}

DeletedIndividualsClass.prototype.clearAllJobs = async function () {
  let self = this
  try {
    await axios.delete(self.Helpers.API_URL + '/jobs/deleted', { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Jobs Cleared!')
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.modes = []
  }
}

new DeletedIndividualsClass()
