let ArchiveIndividualsClass = function () {
  this.Helpers = new HelpersClass()
  this.Helpers.validateLogin()
  this.Helpers.bindCloseBtnsAlerts()
  this.Helpers.bindMovingEvents('job-modal-mover', 'job-modal', 'job-modal-content')
  this.Helpers.bindMovingEvents('assignment-modal-mover', 'assignment-modal', 'assignment-modal-content')
  this.Wizzard = new WizzardClass(['transportation', 'pieces', 'forwarders', 'group', 'group-forwarders', 'costs', 'documents'])
  this.doneJobs = []
  this.departments = []
  this.selectedDoneJobs = []
  this.visitedGroups = []
  this.uploadedDocs = []
  this.selectedDoneJobsDestination = null
  this.selectedDoneJobsVessel = null
  this.currentJob = null
  this.doneJobsTable = null
  this.dispatchFileZone = new Dropzone('#document-upload-form', this.Helpers.getDropzoneConfig())

  this.bindEventsOnButtons()
  this.initVessels()
  this.initDepartments()
  this.initCities()
  this.initForwarders()
  this.initModes()
  this.initServiceTypes()
  this.initGroupPricesTable()
  this.initPiecesTable()
  this.initPricesTable()
  this.initDocumentsTable()
  this.initDoneJobsTable()
}

ArchiveIndividualsClass.prototype.bindEventsOnButtons = function () {
  let self = this

  self.initDocumentsForm()

  $('#department-select').selectize()
  $('#mode-select').selectize()
  $('#ex-select').selectize()
  $('#to-select').selectize()
  $('#group-ex-select').selectize()
  $('#group-to-select').selectize()
  $('#service-type-select').selectize()

  $('#close-job-modal').on('click', function () {
    self.refreshTable('done-jobs-table')
  })

  $('#save-notes-btn').on('click', async function () {
    let job = {
      job_notes: $('#notes').val()
    }
    await self.patchJob(self.currentJob.job_id, job)
    self.refreshTable('done-jobs-table')
    $('#job-modal').modal('hide')
  })

  $('#save-documents-btn').on('click', async function () {
    const dispatchNumber = $('#dispatch-number').val()
    if (self.currentJob.group != null) {
      await self.updateJobGroupDispatchNumber(self.currentJob.group.group_id, dispatchNumber)
    } else {
      let job = {
        job_dispatch_number: dispatchNumber
      }
      await self.patchJob(self.currentJob.job_id, job)
    }

    self.refreshTable('done-jobs-table')
    $('#job-docs-modal').modal('hide')
  })

  $('#upload-documents-btn').on('click', function () {
    $('.dz-message').click()
  })

  $('#assign-btn').on('click', () => {
    self.renderAssignJobsModal()
  })

  $('#assign-jobs').on('click', async () => {
    $(this).attr('disabeld', 'disabled')
    let selGroup = $('input.con-group:checked').val()
    const selectedJob = self.doneJobs.find(el => el.job_id == self.selectedDoneJobs[0])
    if (typeof selGroup === 'undefined' || !selectedJob) {
      self.Helpers.toastr('warning', 'Please select a group first.')
      return
    }
    let conGroupId = selGroup
    if (selGroup == 0) {
      const groupData = {
        con_group_ex: selectedJob.to.city_id,
        con_group_vessel_id: selectedJob.vessel_ids[0]
      }
      let conGroup = await self.createConsolidationGroup(groupData)
      conGroupId = conGroup.con_group_id
    }
    if (conGroupId != 0 && conGroupId != null) {
      await self.assignJobsToConsolidationGroup(conGroupId, self.selectedDoneJobs)
      $('#assignment-modal').modal('hide')
      window.location.replace('consolidations')
    }
    $(this).attr('disabeld', null)
  })
}

ArchiveIndividualsClass.prototype.initDoneJobsTable = async function () {
  let self = this
  self.visitedGroups = []
  await self.initDoneJobs()
  self.doneJobsTable = $('#done-jobs-table').DataTable({
    data: self.doneJobs,
    bLengthChange: false,
    paging: false,
    ordering: false,
    info: false,
    fixedHeader: {
      headerOffset: 100,
      header: true,
      footer: false
    },
    columns: [
      {
        title: 'JOB ID',
        orderable: false,
        data: 'job_id',
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.job_display_id != null) $(td).html(rowData.job_display_id)
        }
      },
      {
        title: 'GROUP ID',
        orderable: false,
        data: null,
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.group == null) {
            $(td).html('')
          } else {
            $(td).html(rowData.group.group_id)
            $(td).css('background-color', rowData.group.group_color).css('font-weight', 'bold')
          }
        }
      },
      {
        title: 'TYPE',
        orderable: false,
        data: 'job_type',
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.job_type == 'Individual') $(td).css('color', self.Helpers.INDIVIDUAL_COLOR).css('font-weight', 'bold')
          if (rowData.job_type == 'Grouped') $(td).css('color', self.Helpers.GROUPED_COLOR).css('font-weight', 'bold')
          if (rowData.job_type == 'Personnel') $(td).css('color', self.Helpers.PERSONNEL_COLOR).css('font-weight', 'bold')
          if (rowData.job_type == 'Local Order') $(td).css('color', self.Helpers.LOCAL_ORDER_COLOR).css('font-weight', 'bold')
        }
      },
      { title: 'REQ. DATE', orderable: false, data: 'request_date' },
      { title: 'USER', orderable: false, data: 'user.user_username' },
      { title: 'DEPT.', orderable: false, data: 'department.department_description' },
      {
        title: 'PIECES',
        orderable: false,
        data: 'pieces_count',
        createdCell: function (td, cellData, rowData, row, col) {
          let text = 'Pieces'
          if (rowData.pieces_count == 1) text = 'Piece'
          $(td).html(`<p class='piece-info' style='cursor: pointer'>${rowData.pieces_count} ${text}</p>`)
        }
      },
      {
        title: 'WEIGHT (KG)',
        orderable: false,
        data: 'job_weight'
      },
      {
        title: 'MODE',
        orderable: false,
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.mode.mode_id == 5) $(td).css('color', 'blue').css('font-weight', 'bold')
        },
        data: 'mode.mode_description'
      },
      {
        title: 'SERVICE',
        orderable: false,
        data: 'service_type.service_type_description'
      },
      {
        title: 'VESSELS',
        orderable: false,
        data: 'vessels'
      },
      { title: 'EX', orderable: false, className: 'danger-header', data: 'ex.city_name' },
      { title: 'TO', orderable: false, className: 'danger-header', data: 'to.city_name' },
      { title: 'DEADLINE', orderable: false, className: 'danger-header', data: 'job_deadline' },
      { title: 'CONFIRMATION DATE', orderable: false, data: 'confirmation_date' },
      {
        title: 'FORWARDER',
        orderable: false,
        data: 'job_forwarder_id',
        createdCell: function (td, cellData, rowData, row, col) {
          let forwarderId = rowData.job_forwarder_id
          if (rowData.group != null) {
            forwarderId = rowData.group.group_forwarder_id
          }
          let foundFowarder = self.forwarders.find(el => el.forwarder_id == forwarderId)
          if (!foundFowarder) return
          $(td).html(foundFowarder.forwarder_name)
        }
      },
      {
        title: 'PRICE ($)',
        orderable: false,
        data: 'costs.job_cost',
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.costs.job_cost == null) {
            $(td).html('')
            return
          }
        }
      },
      {
        title: 'SHARED COST ($)',
        orderable: false,
        data: 'costs.job_shared_cost',
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.group == null) {
            $(td).html('')
            return
          }
          if (rowData.job_parent_id != null) $(td).html(`<p style="font-weight: bold; color: black">-</p>`)
        }
      },
      {
        title: 'DISPATCH DOCS',
        orderable: false,
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.documents_count > 0) {
            $(td).css('color', 'green')
          } else {
            $(td).css('color', 'red')
          }
          if (rowData.job_dissolved == 1) $(td).children('.dispatch-docs').hide()
        },
        defaultContent: `<i class='fa fa-file-pdf-o dispatch-docs action-btn' style='cursor: pointer' title='dispatch-docs'></i>`
      },
      {
        title: 'ACTIONS',
        orderable: false,
        createdCell: function (td, cellData, rowData, row, col) {
          $(td).children('.select-done-jobs').data('city-id', rowData.to.city_id)
          $(td).children('.select-done-jobs').data('vessel-id', rowData.vessel_ids[0])
          $(td).children('.select-done-jobs').data('dissolved', rowData.job_dissolved)
          $(td).children('.select-done-jobs').data('vessel-count', rowData.vessel_ids.length)
          $(td).children('.select-done-jobs').data('documents-count', rowData.documents_count)
          if (rowData.vessels_count == 1) $(td).children('.split-job').hide()
          if (rowData.job_dissolved == 1) {
            $(td).children('.split-job').hide()
            $(td).children('.select-done-jobs').hide()
          }
          if (rowData.job_type == 'Personnel') {
            $(td).children('.split-job').hide()
            $(td).children('.select-done-jobs').hide()
          }

          if (rowData.documents_count == 0) {
            $(td).children('.select-done-jobs').hide()
          }

          if (rowData.vessel_ids.length != 1) $(td).children('.select-done-jobs').hide()
        },
        defaultContent: `<i class='fa fa-pencil job-edit action-btn' title='modify' id='delete-me' style='cursor: pointer'></i> \ 
          <i class='fa fa-sitemap split-job action-btn' style='cursor: pointer' title='dissolve'></i> \
          <i class='select-done-jobs' style='cursor: pointer' title='select'><img src='../assets/icons/consolidations.png'/ style='width: 15px'></i>`
      }
    ],
    createdRow: function (row, rowData, index, cells) {
      if (rowData.job_parent_id != null || rowData.job_dissolved != 0) {
        $('td:eq(0)', row).css('border-left', `4px solid ${rowData.job_dissolve_color}`)
        $('td:eq(0)', row).css('padding-left', `10px`)
      }
      if (rowData.job_parent_id != null) {
        $('td', row).css('font-style', 'italic')
        $('td', row).css('display', 'row')
        $('td', row).css('padding-left', '10px')
      }

      // Merge cells for group dispatch docs
      if (rowData.group != null) {
        if (self.visitedGroups.indexOf(rowData.group.group_id) != -1) {
          $('td:eq(17)', row).attr('colspan', 2)
          $('td:eq(18)', row).css('display', 'none')
        } else {
          self.visitedGroups.push(rowData.group.group_id)
        }
      }
    }
  })

  self.doneJobsTable.on('click', 'i.job-edit', function () {
    const rowData = self.doneJobsTable.row($(this).closest('tr')).data()
    if (!self.Helpers.checkIfUserHasPriviledges(rowData.user.user_username)) {
      self.Helpers.swalUserPermissionError()
      return
    }
    self.renderJobModal(rowData.job_id)
  })

  self.doneJobsTable.on('click', 'i.dispatch-docs', function () {
    const rowData = self.doneJobsTable.row($(this).closest('tr')).data()
    if (!self.Helpers.checkIfUserHasPriviledges(rowData.user.user_username)) {
      self.Helpers.swalUserPermissionError()
      return
    }
    self.currentJob = rowData
    self.renderJobModal(rowData.job_id, 6)
  })

  self.doneJobsTable.on('click', 'p.piece-info', function () {
    const rowData = self.doneJobsTable.row($(this).closest('tr')).data()
    self.Helpers.swalPieceInfo(rowData.job_id)
  })

  self.doneJobsTable.on('click', 'i.split-job', function () {
    const rowData = self.doneJobsTable.row($(this).closest('tr')).data()
    if (!self.Helpers.checkIfUserHasPriviledges(rowData.user.user_username)) {
      self.Helpers.swalUserPermissionError()
      return
    }
    Swal.fire({
      title: 'Dissolve Job?',
      text: `Are you sure you want to dissolve job ${rowData.job_id} per vessel?`,
      icon: 'warning',
      showCancelButton: true,
      showLoaderOnConfirm: true,
      cancelButtonText: 'Cancel',
      confirmButtonColor: '#dc3545',
      confirmButtonText: 'Confirm',
      preConfirm: async () => {
        const result = await self.dissolveJob(rowData.job_id)
        if (!result) throw 'Error'
      }
    }).then(async function (result) {
      if (result.isConfirmed) {
        Swal.fire('Dissolved!', `Job ${rowData.job_id} is dissolved!`, 'success').then(result => {
          if (result.isConfirmed) self.refreshTable('done-jobs-table')
        })
      }
    })
  })

  self.doneJobsTable.on('click', 'i.select-done-jobs', function () {
    const rowData = self.doneJobsTable.row($(this).parents('tr')).data()
    if (!self.Helpers.checkIfUserHasPriviledges(rowData.user.user_username)) {
      self.Helpers.swalUserPermissionError()
      return
    }
    if (self.selectedDoneJobs.length == 0) {
      self.selectedDoneJobsDestination = rowData.to.city_id
      self.selectedDoneJobsVessel = rowData.vessel_ids[0]

      $('i.select-done-jobs').each(function () {
        if ($(this).data('city-id') != self.selectedDoneJobsDestination) $(this).hide()
        if ($(this).data('vessel-id') != self.selectedDoneJobsVessel) $(this).hide()
      })
    }

    if (self.selectedDoneJobs.indexOf(rowData.job_id) == -1) {
      $(this).parents('tr').addClass('datatableBack')
      self.selectedDoneJobs.push(rowData.job_id)
    } else {
      $(this).parents('tr').removeClass('datatableBack')
      self.selectedDoneJobs.splice(self.selectedDoneJobs.indexOf(rowData.job_id), 1)
    }
    if (self.selectedDoneJobs.length == 0) {
      $('i.select-done-jobs').each(function () {
        if ($(this).data('dissolved') != 1 && $(this).data('vessel-count') == 1 && $(this).data('documents-count') != 0) $(this).show()
      })
      $('#assign-btn').attr('disabled', 'disabled')
    } else {
      $('#assign-btn').attr('disabled', null)
    }
  })

  self.Helpers.applyMouseInteractions('done-jobs-table')
  self.Helpers.applySearchInteraction(self.doneJobsTable)
}

ArchiveIndividualsClass.prototype.initGroupPricesTable = async function () {
  let self = this
  self.currentJobGroupPrices = []
  if (self.currentJob != null) self.currentJobGroupPrices = await self.Helpers.getJobGroupPrices(self.currentJob.group.group_id)
  self.groupPricesTable = $('#group-prices-table').DataTable({
    data: self.currentJobGroupPrices,
    bLengthChange: false,
    paging: false,
    ordering: false,
    info: false,
    columns: [
      { title: 'ID', orderable: false, data: 'price_id' },
      { title: 'Forwarder', orderable: false, data: 'forwarder.forwarder_name' },
      { title: 'Price', orderable: false, data: 'price_value' },
      { title: 'Currency', orderable: false, data: 'price_currency_name' },
      { title: 'Price ($)', orderable: false, data: 'price_base_value' },
      {
        title: 'Offer Requested',
        orderable: false,
        data: 'price_offer_requested',
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.price_offer_requested == true) {
            $(td).css('color', 'green').css('text-align', 'center')
            $(td).html(`<i class="fa fa-check action-btn"></i>`)
          } else {
            $(td).css('color', 'red').css('text-align', 'center')
            $(td).html(`<i class="fa fa-times action-btn"></i>`)
          }
        }
      }
    ],
    rowCallback: function (row, data, index, cells) {
      if (data.price_selected) $('td', row).css('background-color', '#8fbc8f')
    }
  })
}

ArchiveIndividualsClass.prototype.initDoneJobs = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/jobs/doneindividuals', { headers: self.Helpers.getHeaders() })
    let tempJobs = response.data
    let groupedJobs = []
    let localOrderJobs = []
    let personnelJobs = []
    let individualJobs = []
    let childJobs = []
    self.jobs = []
    for (let i = 0; i < tempJobs.length; i++) {
      tempJobs[i].request_date = self.Helpers.changeMysqlDateToNormal(tempJobs[i].job_created_at)
      tempJobs[i].confirmation_date = self.Helpers.changeMysqlDateToNormal(tempJobs[i].job_confirmed_at)

      let jobTempVessels = []
      for (let vesselId of tempJobs[i].vessel_ids) {
        let vessel = self.vessels.find(el => el.vessel_id == parseInt(vesselId))
        if (jobTempVessels.indexOf(vessel.vessel_description) == -1) jobTempVessels.push(vessel.vessel_description)
      }
      tempJobs[i].vessels = jobTempVessels.join(',')

      if (tempJobs[i].job_parent_id != null) {
        childJobs.push(tempJobs[i])
        continue
      }
      switch (tempJobs[i].job_type) {
        case 'Individual':
          individualJobs.push(tempJobs[i])
          break
        case 'Grouped':
          groupedJobs.push(tempJobs[i])
          break
        case 'Local Order':
          localOrderJobs.push(tempJobs[i])
          break
        case 'Personnel':
          personnelJobs.push(tempJobs[i])
          break
      }
    }

    let sortedGroupedJobs = _.orderBy(groupedJobs, ['group.group_id', 'job_id'], ['desc'])
    let sortedLocalJobs = _.orderBy(localOrderJobs, ['job_id'], ['desc'])
    let sortedIndJobs = _.orderBy(individualJobs, ['job_id'], ['desc'])
    let sortedPersonnelJobs = _.orderBy(personnelJobs, ['job_id'], ['desc'])
    let sortedChildJobs = _.orderBy(childJobs, ['job_id'], ['desc'])
    self.doneJobs = _.concat(sortedGroupedJobs, sortedLocalJobs, sortedIndJobs, sortedPersonnelJobs)
    for (let childJob of sortedChildJobs) {
      let parentIndex = self.doneJobs.findIndex(el => el.job_id == childJob.job_parent_id)
      self.doneJobs.splice(parentIndex + 1, 0, childJob)
    }
    console.log(`Done Jobs initialized`)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.doneJobs = []
  }
}

ArchiveIndividualsClass.prototype.refreshTable = function (tableId) {
  let self = this
  $(`#${tableId}`).unbind('click')
  $(`#${tableId}`).DataTable().clear()
  $(`#${tableId}`).DataTable().destroy()
  if (tableId == 'pieces-table') self.initPiecesTable()
  if (tableId == 'prices-table') self.initPricesTable()
  if (tableId == 'documents-table') self.initDocumentsTable()
  if (tableId == 'group-prices-table') self.initGroupPricesTable()
  if (tableId == 'done-jobs-table') self.initDoneJobsTable()
}

ArchiveIndividualsClass.prototype.renderJobModal = async function (jobId, focusTabIndex = 0) {
  let self = this
  $('#modal-title-text').html(`Job ID: ${jobId}`)
  self.currentJob = await self.Helpers.getJobInfo(jobId)
  self.handleModalTabs(focusTabIndex)
  self.renderTransportationTab()
  self.renderPiecesTab()
  self.renderPricesTab()
  self.renderGroupTab()
  self.renderGroupPricesTab()
  self.renderCostsTab()
  self.renderDocumentsTab()
  if (!$('#job-modal').hasClass('show')) $('#job-modal').modal('show')
}

ArchiveIndividualsClass.prototype.renderTransportationTab = function () {
  let self = this
  $('#department-select')[0].selectize.setValue(self.currentJob.department.department_id)
  $('#ex-select')[0].selectize.setValue(self.currentJob.ex.city_id)
  $('#to-select')[0].selectize.setValue(self.currentJob.to.city_id)
  $('#mode-select')[0].selectize.setValue(self.currentJob.mode.mode_id)
  $('#deadline_date').val(self.currentJob.job_deadline)
  $('#service-type-select')[0].selectize.setValue(self.currentJob.service_type.service_type_id)
}

ArchiveIndividualsClass.prototype.renderPiecesTab = async function () {
  let self = this
  self.refreshTable('pieces-table')
}

ArchiveIndividualsClass.prototype.renderPricesTab = async function () {
  let self = this
  self.refreshTable('prices-table')
  $('#job-notes').val(self.currentJob.job_notes)
}

ArchiveIndividualsClass.prototype.renderGroupTab = function (hideInputs = true) {
  let self = this
  if (self.currentJob.group == null) return
  $('#group-ex-select')[0].selectize.setValue(self.currentJob.group.group_ex.city_id)
  $('#group-to-select')[0].selectize.setValue(self.currentJob.group.group_to.city_id)
  $('#group-deadline').val(self.currentJob.group.group_deadline)
}

ArchiveIndividualsClass.prototype.renderGroupPricesTab = function (hideInputs) {
  let self = this
  if (self.currentJob.group == null) return
  self.refreshTable('group-prices-table')
  $('#group-notes').val(self.currentJob.group.group_notes)
}

ArchiveIndividualsClass.prototype.renderCostsTab = function () {
  let self = this
  if (self.currentJob == null || self.currentJob.costs == null) return
  $('.group-costs-data').each(function () {
    $(this).hide()
  })
  $('#costs-job-weight').val(self.currentJob.costs.job_weight)
  $('#costs-job-cost').val(self.currentJob.costs.job_cost)
  $('#costs-job-cost-per-kg').val(self.currentJob.costs.job_cost_per_kg)
  $('#costs-shared-cost').val(self.currentJob.costs.job_shared_cost)
  if (self.currentJob.group != null && self.currentJob.group.costs != null) {
    $('.group-costs-data').each(function () {
      $(this).show()
    })
    $('#costs-group-cost').val(self.currentJob.group.costs.group_cost)
    $('#costs-group-cost-per-kg').val(self.currentJob.group.costs.group_cost_per_kg)
    $('#costs-group-weight').val(self.currentJob.group.costs.group_weight)
  }
}

ArchiveIndividualsClass.prototype.renderDocumentsTab = function () {
  let self = this
  $('#dispatch-number').val(self.currentJob.job_dispatch_number)
  self.refreshTable('documents-table')
}

ArchiveIndividualsClass.prototype.initPiecesTable = async function () {
  let self = this
  self.currentJobPieces = []
  if (self.currentJob != null) self.currentJobPieces = await self.getJobPieces(self.currentJob.job_id)
  self.piecesTable = $('#pieces-table').DataTable({
    data: self.currentJobPieces,
    bLengthChange: false,
    paging: false,
    info: false,
    columns: [
      { title: 'ID', orderable: false, data: 'piece_id', visible: false },
      { title: 'Vessel', orderable: false, data: 'vessel.vessel_description', visible: false },
      { title: 'Dimensions', orderable: false, data: 'piece_dimensions' },
      { title: 'Weight (kg)', orderable: false, data: 'piece_weight' },
      { title: 'Reference', orderable: false, data: 'piece_reference' },
      { title: 'Contents', orderable: false, data: 'piece_description' },
      {
        title: 'Products',
        orderable: false,
        data: 'piece_products',
        createdCell: function (td, cellData, rowData, row, col) {
          let productDescriptions = []
          for (let product of rowData.piece_products) productDescriptions.push(product.product_description)
          $(td).html(productDescriptions.join(','))
        }
      },
      { title: 'Value ($)', orderable: false, data: 'piece_value_of_goods' },
      {
        title: 'Actions',
        orderable: false,
        createdCell: function (td, cellData, rowData, row, col) {
          let selectedPrice = self.currentJobPrices.find(el => el.price_selected == 1)
          if (self.currentJob.costs != null || selectedPrice) $(td).children('.delete-piece').hide()
        },
        defaultContent: "<i class='fa fa-qrcode qrcode-piece action-btn' title='qr-code' style='cursor: pointer' ></i>"
      }
    ],
    rowGroup: {
      dataSrc: 'vessel.vessel_description',
      startRender: function (rows, group) {
        let ordersString = 'Order'
        if (rows.count() > 1) ordersString = 'Orders'

        let totalValue = 0
        let totalWeight = 0
        for (let i = 0; i < rows.count(); i++) {
          totalWeight += rows.data()[i].piece_weight
          if (rows.data()[i].piece_value_of_goods != null) totalValue += rows.data()[i].piece_value_of_goods
        }
        return `${group} (${rows.count()} ${ordersString}, ${totalWeight} KG , ${totalValue} $)`
      }
    },
    order: [
      [1, 'asc'],
      [0, 'asc']
    ]
  })

  self.piecesTable.on('click', 'i.qrcode-piece', function () {
    const rowData = self.piecesTable.row($(this).closest('tr')).data()

    var qrcode = new QRCode('qrcode-holder', {
      text: rowData.piece_description,
      width: 128,
      height: 128,
      colorDark: '#000000',
      colorLight: '#ffffff',
      correctLevel: QRCode.CorrectLevel.H
    })

    setTimeout(() => {
      var pdf = new jsPDF({
        orientation: 'landscape',
        unit: 'mm',
        format: [50, 50]
      })

      let base64Image = $('#qrcode-holder img').attr('src')

      pdf.addImage(base64Image, 'png', 5, 5, 40, 40)
      pdf.save('forward-tool-qrcode.pdf')
      qrcode.clear()
      $('#qrcode-holder').html('')
    }, 1000)
  })
}

ArchiveIndividualsClass.prototype.initPricesTable = async function () {
  let self = this
  self.currentJobPrices = []
  if (self.currentJob != null) self.currentJobPrices = await self.getJobPrices(self.currentJob.job_id)
  self.pricesTable = $('#prices-table').DataTable({
    data: self.currentJobPrices,
    bLengthChange: false,
    paging: false,
    ordering: false,
    info: false,
    columns: [
      { title: 'ID', orderable: false, data: 'price_id' },
      { title: 'Forwarder', orderable: false, data: 'forwarder.forwarder_name' },
      { title: 'Price', orderable: false, data: 'price_value' },
      { title: 'Currency', orderable: false, data: 'price_currency_name' },
      { title: 'Price ($)', orderable: false, data: 'price_base_value' },
      {
        title: 'Offer Requested',
        orderable: false,
        data: 'price_offer_requested',
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.price_offer_requested == true) {
            $(td).css('color', 'green').css('text-align', 'center')
            $(td).html(`<i class="fa fa-check action-btn"></i>`)
          } else {
            $(td).css('color', 'red').css('text-align', 'center')
            $(td).html(`<i class="fa fa-times action-btn"></i>`)
          }
        }
      }
    ],
    rowCallback: function (row, data, index, cells) {
      if (data.price_selected) $('td', row).css('background-color', '#8fbc8f')
    }
  })
}

ArchiveIndividualsClass.prototype.initDocumentsTable = async function () {
  let self = this
  self.currnetJobDocuments = []
  if (self.currentJob != null) self.currnetJobDocuments = await self.getJobDocuments(self.currentJob.job_id, self.currentJob.group)
  self.documentsTable = $('#documents-table').DataTable({
    data: self.currnetJobDocuments,
    bLengthChange: false,
    paging: false,
    ordering: false,
    info: false,
    columns: [
      { title: 'ID', orderable: false, data: 'document_id' },
      { title: 'File Name', orderable: false, data: 'document_original_name' },
      { title: 'Upload Date', orderable: false, data: 'document_created_at' },
      {
        title: 'Actions',
        orderable: false,
        defaultContent: "<i class='fa fa-download download-file action-btn' title='download' style='cursor: pointer'></i><i class='fa fa-trash delete-document action-btn' title='delete' style='cursor: pointer' ></i>"
      }
    ]
  })

  self.documentsTable.on('click', 'i.delete-document', function () {
    const rowData = self.documentsTable.row($(this).closest('tr')).data()

    Swal.fire({
      title: ' Delete Document?',
      text: `Are you sure you want to delete ${rowData.document_original_name}?`,
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonColor: '#dc3545',
      confirmButtonText: 'Confirm'
    }).then(result => {
      if (result.isConfirmed) self.deleteDocument(rowData.document_id)
    })
  })

  self.documentsTable.on('click', 'i.download-file', function () {
    const rowData = self.documentsTable.row($(this).closest('tr')).data()
    self.downloadDocument(rowData.document_id, rowData.document_original_name)
  })
}

ArchiveIndividualsClass.prototype.getJobPieces = async function (jobId) {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + `/pieces/job/${jobId}`, { headers: self.Helpers.getHeaders() })
    return response.data
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    return []
  }
}

ArchiveIndividualsClass.prototype.getJobPrices = async function (jobId) {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + `/prices/job/${jobId}`, { headers: self.Helpers.getHeaders() })
    return response.data
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    return []
  }
}

ArchiveIndividualsClass.prototype.initCities = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/cities', { headers: self.Helpers.getHeaders() })
    self.cities = response.data
    for (i = 0; i < self.cities.length; i++) {
      $('#ex-select')[0].selectize.addOption({ text: `${self.cities[i].city_name} - ${self.cities[i].country_name}`, value: self.cities[i].city_id })
      $('#to-select')[0].selectize.addOption({ text: `${self.cities[i].city_name} - ${self.cities[i].country_name}`, value: self.cities[i].city_id })
      $('#group-ex-select')[0].selectize.addOption({ text: `${self.cities[i].city_name} - ${self.cities[i].country_name}`, value: self.cities[i].city_id })
      $('#group-to-select')[0].selectize.addOption({ text: `${self.cities[i].city_name} - ${self.cities[i].country_name}`, value: self.cities[i].city_id })
    }
    console.log(`Cities initialized`)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.cities = []
  }
}

ArchiveIndividualsClass.prototype.initModes = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/modes', { headers: self.Helpers.getHeaders() })
    self.modes = response.data
    for (i = 0; i < self.modes.length; i++) {
      $('#mode-select')[0].selectize.addOption({ text: self.modes[i].mode_description, value: self.modes[i].mode_id })
    }
    console.log(`Modes initialized`)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.modes = []
  }
}

ArchiveIndividualsClass.prototype.initServiceTypes = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/servicetypes', { headers: self.Helpers.getHeaders() })
    for (let serviceType of response.data) {
      if (serviceType.service_type_group !== 'Individuals') continue
      $('#service-type-select')[0].selectize.addOption({ text: serviceType.service_type_description, value: serviceType.service_type_id })
    }

    console.log(`Service Types initialized`)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.vessels = []
  }
}

ArchiveIndividualsClass.prototype.initDepartments = async function () {
  let self = this
  try {
    const response = await axios.get(`${self.Helpers.API_URL}/departments`, { headers: self.Helpers.getHeaders() })
    self.departments = response.data
    for (i = 0; i < self.departments.length; i++) {
      $('#department-select')[0].selectize.addOption({ text: self.departments[i].department_description, value: self.departments[i].department_id })
    }
    console.log(`Departments initialized`)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.departments = []
  }
}

ArchiveIndividualsClass.prototype.handleModalTabs = function (focusIndex) {
  let self = this
  self.Wizzard.focusTab(focusIndex)

  if (self.currentJob.group != null) {
    self.Wizzard.showTab('group')
    self.Wizzard.showTab('group-forwarders')
  } else {
    self.Wizzard.hideTab('group')
    self.Wizzard.hideTab('group-forwarders')
  }
}

ArchiveIndividualsClass.prototype.dissolveJob = async function (jobId) {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + `/jobs/dissolve/${jobId}`, { headers: self.Helpers.getHeaders() })
    return true
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    return false
  }
}

ArchiveIndividualsClass.prototype.patchJob = async function (jobId, job) {
  let self = this
  try {
    await axios.patch(self.Helpers.API_URL + `/jobs/${jobId}`, job, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Job Updated!')
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

ArchiveIndividualsClass.prototype.patchDocument = async function (documentId, document) {
  let self = this
  try {
    await axios.patch(self.Helpers.API_URL + `/documents/${documentId}`, document, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Document Updated!')
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

ArchiveIndividualsClass.prototype.updateJobGroupDispatchNumber = async function (groupId, dispatchNumber) {
  let self = this
  const payload = {
    job_dispatch_number: dispatchNumber
  }
  try {
    await axios.put(self.Helpers.API_URL + `/jobgroups/updateDispatchNumber/${groupId}`, payload, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Group Updated!')
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

ArchiveIndividualsClass.prototype.getAvailableJobGroups = async function (payload) {
  let self = this
  try {
    const response = await axios.post(self.Helpers.API_URL + `/consolidationGroups/available`, payload, { headers: self.Helpers.getHeaders() })
    return response.data
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

ArchiveIndividualsClass.prototype.getJobDocuments = async function (jobId, group) {
  let self = this
  try {
    let documentsURL = `/documents/job/${jobId}/1`
    if (group != null) documentsURL = `/documents/group/${group.group_id}/1`
    const response = await axios.get(self.Helpers.API_URL + documentsURL, { headers: self.Helpers.getHeaders() })
    return response.data
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

ArchiveIndividualsClass.prototype.renderAssignJobsModal = async function () {
  let self = this
  const payload = {
    city_id: self.selectedDoneJobsDestination,
    vessel_id: self.selectedDoneJobsVessel
  }
  const availableConsolidationGroups = await self.getAvailableJobGroups(payload)
  $('#con-group-radios').html('')
  $('#con-group-radios').append(`<label class="custom-control custom-radio dark">
        <input name="radio-stacked" class="custom-control-input con-group" type="radio" value="0" />
        <span class="custom-control-indicator"></span>
        <span class="custom-control-description">NEW CONSOLIDATION</span>
    </label>`)
  for (let conGroup of availableConsolidationGroups) {
    let conId = conGroup.con_group_parent_id == null ? conGroup.con_group_id : conGroup.con_group_parent_id
    $('#con-group-radios').append(`<label class="custom-control custom-radio dark">
                  <input name="radio-stacked" class="custom-control-input con-group" type="radio" value="${conGroup.con_group_id}" />
                  <span class="custom-control-indicator"></span>
                  <span class="custom-control-description" style="background-color: ${conGroup.con_group_color}">CONSOLIDATION ID: ${conId}, VESSEL: ${conGroup.vessel_description}</span>
                  </label>`)
  }
  $('#assignment-modal').modal('show')
}

ArchiveIndividualsClass.prototype.initVessels = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/vessels', { headers: self.Helpers.getHeaders() })
    self.vessels = response.data
    console.log(`Vessels initialized`)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.vessels = []
  }
}

ArchiveIndividualsClass.prototype.initForwarders = async function () {
  let self = this
  try {
    const response = await axios.get(self.Helpers.API_URL + '/forwarders', { headers: self.Helpers.getHeaders() })
    self.forwarders = response.data
    console.log(`Forwarders initialized`)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.forwarders = []
  }
}

ArchiveIndividualsClass.prototype.downloadDocument = async function (documentId, documentName) {
  let self = this
  try {
    await self.Helpers.downloadFile(self.Helpers.API_URL + `/documents/download/${documentId}`, documentName)
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    self.vessels = []
  }
}

ArchiveIndividualsClass.prototype.createConsolidationGroup = async function (groupData) {
  let self = this
  try {
    const response = await axios.post(self.Helpers.API_URL + '/consolidationGroups', groupData, { headers: self.Helpers.getHeaders() })
    return response.data
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    return null
  }
}

ArchiveIndividualsClass.prototype.assignJobsToConsolidationGroup = async function (conGroupId, jobIds) {
  let self = this
  try {
    await axios.post(self.Helpers.API_URL + '/consolidationGroups/assignJobs/' + conGroupId, jobIds, { headers: self.Helpers.getHeaders() })
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
    return null
  }
}

ArchiveIndividualsClass.prototype.initDocumentsForm = function () {
  let self = this

  self.dispatchFileZone.on('success', async function (file, response) {
    let payload = {
      document_job_id: self.currentJob.job_id,
      document_group_id: self.currentJob.group != null ? self.currentJob.group.group_id : null,
      document_job_type: 1
    }
    await self.patchDocument(response.document_id, payload)
    self.refreshTable('documents-table')
  })
}

ArchiveIndividualsClass.prototype.deleteDocument = async function (documentId) {
  let self = this
  try {
    await axios.delete(self.Helpers.API_URL + `/documents/${documentId}`, { headers: self.Helpers.getHeaders() })
    self.Helpers.toastr('success', 'Document Deleted!')
    self.refreshTable('documents-table')
  } catch (err) {
    console.error(err)
    self.Helpers.handleRequestError(err)
  }
}

new ArchiveIndividualsClass()
