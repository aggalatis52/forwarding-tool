let ExpandIdClass = function (Helpers) {
  this.Helpers = Helpers
  this.tableInitialized = false
}

ExpandIdClass.prototype.openModal = function () {
  let self = this
  $('#expand-table-div').hide()
  $('#expand-job-type')[0].selectize.setValue('')
  $('#expand-group-id').val('')
  $('#expand-modal').modal('show')
}

ExpandIdClass.prototype.bindEventsOnBtn = function () {
  let self = this

  $('#expand-job-type').selectize()
  $('#expand-job-type')[0].selectize.addOption({ text: 'Group', value: 1 })
  $('#expand-job-type')[0].selectize.addOption({ text: 'Consolidation', value: 2 })

  $('#expand-id-btn').on('click', function () {
    if (self.tableInitialized) {
      $('#expand-id-table').DataTable().clear()
      $('#expand-id-table').DataTable().destroy()
    }
    $('#expand-table-div').show()
    const groupId = $('#expand-group-id').val()
    const groupType = $('#expand-job-type').val()

    if (!groupId || !groupType) {
      self.Helpers.toastr('error', 'Group Id or Group Type is empty!')
      return
    }

    if (groupType == 1) {
      self.loadGroupData(groupId)
    } else {
      self.loadConsolidationData(groupId)
    }
  })
}

ExpandIdClass.prototype.loadGroupData = async function (groupId) {
  let self = this
  const response = await self.getGroupInfo(groupId)
  if (response == null) {
    self.Helpers.toastr('error', 'Unable to find info for this group')
    return
  }
  const myTable = $('#expand-id-table').DataTable({
    data: response.jobs,
    bLengthChange: false,
    paging: false,
    ordering: false,
    info: false,

    columns: [
      { title: 'Group ID', orderable: false, data: 'group.group_id' },
      {
        title: 'Confirmation Date',
        orderable: false,
        data: 'job_confirmed_at',
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.job_confirmed_at != null) $(td).html(self.Helpers.changeMysqlDateToNormal(rowData.job_confirmed_at))
        }
      },
      { title: 'Vessels', orderable: false, data: 'vessels' },
      { title: 'EX', orderable: false, data: 'ex.city_name' },
      { title: 'TO', orderable: false, data: 'to.city_name' },
      { title: 'Service', orderable: false, data: 'service_type.service_type_description' },
      { title: 'Job ID', orderable: false, data: 'job_id' },
      { title: 'Department', orderable: false, data: 'department.department_description' },
      { title: 'Pieces', orderable: false, data: 'pieces_count' },
      { title: 'Weight', orderable: false, data: 'job_weight' },
      {
        title: 'Individual Cost',
        orderable: false,
        data: 'costs.job_cost',
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.job_id == null) $(td).css('font-weight', 'bolder').css('font-size', '14px')
          $(td).css('text-align', 'center')
          if (rowData.costs.job_cost == 0) $(td).html(``)
        }
      },
      { title: 'Group Cost', orderable: false, data: 'group.costs.group_cost' },
      // {
      //   title: 'Saving %',
      //   orderable: false,
      //   data: null,
      //   createdCell: function (td, cellData, rowData, row, col) {
      //     if (rowData.job_id == null && rowData.costs.job_cost != 0) {
      //       const savingPercent = (rowData.costs.job_savings * 100) / rowData.costs.job_cost
      //       $(td).html(`${savingPercent.toFixed(2)} %`)
      //       $(td).css('font-weight', 'bolder').css('font-size', '14px')
      //       $(td).css('text-align', 'center')
      //     } else {
      //       $(td).html(``)
      //     }
      //   }
      // },
      // {
      //   title: 'Saving',
      //   orderable: false,
      //   data: 'costs.job_savings',
      //   createdCell: function (td, cellData, rowData, row, col) {
      //     if (rowData.job_id == null) $(td).css('font-weight', 'bolder').css('font-size', '14px')
      //     $(td).css('text-align', 'center')
      //     if (rowData.costs.job_savings == 0) $(td).html(``)
      //   }
      // },
      {
        title: 'Shared Cost',
        orderable: false,
        data: 'costs.job_shared_cost',
        createdCell: function (td, cellData, rowData, row, col) {
          if (rowData.job_id == null) $(td).css('font-weight', 'bolder').css('font-size', '14px')
          $(td).css('text-align', 'center')
        }
      }
    ]
  })
  const fakeRow = {
    job_id: null,
    job_weight: null,

    pieces_count: null,
    group: { group_id: null, costs: { group_cost: null } },
    department: { department_description: null },
    job_confirmed_at: null,
    vessels: null,
    ex: { city_name: null },
    to: { city_name: null },
    service_type: { service_type_description: null },
    costs: { job_cost: response.total.individualCost, job_shared_cost: response.total.groupCost }
  }
  myTable.row.add(fakeRow).draw(false)
  myTable.draw()
  self.tableInitialized = true
}

ExpandIdClass.prototype.getGroupInfo = async function (groupId) {
  let self = this
  try {
    const response = await axios.get(`${self.Helpers.API_URL}/jobgroups/info/${groupId}`, { headers: self.Helpers.getHeaders() })
    return response.data
  } catch (err) {
    console.error(err)
    if (err.response.status == 400) return null
    self.Helpers.handleRequestError(err)
  }
}

ExpandIdClass.prototype.initExpandTableForGroup = async function () {
  let self = this
}
