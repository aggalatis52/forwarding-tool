let WizzardClass = function (wizzardTabs) {
  console.info('Constructing Wizzard...')
  this.wizzardPages = wizzardTabs
  this.activeWizzardIndex = 0
  this.initWizzard()
}

WizzardClass.prototype.initWizzard = function () {
  let self = this

  $('.job-wizzard-tab').on('click', function () {
    const selectedTab = $(this).data('wizzard')
    const selectedWizzardIndex = self.wizzardPages.indexOf(selectedTab)
    if (self.activeWizzardIndex == selectedWizzardIndex) return
    self.activeWizzardIndex = selectedWizzardIndex
    self.updateWizzard()
  })
}

WizzardClass.prototype.updateWizzard = function () {
  let self = this
  const activeTab = self.wizzardPages[self.activeWizzardIndex]

  $('.job-wizzard-tab').each(function () {
    $(this).removeClass('active')
  })
  $(`#wizzard-${activeTab}-tab`).addClass('active')
  $('.wizzard-content').each(function () {
    $(this).hide()
  })
  $(`#${activeTab}-content`).show()
}

WizzardClass.prototype.focusTab = function (tabIndex) {
  let self = this
  if (tabIndex < 0) return
  self.activeWizzardIndex = tabIndex
  self.updateWizzard()
}

WizzardClass.prototype.hideTab = function (tab) {
  let self = this
  $(`#wizzard-${tab}-tab`).hide()
  self.updateWizzard()
}

WizzardClass.prototype.showTab = function (tab) {
  let self = this
  $(`#wizzard-${tab}-tab`).show()
  self.updateWizzard()
}
