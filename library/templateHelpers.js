let TemplateHelpersClass = function () {
  console.log('Contructing Template Helpers..')
  this.Helpers = new HelpersClass()
  this.RefTracker = new RefTrackerClass(this.Helpers)
  this.ExpandId = new ExpandIdClass(this.Helpers)
  this.MailPreview = new MailPreviewClass(this.Helpers)
  window.MailPreview = this.MailPreview
  this.initUniversalBtn()
  this.initModals()
}

TemplateHelpersClass.prototype.initUniversalBtn = function () {
  let self = this
  $('#universal-actions-list').load('../pages/templates/actionButtons.html', function () {
    $('#ref-tracker-btn').on('click', function () {
      self.RefTracker.openModal()
    })

    $('#expand-id-btn').on('click', function () {
      self.ExpandId.openModal()
    })
  })
}

TemplateHelpersClass.prototype.initModals = function () {
  let self = this
  $('#ref-tracker-modal').load('../pages/reftracker/reftracker.html', function () {
    self.Helpers.bindMovingEvents('ref-tracker-modal-mover', 'ref-tracker-modal', 'ref-tracker-modal-content')
    self.Helpers.bindCloseBtnsAlerts()
    self.RefTracker.bindEventsOnBtn()
  })

  $('#expand-modal').load('../pages/expandid/expandid.html', function () {
    self.Helpers.bindMovingEvents('expand-modal-mover', 'expand-modal', 'expand-modal-content')
    self.Helpers.bindCloseBtnsAlerts()
    self.ExpandId.bindEventsOnBtn()
  })

  $('#mail-preview-modal').load('../pages/mailpreview/mailpreview.html', function () {
    self.Helpers.bindMovingEvents('mail-preview-modal-mover', 'mail-preview-modal', 'mail-preview-modal-content')
    self.Helpers.bindCloseBtnsAlerts()
    self.MailPreview.bindEventsOnBtn()
  })
}

new TemplateHelpersClass()
