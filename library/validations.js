let ValidationsClass = function () {
  console.info('Constructing Validations...')
}

ValidationsClass.prototype.isUserValid = function (user) {
  const constrains = {
    user_username: {
      presence: true,
      length: {
        minimum: 3
      }
    },
    departments: {
      presence: true
    },
    user_role_id: {
      presence: true,
      numericality: {
        onlyInteger: true,
        greaterThan: 0
      }
    },
    user_username: {
      presence: true,
      length: {
        minimum: 3
      }
    },
    user_password: {
      presence: true,
      length: {
        minimum: 6,
        message: 'must be at least 6 characters'
      }
    },
    user_password_repeat: {
      equality: 'user_password'
    },
    user_email: {
      presence: true,
      email: true
    }
  }
  return validate(user, constrains, { format: 'flat' })
}

ValidationsClass.prototype.isProductValid = function (product) {
  const constrains = {
    product_description: {
      presence: true,
      length: {
        minimum: 3
      }
    },
    product_department_id: {
      presence: true,
      numericality: {
        onlyInteger: true,
        greaterThan: 0
      }
    }
  }
  return validate(product, constrains, { format: 'flat' })
}

ValidationsClass.prototype.isCityValid = function (city) {
  const constrains = {
    city_country_id: {
      presence: true,
      numericality: {
        onlyInteger: true,
        greaterThan: 0
      }
    },
    city_name: {
      presence: true,
      length: {
        minimum: 3
      }
    }
  }
  return validate(city, constrains, { format: 'flat' })
}

ValidationsClass.prototype.isPositionValid = function (position) {
  const constrains = {
    position_info: {
      presence: true
    },
    position_vessel_id: {
      presence: true,
      numericality: {
        onlyInteger: true,
        greaterThan: 0
      }
    },
    position_city_id: {
      presence: true,
      numericality: {
        onlyInteger: true,
        greaterThan: 0
      }
    },
    position_eta: {
      presence: true
    },
    position_etd: {
      presence: true
    }
  }
  return validate(position, constrains, { format: 'flat' })
}

ValidationsClass.prototype.isForwarderValid = function (forwarder) {
  const constrains = {
    forwarder_name: {
      presence: true,
      length: {
        minimum: 3
      }
    },
    forwarder_email: {
      presence: true,
      email: true
    }
  }
  return validate(forwarder, constrains, { format: 'flat' })
}

ValidationsClass.prototype.isServiceTypeValid = function (serviceType) {
  const constrains = {
    service_type_description: {
      presence: true,
      length: {
        minimum: 3
      }
    },
    service_type_group: {
      presence: true,
      length: {
        minimum: 3
      }
    }
  }
  return validate(serviceType, constrains, { format: 'flat' })
}

ValidationsClass.prototype.isPieceValid = function (piece) {
  const constrains = {
    piece_job_id: {
      presence: true,
      numericality: {
        onlyInteger: true,
        greaterThan: 0
      }
    },
    piece_vessel_id: {
      presence: true,
      numericality: {
        onlyInteger: true,
        greaterThan: 0
      }
    },
    piece_description: {
      presence: {
        allowEmpty: false
      }
    },
    piece_products: {
      presence: {
        allowEmpty: false
      }
    },
    piece_dimensions: {
      presence: {
        allowEmpty: false
      }
    },
    piece_reference: {
      presence: {
        allowEmpty: false
      }
    },
    piece_value_of_goods: {
      numericality: {
        onlyInteger: false,
        greaterThan: 0
      }
    },
    piece_weight: {
      presence: {
        allowEmpty: false
      },
      numericality: {
        onlyInteger: false,
        greaterThan: 0
      }
    }
  }
  return validate(piece, constrains, { format: 'flat' })
}

ValidationsClass.prototype.isPriceValid = function (price) {
  const constrains = {
    price_forwarder_id: {
      presence: true,
      numericality: {
        onlyInteger: true,
        greaterThan: 0
      }
    },
    price_currency_rate: {
      presence: true,
      numericality: {
        greaterThan: 0
      }
    },
    price_currency_name: {
      presence: true
    },
    price_selected: {
      type: 'boolean',
      presence: true
    },
    price_offer_requested: {
      type: 'boolean',
      presence: true
    }
  }
  return validate(price, constrains, { format: 'flat' })
}

ValidationsClass.prototype.isJobValid = function (job) {
  const jobStatuses = [1, 2, 3, 4, 5]
  const constrains = {
    job_user_id: {
      presence: true,
      numericality: {
        onlyInteger: true,
        greaterThan: 0
      }
    },
    job_department_id: {
      presence: true,
      numericality: {
        onlyInteger: true,
        greaterThan: 0
      }
    },
    job_ex_city_id: {
      presence: true,
      numericality: {
        onlyInteger: true,
        greaterThan: 0,
        message: 'cannot be empty.'
      }
    },
    job_to_city_id: {
      presence: true,
      numericality: {
        onlyInteger: true,
        greaterThan: 0,
        message: 'cannot be empty.'
      }
    },
    job_mode_id: {
      presence: true,
      numericality: {
        onlyInteger: true,
        greaterThan: 0,
        message: 'cannot be empty.'
      }
    },
    job_deadline: {
      presence: {
        allowEmpty: false,
        message: 'cannot be empty.'
      }
    },
    job_status: {
      presence: true,
      inclusion: jobStatuses
    }
  }
  return validate(job, constrains, { format: 'flat' })
}

ValidationsClass.prototype.isJobGroupValid = function (jobGroup) {
  const constrains = {
    job_group_id: {
      presence: true,
      numericality: {
        onlyInteger: true,
        greaterThan: 0
      }
    },
    job_group_color: {
      presence: {
        allowEmpty: false
      }
    },
    job_group_ex_city_id: {
      presence: true,
      numericality: {
        onlyInteger: true,
        greaterThan: 0
      }
    },
    job_group_to_city_id: {
      presence: true,
      numericality: {
        onlyInteger: true,
        greaterThan: 0
      }
    },
    job_group_deadline: {
      presence: {
        allowEmpty: false
      }
    }
  }
  return validate(jobGroup, constrains, { format: 'flat' })
}

ValidationsClass.prototype.isConsolidationPriceValid = function (price) {
  const constrains = {
    con_price_consolidation_id: {
      presence: true,
      numericality: {
        onlyInteger: true,
        greaterThan: 0
      }
    },
    con_price_forwarder_id: {
      presence: true,
      numericality: {
        onlyInteger: true,
        greaterThan: 0
      }
    },
    con_price_total_value: {
      presence: {
        allowEmpty: true
      }
    },
    con_price_currency_name: {
      presence: {
        allowEmpty: false
      }
    }
  }
  return validate(price, constrains, { format: 'flat' })
}
