let HelpersClass = function () {
  console.info('Constructing Helpers...')
  // this.API_URL = 'http://127.0.0.1:3000/api/v1'
  this.API_URL = 'http://master.portalx.eu:8081/api/v1'
  this.SERVER_DOCUMENTS_URL = 'http://master.portalx.eu:8081/documents'
  this.protectedRoutes = ['cities', 'vessels', 'users', 'forwarders', 'departments', 'products', 'colors', 'servicetypes', 'positions', 'deletedIndividuals', 'statistics']
  this.GLOBAL_CURRENCY = 'USD'
  this.GROUPED_COLOR = '#32CD32'
  this.INDIVIDUAL_COLOR = '#DC143C'
  this.PERSONNEL_COLOR = '#4B0082'
  this.LOCAL_ORDER_COLOR = '#1E90FF'
  this.LOCAL_ORDER_TEXT = 'Local Order'
  this.TBA = 'TBA'
  this.user = {}
  this.currencies = []
  this.ENGLISH_LETTER = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
  this.initializeLogo()
}

HelpersClass.prototype.validateLogin = function () {
  let self = this
  const token = localStorage.getItem('user-token')
  const userData = JSON.parse(localStorage.getItem('user-data'))
  if (!token || !userData) self.handleLogout()

  self.user = userData
  $('#span-username').html(self.user.user_fullname)

  // Enable routes for Not simple users
  if (self.user.user_role_id > 2) {
    $('#statistics_list').attr('hidden', null)
    $('#positions_list').attr('hidden', null)
    $('#deleted-ind').attr('hidden', null)
    $('#deleted-person').attr('hidden', null)
    $('#settings_list').attr('hidden', null)
  } else {
    const lastRoute = window.location.href.split('/').pop()
    if (self.protectedRoutes.indexOf(lastRoute) !== -1) self.handleLogout()
  }

  // Enable Positions for operator
  if (self.user.user_role_id == 2) {
    $('#positions_list').attr('hidden', null)
  }

  // Enable service types for God
  if (self.user.user_role_id == 5) {
    $('#service-types-list').attr('hidden', null)
  }

  $('#logout-ref').on('click', function () {
    self.handleLogout()
  })
}

HelpersClass.prototype.userTokenExists = function () {
  const token = localStorage.getItem('user-token')
  if (!token) return false
  return true
}

HelpersClass.prototype.handleLogout = function () {
  let self = this
  localStorage.removeItem('user-data')
  localStorage.removeItem('user-token')
  window.location.replace('/login')
}

HelpersClass.prototype.bindMovingEvents = function (btnId, modalId, modalContentId) {
  let moveButton = document.getElementById(btnId)

  moveButton.onmousedown = function (event) {
    let modal = document.getElementById(modalId)

    modal.style.position = 'absolute'
    document.body.append(modal)

    function moveAt(pageX, pageY) {
      let modalContent = document.getElementById(modalContentId)
      modal.style.left = pageX - modalContent.offsetWidth * 1.5 + 'px'
      modal.style.top = pageY - modalContent.offsetWidth * 0.05 + 'px'
    }

    function onMouseMove(event) {
      moveAt(event.pageX, event.pageY)
    }

    document.addEventListener('mousemove', onMouseMove)

    modal.onmouseup = function () {
      document.removeEventListener('mousemove', onMouseMove)
      modal.onmouseup = null
    }
  }
}
HelpersClass.prototype.toastr = function (type, message) {
  // notification popup
  toastr.options.closeButton = true
  toastr.options.positionClass = 'toast-top-right'
  toastr.options.showDuration = 1000
  toastr[type](message)
}

HelpersClass.prototype.getDateTimeNow = function () {
  var datetime = require('datetime-js')
  var dateObj = new Date()

  return datetime(dateObj, '%Y-%m-%d %H:%i:%s')
}

HelpersClass.prototype.changeDateToMysql = function (datetime) {
  var dateTimeArray = datetime.split('/')
  if (dateTimeArray.length == 3) {
    return dateTimeArray[2] + '-' + dateTimeArray[1] + '-' + dateTimeArray[0]
  } else {
    return datetime
  }
}

HelpersClass.prototype.changeMysqlDateToNormal = function (datetime) {
  let dateTimeArray = datetime.split(' ')
  let dateSplitted = dateTimeArray[0].split('-')
  let stringdate = `${dateSplitted[2]}/${dateSplitted[1]}/${dateSplitted[0]}`
  if (dateTimeArray.length != 2) return stringdate
  return `${stringdate} ${dateTimeArray[1]}`
}

HelpersClass.prototype.formatFloatValue = function (num) {
  if (num == null) return null
  if (typeof num == 'string') {
    if (num == '') return null
    return parseFloat(num.replace(',', '.'))
  }
  return parseFloat(num.toFixed(2))
}

HelpersClass.prototype.checkIfUserHasPriviledges = function (jobUserName) {
  let self = this

  if (jobUserName == self.user.user_username || self.user.user_role_id > 2) {
    return true
  }
  return false
}

HelpersClass.prototype.initCurrencies = function (currencyInputs) {
  let self = this

  for (let currency of currencyInputs) {
    $(`#${currency}`).chosen()
    $(`#${currency}`).empty()
    $(`#${currency}`).append('<option></option>')
  }

  $.ajax({
    contentType: 'application/json',
    url: self.API_URL + '/Rates/GetRates',
    type: 'GET',
    success: function (response) {
      if (response.status !== 200) {
        for (let currency of currencyInputs) {
          $(`#${currency}`).append(new Option('EUR', '1'))
          $(`#${currency}`).trigger('chosen:updated')
        }
        return
      }
      for (let currency of currencyInputs) {
        for (let curr of response.data) $(`#${currency}`).append(new Option(curr.currency, curr.rate))
        $(`#${currency}`).val('1')
        $(`#${currency}`).trigger('chosen:updated')
      }
    },
    error: function (jqXHR, textStatus, error) {
      console.log('Request failed: ' + error)
      for (let currency of currencyInputs) {
        $(`#${currency}`).append(new Option('EUR', '1'))
        $(`#${currency}`).trigger('chosen:updated')
      }
    }
  })
}

HelpersClass.prototype.validOutput = function (field) {
  if (typeof field == 'undefined' || field == null) return '-'
  return field
}

HelpersClass.prototype.applyRate = function (value, rate) {
  if (value == null || value === '') return null
  return rate === '' ? this.formatFloatValue(value) : this.formatFloatValue(value * rate)
}

HelpersClass.prototype.revertRate = function (value, rate) {
  if (value == null || value === '') return null
  return rate === '' ? this.formatFloatValue(value) : this.formatFloatValue(value / rate)
}

HelpersClass.prototype.swalUserPermissionError = function () {
  Swal.fire({
    title: 'Unable to access this job.',
    text: "Unfortunately this is a job inserted by different user. You can't access it.",
    icon: 'error',
    showCancelButton: true,
    showConfirmButton: false
  })
}

HelpersClass.prototype.bindCloseBtnsAlerts = function () {
  let self = this

  $('.close-btn').on('click', function () {
    let modalId = $(this).data('modal-referer')
    if ($(this).data('warning') == true) {
      Swal.fire({
        title: 'Close Window?',
        text: `Are you sure you want to close this window? Changes made will not be saved.`,
        icon: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancel',
        confirmButtonColor: '#dc3545',
        confirmButtonText: 'Confirm'
      }).then(result => {
        if (result.isConfirmed) {
          $(`#${modalId}`).modal('hide')
        }
      })
    } else {
      $(`#${modalId}`).modal('hide')
    }
  })
}

HelpersClass.prototype.initializeLogo = function () {
  let self = this
  $('.company-logo').show()
}

HelpersClass.prototype.getHeaders = function () {
  const token = localStorage.getItem('user-token')
  return { Authorization: `Bearer ${token}` }
}

HelpersClass.prototype.handleRequestError = function (error) {
  let self = this
  if (error.response && error.response.status && error.response.status == 401) self.handleLogout()
  self.toastr('error', 'Server Error. Please try again later')
}

HelpersClass.prototype.setSelectizeAfterValidation = function (elementId, data, path) {
  let self = this
  let selectize = $(`#${elementId}`)[0].selectize
  if (_.has(data, path)) {
    selectize.setValue(_.get(data, path))
  } else {
    selectize.setValue('')
  }
}

HelpersClass.prototype.validateSelectValue = function (inputValue) {
  if (inputValue == '') return null
  return inputValue
}

HelpersClass.prototype.getDropzoneConfig = function () {
  let self = this
  return { url: `${self.API_URL}/documents`, method: 'post', uploadMultiple: false, maxFilesize: 1024, paramName: 'document', headers: self.getHeaders() }
}

HelpersClass.prototype.makeTimeout = function (ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}

HelpersClass.prototype.downloadFile = async function (url, fileName) {
  let self = this
  try {
    const response = await axios({
      url,
      method: 'GET',
      responseType: 'blob',
      headers: self.getHeaders()
    })

    const href = window.URL.createObjectURL(response.data)
    const anchorElement = document.createElement('a')

    anchorElement.href = href
    anchorElement.download = fileName

    document.body.appendChild(anchorElement)
    anchorElement.click()

    document.body.removeChild(anchorElement)
    window.URL.revokeObjectURL(href)
  } catch (error) {
    console.log(error)
  }
}

HelpersClass.prototype.swalPieceInfo = async function (jobId) {
  let self = this
  try {
    const response = await axios.get(self.API_URL + `/pieces/job/${jobId}`, { headers: self.getHeaders() })

    let htmlString = ''
    for (let piece of response.data) {
      let pieceString = `<h5>${piece.vessel.vessel_description}: ${piece.piece_reference} Ref.  ${piece.piece_weight} KG (${piece.piece_description})</h5>`
      htmlString += pieceString
    }
    Swal.fire({
      html: htmlString
    })
  } catch (err) {
    console.error(err)
    self.handleRequestError(err)
    return []
  }
}

HelpersClass.prototype.applyMouseInteractions = function (tableID) {
  $(`#${tableID}`).on('mouseenter', 'tbody tr', function () {
    $(this).addClass('hover-table-tr')
  })

  $(`#${tableID}`).on('mouseleave', 'tbody tr', function () {
    if ($(this).hasClass('hover-table-tr')) $(this).removeClass('hover-table-tr')
  })

  $(`#${tableID}`).on('click', 'tbody tr', function () {
    if ($(this).hasClass('click-table-tr')) {
      $(this).removeClass('click-table-tr')
    } else {
      $(this).addClass('click-table-tr')
    }
  })
}

HelpersClass.prototype.applySearchInteraction = function (table) {
  $('#search_datatable').keyup(function () {
    if ($(this).val() != '') {
      let regexp = `\\b${$(this).val()}`
      table.search(regexp, true, false).draw()
    } else {
      table.search('').draw()
    }
  })
}

HelpersClass.prototype.getJobInfo = async function (jobId) {
  let self = this
  if (jobId == null) return null
  try {
    const response = await axios.get(self.API_URL + `/jobs/${jobId}`, { headers: self.getHeaders() })
    return response.data
  } catch (err) {
    console.error(err)
    self.handleRequestError(err)
    return null
  }
}

HelpersClass.prototype.getJobGroupPrices = async function (groupId) {
  let self = this
  try {
    const response = await axios.get(self.API_URL + `/prices/group/${groupId}`, { headers: self.getHeaders() })
    return response.data
  } catch (err) {
    console.error(err)
    self.handleRequestError(err)
    return []
  }
}

HelpersClass.prototype.getConsolidationInfo = async function (consolidationId) {
  let self = this
  try {
    const response = await axios.get(self.API_URL + `/consolidationGroups/${consolidationId}`, { headers: self.getHeaders() })
    return response.data
  } catch (err) {
    console.error(err)
    self.handleRequestError(err)
    return null
  }
}
