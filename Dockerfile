FROM httpd:2.4.54-alpine

COPY httpd.conf /usr/local/apache2/conf/httpd.conf

COPY . /usr/local/apache2/htdocs/

RUN rm /usr/local/apache2/htdocs/httpd.conf